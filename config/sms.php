<?php

return [
    'username' => env('SMS_USERNAME', ''),
    'password' => env('SMS_PASSWORD', ''),
    'baseUrl' => env('SMS_BASEURL', ''),
];


