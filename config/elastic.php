<?php

return [
    'app_info_log_enable' => env('APP_INFO_LOG_ENABLE', true),
    'info_level_log' => env('INFO_LEVEL_LOG', 'DEBUG'),
    'success_log' => env('SUCCSS_LOG', true)
];