<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\PatientController;
use App\Http\Controllers\PatientServiceController;
use App\Http\Controllers\PatientServiceAjaxController;
use App\Http\Controllers\ResourceController;
use App\Http\Controllers\DoctorController;
use App\Http\Controllers\PrintController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\ReportController;
use App\Http\Controllers\ServiceTypesController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\PatientSenderController;
use App\Http\Controllers\DiscountController;
use App\Http\Controllers\PatientCardController;
use App\Http\Controllers\SmsController;
use App\Http\Controllers\TelegramController;
use App\Http\Controllers\DebitController;
use App\Http\Controllers\DebitProductsController;
use App\Http\Controllers\ServiceProductController;
use App\Http\Controllers\OutputController;
use App\Http\Controllers\UserController;
use App\Models\Patient;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/ping', function(){

    return Carbon::now()->startOfYear()->toDateString();
});

Route::get('/prices/{service_type_id}', [PrintController::class,'generatePrices']);



Route::post('/webhook/{token}/callback', [TelegramController::class,'handle'])->withoutMiddleware([\App\Http\Middleware\VerifyCsrfToken::class]);;


// Route::post('/patient-services/export', [PatientServiceAjaxController::class, 'index'])->name('patient-services.api.index');
Route::get('patient/profile/{code?}',[PatientController::class,'profile'])->name('patient.login');
Route::get('patient/detail',[PatientController::class,'profileDetail'])->name('patient.profile');
Route::get('patient/logout/{id}',[PatientController::class,'logout'])->name('patient.logout');


Route::middleware(['auth'])->group(function(){

    Route::get('/', [HomeController::class,'index'])->name('home');
     Route::get('/home', [HomeController::class,'index'])->name('home.welcome');
    Route::get('/dashboard/{range}', [DashboardController::class,'statistic'])->name('dashboard.statistic');

    Route::get('/sms/index', [SmsController::class, 'index'])->name('sms.index');
    Route::post('/sms/send', [SmsController::class, 'send'])->name('sms.send');
    Route::get('/seach-patients/{phone}', [SmsController::class, 'ajax']);


    Route::resource('patients',PatientController::class);


    Route::resource('service-types',ServiceTypesController::class);



    Route::get('patients', [PatientController::class,'index'])->name('patients.index');

    // Route::delete('patients-services/delete/{patient_service}', [PatientServiceController::class,'delete'])->name('patient-services.delete');

    //repetition
    Route::get('repetition/create/{patient_id?}', [PatientServiceController::class,'createRepetetion'])->name('repetition.create');
    Route::post('repetition', [PatientServiceController::class,'storeRepetetion'])->name('repetition.store');


    Route::resource('patient-services',PatientServiceController::class);
    Route::prefix('excel')->group(function () {
        Route::get('patient-services',[PatientServiceController::class,'excelExport'])->name('patient-services.excel'); 
    });
    Route::resource('doctors',DoctorController::class);
    Route::resource('services',ServiceController::class);


    Route::get('/print/{service_id}', [PrintController::class, 'printConclusion'])->name('printConclusion');
    Route::get('/print-recommendation/{service_id}', [PrintController::class, 'printRecommendation'])->name('printRecommendation');
    Route::get('/print/check/{patient_id}', [ PrintController::class,'printCheck'])->name('print.check');

    //payment
    Route::get('/patients-for-cashier', [ PatientServiceController::class,'patientsForCashier'])->name('patient-services.cashier');
    Route::get('/get-patient-services/{patient_id}', [ PatientServiceController::class,'getPatientServices'])->name('get-patient-services');

    Route::post('/confirm', [ PatientServiceController::class,'paymentConfirm'])->name('payment.confirm');
    Route::get('/patients-services/profile/{patient_service}', [ PatientServiceController::class,'profile'])->name('patient-services.profile');
    Route::get('/patients-services/for-patient/{patient_service}', [ PatientServiceController::class,'forPatient'])->name('patient.service-details');

    //conclusion

    Route::get('/payed-services', [ PatientServiceController::class,'getPayedServices'])->name('patient-services.payed');
    Route::post('/save-conlusion', [ PatientServiceController::class,'saveConclusion'])->name('patient-services.conclusion');


    Route::get('/resources/cities/{region_id}', [ResourceController::class,'getCitiesByRegion']);
    Route::get('/resources/diagnostics/{service_type_id}', [ResourceController::class,'getDiagnosticTypes']);
    Route::get('/resources/services/{service_type_id}/{diagnostic_type_id?}', [ResourceController::class,'getServices']);
    Route::get('/resources/service-types', [ResourceController::class,'getServiceTypes']);
    Route::get('/resources/debit-products', [ResourceController::class,'getDebitProducts']);



    //reports

    Route::get('/reports/all', [ReportController::class, 'all'])->name('reports.all');
    Route::get('/reports/by-patient-senders', [ReportController::class, 'bySenders'])->name('reports.bysenders');




    Route::resource('patient-senders',PatientSenderController::class);

    Route::get('/sended-patients/{patient_sender}', [PatientServiceController::class, 'getSendedPatients'])->name('patients.sended');

    Route::resource('discounts',DiscountController::class);

    Route::resource('cards',PatientCardController::class);

    // debits
    Route::resource('/debits', DebitController::class);
    // Route::get('/debits/{id}/edit', [DebitController::class,'createRepetetion'])->name('repetition.create');

    // debit_products
    Route::resource('/debit-products', DebitProductsController::class);


    Route::resource('/service-products', ServiceProductController::class);

    
    Route::get('/outputs', [OutputController::class,'index']);

    Route::prefix('users')->group(function () {
        Route::get('/', [UserController::class,'index'])->name('users.index');
        Route::post('/', [UserController::class,'store'])->name('users.store');
        Route::get('/create', [UserController::class,'create'])->name('users.create');
    });
    

});



Auth::routes();



