<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Middleware\Logs;
use App\Http\Controllers\SkillsPassportController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::group(['prefix' => 'v1', 'middleware' => 'logs'], function () {
    
   
//     

// });

Route::middleware([Logs::class])->group(function(){
    Route::get('ping',function(){
        return 'pong';
    });
    Route::get('competitors', [SkillsPassportController::class,'getCompetitors']);
    Route::post('check-competitor/{id}', [SkillsPassportController::class,'getCompetitorResult']);
    Route::put('update-competitor/{id}', [SkillsPassportController::class,'updateCompetitor']);
    Route::post('pingp',function(Request $request){
        return response()->json([
            'success' => true,
            'data' =>  $request->all()
        ]); 
    });

});


