@extends('layouts.app')

@section('content')

<div class="pd-20 card-box mb-30">
    <div class="clearfix mb-20">
		<div class="text-center">
			<h4 class="text-blue h4">Umumiy statistika  <span id="interval">   {{ (app('request')->input('begin_date')) ? ' : '.app('request')->input('begin_date').' dan': '' }}    {{ (app('request')->input('end_date')) ? app('request')->input('end_date').' gacha': ''  }}  </span></h4>
		</div>
    </div>
    <div class="row mb-30">
        <div class="col-md-2">
            <div class="form-group">
                <label>Boshlanish sanasi</label>
                <input class="form-control date-picker" placeholder="Boshlanish sanasi" type="text" id="begin_date">
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>Tugash sanasi</label>
                <input class="form-control date-picker" placeholder="Tugash sanasi" type="text" id="end_date">
            </div>
        </div>
        <div class="col-md-3">
            <button class="mb-30 btn btn-success" style="position:absolute; top:35%" onclick="updateUrl()">Qo'llash</button>
        </div>
    </div>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th>№</th>
                <th>Xizmat nomi</th>
                <th colspan="4" style="text-align: center;">Mijozlar</th>
                <th colspan="4" style="text-align: center;" >Xizmatlar</th>
                <th colspan="2" style="text-align: center;" >Kirim</th> 
            </tr>
            <tr>
                <th></th>
                <th></th>
                <th>Jami</th>
                <th>Ro'yxatga olindi</th>
                <th>To'lov qilindi</th>
                <th>Xulosa olindi</th>
                <th>Jami</th>
                <th>Ro'yxatga olindi</th>
                <th>To'lov qilindi</th>
                <th>Xulosa olindi</th>
                <th>To'lov qilindi</th>
                <th>To'lov jarayonida</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th></th>
                <th> Jami </th>
                <th>{{ array_sum(array_column($statistics, 'all_patients')) }}</th>
                <th>{{ array_sum(array_column($statistics, 'registered_patient')) }}</th>
                <th>{{ array_sum(array_column($statistics, 'payed_patient')) }}</th>
                <th>{{ array_sum(array_column($statistics, 'conclused_patient')) }}</th>
                <th>{{ array_sum(array_column($statistics, 'all_services')) }} </th>
                <th>{{ array_sum(array_column($statistics, 'registered_service')) }}</th>
                <th>{{ array_sum(array_column($statistics, 'payed_service')) }}</th>
                <th>{{ array_sum(array_column($statistics, 'conclused_service')) }}</th>
                <th>{{ number_format(array_sum(array_column($statistics, 'payed_benefit')), 0, '', ' ')  }}</th>
                <th>{{  number_format(array_sum(array_column($statistics, 'waiting_benefit')), 0, '', ' ') }}</th>    
            </tr>
            @foreach($statistics as $statistic)
            <tr>
                <td>{{ $loop->index+1 }}</td>
                <td>{{ $statistic->name }}</td>
                <td>{{ $statistic->all_patients }} </td>
                <td>{{ $statistic->registered_patient }}</td>
                <td>{{ $statistic->payed_patient }}</td>
                <td>{{ $statistic->conclused_patient }}</td>
                <td>{{ $statistic->all_services }} </td>
                <td>{{ $statistic->registered_service }}</td>
                <td>{{ $statistic->payed_service }}</td>
                <td>{{ $statistic->conclused_service }}</td>
                <td>{{ number_format($statistic->payed_benefit, 0, '', ' ') }}</td>
                <td>{{  number_format($statistic->waiting_benefit, 0, '', ' ') }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection

<script>

function updateUrl(){
    
    
    
    var begin_date = document.getElementById("begin_date").value;
    var end_date = document.getElementById("end_date").value;
    var base_url = window.location.origin;
    
    if(begin_date == "" && end_date == "") {
        window.location.href  = base_url + '/reports/all';
    }

    if(begin_date == "" && end_date != "") {
        window.location.href  = base_url + '/reports/all?end_date='+formatDate(end_date);
    }
    
    if(begin_date != "" && end_date == "") {
        window.location.href  = base_url + '/reports/all?begin_date='+formatDate(begin_date);
    }

    if(begin_date != "" && end_date != "") {
        window.location.href  = base_url + '/reports/all?begin_date='+formatDate(begin_date)+'&end_date='+formatDate(end_date);
    }

  
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}

</script>