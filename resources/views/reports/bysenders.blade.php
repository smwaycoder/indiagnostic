@extends('layouts.app')

@section('content')

<div class="pd-20 card-box mb-30">
    <div class="clearfix mb-20">
		<div class="text-center">
			<h4 class="text-blue h4">Shifokorlar statistikasi  <span id="interval">   {{ (app('request')->input('begin_date')) ? ' : '.app('request')->input('begin_date').' dan': '' }}    {{ (app('request')->input('end_date')) ? app('request')->input('end_date').' gacha': ''  }}  </span></h4>
		</div>
    </div>
    <div class="row mb-30">
        <div class="col-md-2">
            <div class="form-group">
                <label>Boshlanish sanasi</label>
                <input class="form-control date-picker" placeholder="Boshlanish sanasi" type="text" id="begin_date">
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>Tugash sanasi</label>
                <input class="form-control date-picker" placeholder="Tugash sanasi" type="text" id="end_date">
            </div>
        </div>
        <div class="col-md-3">
            <button class="mb-30 btn btn-success" style="position:absolute; top:35%" onclick="updateUrl()">Qo'llash</button>
        </div>
    </div>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th rowspan="2">№</th>
                <th rowspan="2">Shifokor nomi</th>
                <th rowspan="2">Jami kirim miqdori </th>
                <th rowspan="2">Jami xizmatlar</th>
                <th colspan="10" style="text-align: center;">Xizmat turlari bo'yicha</th>
            </tr>
            <tr>
                <th>Лаборатория</th>
                <th>МСКТ</th>
                <th>Узи</th>
                <th>Консултация</th>
                <th>Процедуралар</th>
                <th>Лор</th>
                <th>Детский массаж</th>
                <th>ЭКГ</th>
                <th>Стационар</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <th></th>
                <th> Jami </th>
                <th>{{ number_format(array_sum(array_column($statistics, 'payed_benefit')), 0, '', ' ')  }}</th>
                <th>{{ array_sum(array_column($statistics, 'all_services')) }}</th>
                <th>{{ array_sum(array_column($statistics, 'labaro')) }}</th>
                <th>{{ array_sum(array_column($statistics, 'mskt')) }}</th>
                <th>{{ array_sum(array_column($statistics, 'uzi')) }}</th>
                <th>{{ array_sum(array_column($statistics, 'consult')) }} </th>
                <th>{{ array_sum(array_column($statistics, 'proced')) }}</th>
                <th>{{ array_sum(array_column($statistics, 'lor')) }}</th>
                <th>{{ array_sum(array_column($statistics, 'detskiy_massaj')) }}</th>
                <th>{{ array_sum(array_column($statistics, 'ekg')) }}</th>
                <th>{{ array_sum(array_column($statistics, 'statsionar')) }}</th>
            </tr>
            @foreach($statistics as $statistic)
            <tr>
                <td>{{ $loop->index+1 }}</td>
                <td>
                    <a href="{{ route('patients.sended', ['patient_sender' => $statistic->patient_sender_id]) }}">
                     {{ $statistic->fullname }}
                    </a>
                </td>
                <td>{{ number_format($statistic->payed_benefit, 0, '', ' ') }}</td>
                <td>{{ $statistic->all_services }} </td>
                <td>{{ $statistic->labaro }} </td>
                <td>{{ $statistic->mskt }} </td>
                <td>{{ $statistic->uzi }} </td>
                <td>{{ $statistic->consult }} </td>
                <td>{{ $statistic->proced }} </td>
                <td>{{ $statistic->lor }} </td>
                <td>{{ $statistic->detskiy_massaj }} </td>
                <td>{{ $statistic->ekg }} </td>
                <td>{{ $statistic->statsionar }} </td>
                
   
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection

<script>

function updateUrl(){

    var begin_date = document.getElementById("begin_date").value;
    var end_date = document.getElementById("end_date").value;
    var base_url = window.location.origin;
    
    if(begin_date == "" && end_date == "") {
        window.location.href  = base_url + '/reports/by-patient-senders';
    }

    if(begin_date == "" && end_date != "") {
        window.location.href  = base_url + '/reports/by-patient-senders?end_date='+formatDate(end_date);
    }
    
    if(begin_date != "" && end_date == "") {
        window.location.href  = base_url + '/reports/by-patient-senders?begin_date='+formatDate(begin_date);
    }

    if(begin_date != "" && end_date != "") {
        window.location.href  = base_url + '/reports/by-patient-senders?begin_date='+formatDate(begin_date)+'&end_date='+formatDate(end_date);
    }

  
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [year, month, day].join('-');
}

</script>