@extends('layouts.app')

@section('content')
<div class="pd-20 card-box mb-30">
	<div class="clearfix mb-20">
		<div class="text-center">
			<h4 class="text-blue h4">Shifokorlar ro'yxati </h4>
		</div>
		<div class="pull-right">
			<a href="{{ route('patient-senders.create') }}" class="btn btn-primary btn-sm scroll-click">Qo'shish  </a>
		</div>
	</div>
	<div class="table-responsive">
		<table class="table table-bordered table-striped" id="myTable">
			<thead>
				<tr>
					<th>#</th>
					<th>Shifokor ID raqami</th>
					<th>Shifokor nomi</th>
					<th>Qo'shimcha ma'lumot</th>
					<th>Holati</th>
					<th>TG link</th>
					<th>Botga ulanganligi</th>
					<th>Qo'shilgan vaqt</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				@forelse($patientSenders as $patientSender)
					<tr>
						<td>{{ $loop->index+1 }}</td>
                    	<td> {{ $patientSender->id }}</td>
						<td> {{ $patientSender->fullname }}</td>
						<td> {{ $patientSender->add_info }}</td>
                      	<td> {{ $patientSender->is_active ? 'Faol' : 'Faol emas' }}</td>	
						<td> {{ $patientSender->uuid ? 't.me/ProMedClinicBot?start='.$patientSender->uuid : '' }}</td>
						<td> {{ $patientSender->telegram_user_id ? 'Ulangan' : 'Ulanmagan' }}</td>
						<td> {{ $patientSender->created_at }}</td>
						<td><a href="{{ route('patient-senders.edit', ['patient_sender'=>$patientSender->id])}}" class="btn btn-success">Tahrirlash</a></td>
					</tr>
				@empty
					<tr>
						<td colspan="6">Ma'lumotlar topilmadi</td>
					</tr>
				@endforelse	
			</tbody>
			
		</table>
	</div>
</div>	
@endsection
