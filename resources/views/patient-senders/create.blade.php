@extends('layouts.app')
@section('content')
	<div class="pd-20 card-box mb-30">
		<div class="clearfix mb-30">
			<div class="text-center">
				<h4 class="text-blue h4"> Yangi shifokor qo'shish </h4>
			</div>
		</div>
		@if ($errors->any())
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
			</div>
		@endif
		<form method="POST" action="{{ route('patient-senders.store') }}">
			@csrf

			<div class="clearfix mb-15 mt-15">
				<div class="pull-left">
					<h4 class="text-blue h4">  </h4>
				</div>
			</div>
			<div id="patient-senders">
				<div class="row">
					<div class="col-md-4 col-sm-12">
						<div class="form-group">
							<input class="form-control"  type="text"  placeholder="Shifokor nomi" name="fullname">
						</div>
					</div>
					<div class="col-md-4 col-sm-12">
						<div class="form-group">
							<input class="form-control"  type="text"  placeholder="Qo'shimcha ma'lumot" name="add_info">
						</div>
					</div>
				</div>
			</div>
			<hr>
	
			<br>
			<div class="row">
				<div class="form-group offset-11">
					<button type="submit" class="btn btn-primary">Saqlash</button>
				</div>
			</div>
		</form>
	</div>
@endsection
