<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-4-grid@3.4.0/css/grid.min.css" rel="stylesheet" >
	<!-- <link rel="stylesheet" type="text/css" href="/themes/deskapp/src/plugins/datatables/css/responsive.bootstrap4.min.css"> -->
    
	<!-- <link rel="stylesheet" type="text/css" href="/themes/deskapp/vendors/styles/core.css"> -->
	<!-- <link rel="stylesheet" type="text/css" href="/themes/deskapp/vendors/styles/icon-font.min.css"> -->
	<!-- <link rel="stylesheet" type="text/css" href="/themes/deskapp/src/plugins/datatables/css/dataTables.bootstrap4.min.css"> -->
	<!-- <link rel="stylesheet" type="text/css" href="/themes/deskapp/src/plugins/datatables/css/responsive.bootstrap4.min.css"> -->
	<!-- <link rel="stylesheet" type="text/css" href="/themes/deskapp/vendors/styles/style.css"> -->
    
    <style>
        body{
            margin:0;
            padding: 0;
            /* height: 100%; */
        }
        table {
          border-collapse: collapse;
        }
         td {
            padding: 6px;
        }
        
    </style>
</head>
<body>
   

    <div class="row">
        <div style="text-align: left;">
            <img src="{{ public_path('images/logonew.png') }}" alt="" style="width: 250px; height:80px">
        </div>
       
        <div  style="text-align:right; top:0; right:0; position:absolute; margin:0; padding:0">
            <div class="text-ceaster">
                <!-- <center> -->
                    <p>Aдрес: Мирзо Улугбекский район, <br> массив Ахмад Югнакий, 45-дом.</p>
                    <p>Телефон: +998 71 200 67 77</p>
                    
                <!-- </center> -->
            </div>
            
        </div>
    </div> 
    <br><br>
    <div class="row">
        <div class="col-md-12">
            <center>
                <table  border="1">
                    <tr>
                        <td>Дата исследования:</td>
                        <td colspan="2"> {{  $patientService->created_at }}</td>
                    </tr>
                    <tr>
                        <td>ФИО/возраст пациента:</td>
                        <td> {{  $patientService->patient->last_name.' '.$patientService->patient->first_name }}</td>
                        <td>{{ $patientService->patient->birth_date }}</td>
                    </tr>
                    <tr>
                        <td>Область исследования</td>
                        <td colspan="2">{{ $patientService->service->name }}</td>
                    </tr>
                </table>
            </center>    
            
            <div> {!! $patientService->recommendation !!} </div>
        </div>
    </div>
    
    
</body>
</html>



