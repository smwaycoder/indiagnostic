<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-4-grid@3.4.0/css/grid.min.css" rel="stylesheet" >
	<!-- <link rel="stylesheet" type="text/css" href="/themes/deskapp/src/plugins/datatables/css/responsive.bootstrap4.min.css"> -->
    
	<!-- <link rel="stylesheet" type="text/css" href="/themes/deskapp/vendors/styles/core.css"> -->
	<!-- <link rel="stylesheet" type="text/css" href="/themes/deskapp/vendors/styles/icon-font.min.css"> -->
	<!-- <link rel="stylesheet" type="text/css" href="/themes/deskapp/src/plugins/datatables/css/dataTables.bootstrap4.min.css"> -->
	<!-- <link rel="stylesheet" type="text/css" href="/themes/deskapp/src/plugins/datatables/css/responsive.bootstrap4.min.css"> -->
	<!-- <link rel="stylesheet" type="text/css" href="/themes/deskapp/vendors/styles/style.css"> -->
    
    <style>
        body{
            margin:0;
            padding: 0;
            /* height: 100%; */
        }
        
        table, th, td {
		border: 1px solid;
		border-collapse: collapse;
		
	}

     /* .result > table, th, td  {
        font-size: 12px !important;
		padding: 6px !important;
     } */

        
        /* body, table, th, tr, td, div {
            line-height: 1.1;

        } */


        .footer {
            position: fixed;
            left: 0;
            bottom: 0;
            width: 100%;
            text-align: left;
            font-size: 10px;
        }

        .parent {
            margin-bottom: 20px;
        }

        .left-arrow { float: left; }
        .right-arrow { float: right; }

        .icon_header{
            width: 25px;
            height: 25px; 
            margin-top: 10px;  
        }
        
    </style>
</head>
<body>
   

    <div class="row">
        {{-- <div  style="text-align: left; margin-top:30px;">
            <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAUYAAABeCAYAAABfJb2yAAAABGdBTUEAALGPC/xhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAC4jAAAuIwF4pT92AAAAB3RJTUUH5QEFDRcTCQavCAAAM5pJREFUeNrtnXmcHGWdxr9vdc+RSTLTIQk5zMEZw6WAF17oCl6oiCJIEhCVoHitq4vrqrsqKrvrtesFApmRMzMioqgswioiIofch4QEQiATyH3MkcmcXe/+8bw1XTPTXV3d0zM9Sfr5fPqTyUxV9VtV7/u8v/tnmGBItTQBeMDxwIeAtwILgCrgReDXwCXAs2BpW7K83EOuoIIK9jGYcg8gQOqaS6CqFqAe+CTwKWBelkMt8BDweeAugLYl5435+BqamzB6Wgcg0j4I6AYeB1YD/eMxjgoqqGDsMXGIUZLiNOA7wIeBZJ5TnkYS5d/GWnJMtTSCxWDMycC/A68EJiGS3gr8HPgvYLPt66b93E+X92FWUEEFo0I+8hkXOFKsAb4GfBSp0vmwCJHRWWC2jHoQF32R1KJFYPEwNACzkcQ6H5iPYSHwDmBO6CwDzAI+646/wFRPaov6mobmRuoPnU3nui0HAq9w1+8AHsGyFkO6InlWUEF5UXaJsaGlEaNhfBi4FElicWGBr01tmPHNzvbtRanUjpSrgMOBNwKvB44CXgI0IMKO85wGkPp/hbWW9qUjJdhUcxNgExhzGvCvwDHu+j6yn64Afgh0VMixggrKh7JLjI4UDwEupDBS1Onw0c727TcCqwo50RFiEngtsBw5eWYRT1rNhiRwGnCNMaYnx/cB5kzkPJoW+rOHJMevITL+cqqlqa9CjhVUUB6UlRhTzY1UVVfT39f3ESSlFYODgCXGmH9PtTTllRpTKxvBMwAzEBmfjxwqpcACYCrQk+PvC4GvMJQUw0gAFyAp8sZUS9Mj7bu62utTdVkl0FHgZGAJkrjzYQBoRxLtGuRoetH9vlAsBF4HPAU8FvP7K6ggQA1wIlAH3IFMUGOC8kqMxtDf13coWqSjwfustZehBZsTqeYrcK7lecCPgPdSvISYDb1EE8aJwBF5rjEZ+DQyLTzeMG3yb4GbUy1Na4CBEkmRRyFbbqHoQ8/4buAGNDk7Y547DbgCeBvwDPB+4O+luJkK9huchbStGuA/kIY1JigbMTY0B6olpwKHjvJyLwXeBDTXNzfRsTQHeZgEQAr4HvC+MbitR6217cYMNUlOuebHwY+HEp+IpyDp6rXAPwK3AD9LtTQ9QOkIcjNwPSL0XKhCpDbPjX8ecDZwOiLG7wF/AdJ5visFLHY/L0A23Aox7p9IoDnQgKJL9sQ87wgkOAAcjfirGM0lL8pGjI476pFdrhT3cYqF6z2TfYGmWppo29lD6oDaC4APjMEtbQeuNcb41g7VEI0d5MKeQi+K7KhzkR30fSg06H9SLU2lCHB/Efg60BZjDDXAgSiG83TgFPd5NfBttJN3R1zjBeRcOxtJnA+MZuAV7NWYiebxDOCdiBzj4Ho0/+qBRsaIFKG0amQxeDlwbImudYLJHhA+iNQBtccAn0A7ViGwiNTayf4ydgJfs5a/ACPsgZ3nfir48RHi747ZMB15vn8NnGKtMRmnzpgiuP9W4Cakhp8O/MmN6VvAlxF55kI/ki7fjMKbdo7HwCuYkEgicqynMA56BJlg3g7cOpYDLAsxpjJq9Enu4ZQC88lBsqmWJjzPA1iGRPh82IUkmmuBryJ733vceP8FZdysQ06EFuCDwOXG4OdRce8Bbi/BvR4D/MwYPmjTacaJHMPoB/4MLAWuQur2hUiqjUIa2EG06l5BBVHYjQSUMXXclUdilBo9GTkjSoVq4HUGQ0Nz44g/+r4/F5FbFLYD30WB3G8F+2Hgm929A9e0LTnvj8BDFvs/SIV8E5J+zgX+2Ns3EBmYbX0LclR8Gbi3BPc7C/i+SSROAnn4y4AtwD8jFacW+BJwQjkGUkEFpUReG2ODS4czxsxA0pYBNlrYYhhVlsbBwJExj7XEC7J+hcXWGWOyqauvBg6LOHcn8Clr7Q3GGJvtvkK/2+0+sdG+bLkkZcPfkXftPKQSzEQ74DqUzfNSRPJxMBfZCP+OKUH2T3HYhUKQFqFMngtRquZoTAYVFIhQ2mod0sJqkNmn01o6DfiY8akrsC8gJ9nUXXkp1bU1oIf8UaROHuT+vBnZl66w1j6GMbY95gNPNa8A44GM8FeTX2rtAf6KpMt8hNEK/AOwLpgAqZYmDGBl3/rniHN/grX/hDFjnpLnpDuDMVNR3GO3xXYYzHSUefNepLbPJf+G4AP/BPw4V8ZNFnwW+AEqxnEy+Z0vcXAGep8+cCbyog/HZHe/eygsBs0gL/10d75B0vd2tEENV6uqULLAAPEJ2kPxrC9x/ybdtTcCm1CoUoDDkcbwiyz3UYOk5x6GmgwSyNkw113fuHM3u08fBSJkQpmOsrbeiswss9wYBpDpYjWKILgdazeAsW1LC57jde66M9yz9d3z2ebeQ5R5pNqNB3fePGQjnIK0s9Vk54E9DLXpJ1CEhIcEmUKcL0nkBW9w4+lHAklbtuvklBgdKU5HquU5w45tQJLNKcaYfwNaUi1NsQjFGoOHwWJfRTxV/iFUWOI4N54oHIhCStYN+U69yOMizusEfokx6W6/4PlZMNpEXhYtjPDC2pJqafoV8FskgZ2JpK+DIy7nueOuMca0j/ngc+MWZHd8J7K53sbIEJ5lKBXyWuLFoFUDr0KRC69DduQpiFS60EZ4N3JGPUiGXE5CDqG7gC8QvYCSqCjIUrT5LkAE7rnrbQMeBn4J3OzG9F0k7e9G3tUwzgM+BvwUuBwt5hPQGnojIoVJ7h563fUfAW4E/pcYm9TUqxpJ1BgQWb0fOeSOI7vz6xD3DJcCazHmZ8CVqZambRZLe3RUg0Fa1uko/vRwRExVaP72IlJc5d7374D1Wa5zBtIkfHdeNao54KHNtJuRAkAPClN7KPS7WcB17r7PQfGw+ca/EM3JtyDOmk6GGHcAT7qx34o2QCAHMYWKOgSOh1wEuhAFSp9ljBfLCWBEipORRzoO/hd4FFWxyYdasgdQzyAj7WbDc+4B0bvsEzGHNTZoW3IebUvOGwBWWbgIeDfwKzSpcuEYis8cKhW6kAQFIoBsTq4DEMnPjnG9w1AI0M1oUb0KkcxGFPozgJxtF6I58iMyG8h0pNYvInrznQ58Ay3ozyDTThda6I8g4m1AYVJXIVvqpSj29vdkd6TNQ3N7LlpD/wr8Bvg4moObkWPvPjTvgutfiRx5r4h6KPUtKwJSnAf8BOXXn0B0RADu2b0U+E/3PccbTNSarUZZYbe6c16PyOQx4E6kxT2NSP6d7vnf6s6py/Ld1aFPVehvVcP+Fv4Mf3dVSPA5nPzaYz0i1tvQPDodbax7EAF2IHI+A4X+3IK02BrIQnihwOsPIC9jPlVuGvAta/01wINx0vIQ8x+S7yA3+DutZZcxtJI/awRgsWc8Gpobw2rlHESOubAGa3dNgJoag2hbch6p5hUW461C9SlTaNfLhgZEEvdMuXYFu885v1zDvgfFRs4HXoYWfjF4PfBjJAVtBS5D5LUazQmLFt9CZDo5CxHPy5DE5sf4jlloMZ+ByPDn7vMEspum0UY7D0mgH0JSonHHfAFJe7lQBXwR+Dd3/csQsa4mo/5Pctc/GZmr3oHIczmShIdgynU/xRNXLETS6NuLeLaeu59rkWR7d6q5iSyq9XIkGQM0u8/j7tn0k4ltnene0xmIIH+CiPCy0LV+gTY4yKjSNyPp/xz3TIaH0FniZ1UNx1w39jPRBnobkvgfRPOp172f6W7sp7tnucL9/6IRxOgCr+che1wd8XAQmgTnEs+mcwjRRBVgHbDGGAaAZ2OO5VDf+rXDCjnMyXMv6zAmbSdY6m7b0vOD6kNbgCak6uWS3hcZA4lEWcn9RfSeXoIyE35TxDVeiSboEUg1/wrwN0aq5TuADUhyuRrN148jsrstz3fUIknxTKT6fQlJ5cPtZB1oIT2MFvfXEUGmkNkmak6+DUk3LwKfQ0QwXKXvQqroo+5ZfQ9Jo99HRLMhfHAyUQ0SRL5HHlIMZnLEbDgSkdhZGNYMEyQOdmNOIIn3p2S3IQaxvWuR+efdyOxxZ5bjwutxChm1ut19SoXpqELVB4DnkdZ1g3vWw7EFaQc3uuf+TfTOaoaIqiGxeimFB16fgoy/1OcIHQmF0SwmXiWdJ6y1u9zP62IcDyL14bGRs4j2wLcC+Ok4gsb4ImQDeozooOg5vm89U16hdw/uWRJtusiFGUhtOwKpquciKTRfumErii/9FrJF5vNAvRsR3HaUl95C/tjK9chpdTWSiL+KJPVceAUiws+goPh8joJnkOp3P/Aa4CPhP6ZaGplUmwJJeTnTWX1rOXnOPH7y6jfwiUVHkfQizfjHuvuYNCyN9XhkyrgLqZlx4k570eayHMX3lgPGPcMPILI+F5lAuvKc14M2vvci7Wxbtqc2FxnJC11idcgeWRu1Ot0LiKMSAzxmjAnY6nnyLxDQ4houjaYijk/jVKLOsz9W4C2PK9qIVi2mGmOSZTYHWKRqgeyJhWYYLUFq5WpkO2wt4Nxe5Gm/BdnScqEOqa21qKjF/xbwHR1Iqvi7G+db8xzfWOD11yOJ10dOlQMzfzJ097Qdiaov5XyuPpaXTZvO2Ycs4u1z55PIv1OehpM+Q4LLTPfvsxQWlhY4ZMqFlyMbZxeKF/5LgeevRjbsjHGzoWXwobyN4g35JxIhaRpjsNbWEh1PGKCXoTUWXySemj4VSYhhRKnRafLvKBMBPtEbQ5KJYSQNxlhFYQkEB6ANGeTgKKi+psNuZJvcFXHMYiSRbUGSYqH2k+fceUkkYeQiqU3AyiKu/2ck7SzCCRAhTe4MipPEo1CHJKuakNQYRErMp/AaqeXE+5HZ7Bak2heNwYnrCsbWIjG90J0+wAHAKZjITIwG9MDzYRdDjffbiBf7VsPQ9gPkuR9LPEm03Ai8d7kwwMSobzjF/dtLPCdIgKOQd/05Mob6YnA/WRwXIRyN5unjxLdbD8cdiISPJ3cI2ROI4ArFNqRWT2KogzKFnDNjgdciT2+Ah8nEBH+STEWbiYwpKBPNIkfLqCTX4Tv6Ykaf0nUylhS5RfhZDFERcmIj2tUDtCGDe5x7Gk6M6TzHF7sRjCcmE7177wFbboKvRqYYkNOikPEcjaSXx1E4TrHoAf4Y8fcgpGct0dWAotCK5uZscs/l5yiumlIQOwlDCygfzFDyAiBtLQO+H/pYfFfdyaK/9Yf/bv1su+dMhmp6a1CISxLVPVyJpNXDiO+QHW/MQo6TrcgePyokYYht4c3EI60oHIEI9r4cf5+P1N18WG+hM0Sve5CxPA5mD7uvqAXgsXfsiAfkGecuz0um0/6YVWKKg+lkpJxCpaVAi1iHwkFGg8cjrhE4TEZT3SfI3JlD7rlcUMroMARjDzsMFzLM2eMZw+tmzmbOpDoCuvOt5Zhp4tNZtXWcvvAQ+n0J7gZDT3qAv2zdRGd/f9ju4uFIt2HlFbQv+5hFHmuLJMZTkcNqK5kq7k+7f59FZq5ym6Omu+ezjvg8kRNJGHSI1JA7Tq4QpJDUeV84prH+2suDvy8kXi7wswYGrLWB5azXYOIEeQPM9NO+8TwTbI5RKniC0rU2GEu8hGiJcbPvp8ttZDwGvd89KASlEASkX2zsWhi7kLQ2lo8juHYu80WpzRrTGBZZkTSGzx35Mk6ek73a3pGpaVx+wpuG/G5rTzfvuv0WOvraGOaJng5gzKDytAeFDf0aOZpOQnGir0TpkAZpBNsRQf4VmUAeYvQbWzGodc+nuxTfH37QOct2FYETrLUJgxlUpUwigUtajhPYDU7iaF+63MXyeT7YuMQ43fO8ZOgB7UD2rlzOgNkAU69bQefZZQuOzoqG5hXBj4uJVvlbgTHtr50HBjkjJiFSLLQ6d5DOF7eARhRqyB2eFThmZsW8VjbUo810N6XJMy8KFmjt2s3THW0MhIojz6ip5cDaSXQN9LOha/egodcDtvf20OdHWDiGbiU+WodrkYd9BgqHOwip1YuRCeR4RJafRCl7/4EyfMYTA268hTr9siI8eY5lpG2uWBztqvEM2gjlkSZh4nnVelF4jmABCX/bYpwL0IAZzIeETLR7LolrwUBfP8mqsjdNHAGjghtVRG9a3RTvSCgVXoZULlCIStx3FSDY9OYRbKHF41D0rrNdI3hORyHVq5jg4iPRWnmYUH7tGGMXWvyDk7Tf9/n6Yw9Q7SUGbzRtfT6z+Bj++ciX8+jOHZx/75/p9dME7lXfWjr6+8gSUrcTwNqc/rIBMgUvHnS/89AmsQhlvSxHcZsHoLCi0ZgTCkUbUucPcGNqG83FvFRLI26beA2la3UQ7CpDYKQuzYtxfgehxlahiPy4toOpDM0d3Uq0inZwsrqqjjJHR0dgDlJTc2ErxafflQK1KPh5HnKcXF/ENVYj1ewo8hcLiYIhus7nY2jDPoY8eck5kEQpZFWowtR4Fe5YTxaTUEd/P9t7e9jhPtt7eugakDzQ5/uDv9vR28P23h529vWStiP2Cx9XkKF9aUEak48I6H6UYfJRtFGcgdTv8cQWNPdmk8VJVSg8tznXUdwkyYV6stdaTBFPhdlOdoljF/FCQCYzlBi3EV2EYiGjW4xjgilX/jT48TiiK48/zfirLmGcg/KVQcUQimly9Rgi9yNR+EixOIToRfkMSlmbgtpcFOp4eysixk0o1Wy88BxZqsmY4R8zGHrn/m/wjBlyTBYEaYmjxR9QBkk1yncfT+xEDt8aJL2OCoEuPod4QddxYYBjjBlRTXsWuXsqh7GR7A6TXD1XhqOWTP234Lz1EcfHLWoxrkjWVpNOp0FB91G2t78B3WUKYzwN5R1PQpkGP6W4gWxAxvsaRFipIsdzNtFdJ/tQLvYuMilgccO1Xg78lxvbCkpDJnHRRv4c8GJxL0NJN0Hxgd3Pu39TEccMuE+CoZV2RgOLHEVdKAf+2NFcLCDGw4kXptND/EbpR1hra4Z5vl5CvB16fV9vf3eWb9lNPI9TUCwU17Gvj+hMism4eo31K8vSIiAnEonEfKKjBbqRR3C8HS+1yKZ0KVJf1qJCIsXa3Cwy8K9DKWqfo3BHzMmokES+DKk73LgTwL+jPOuo3kMJ5JW9EtlSb0Kpe+OyE4WqVf2CvGmShrSzE/bbWPH13cA1QE/bsvODe13mnk+hPgdDRo2NijnegzamycQrJhMXf0HkOB/lzccpbxdGCscbATEeRf56bqBJfwnxjKoH4cJgQkUlFhLPjvlcdU0VdqQtpIt4lY6rgvsJ2ScfJVoNPwFL0vMmhp0xlDn0FqJtJmsZX8mlDtVb/BkihznIPngBuWNX4+JJlIu8B5X1+jeiCzUESADvQtJqHyr0EIU0Kkv1M3c/F6FsiQ+hONyZaO7OR4T4I1SS7OWoEstniZdsUFL096WfROXGcs7jhDHc1Po8H7jzNi5+4qHBGMYI3ETQcU/HBp0oP4zIcXG+C4TwFpQ514uKf+TCbjRnqpFvo1ToAS5G6+FdKBc+bl2GY1FptS8Bk5K+tcYzJm5u9GpkcN5E/iDt2UhC3BQSGg8mP3xcJZ32ZSMkoB7iSYxJRhL939FknpnjnOMxzGFYqaeyQQ9tMqqGHaXq3TFpSvWWPZ1FVR6vQe+kg9wxfwlkj5uLpKUT0WRuQCT0W1ShZdTZBg4r0eL8Gpqkr0Sb8b1IygjvlpPRwj0HkRpkVON8FYfbUaGKDYgI3ooW9g5kcxsgk3dfh2xYX0FkUUhbhpKgbcl5Qc70Ze6ZZK2wY4Dndnfw7O52DCZfEYnHcRuRTQ+G8GxF4TaXIDPJUWgD+R1al8OTJRJI23wfkrznINvrHRHf6yMyXobe3W2oMHD4mgvItE4oBMEmfRlqfrcYEeRvkbQdzkaqQQLc+1DVooPd76YlPfUdievFedBau94YEyS5R2Eqsts96IzBVcQjxnDpquHoJZ7E6DFSDXse2VFyEeNC5IDa0LCyMRspjxtCdtnXI+ksF7qAm7t399FeeA8PUBWaW4iWpBNIvZhMRtoP1PdGpLqUkij6UT29LYgc34kysp5CEuUmN97paA4eg6S7ZxFxXU+mGEU+tCMJ4xbkPHozWpAHu/sOPrh7P9Q9s4coLA+8JLAD/Zhk1U5Ue3IKOar7eK59SB6sRiXXnsJC+9DKUr9F7/QiVMbtP1FfoSdQ5ss2d//1ZIoSB3xwI5L28wXq/x6FdZ3qzrkNEW81csC9EdUgvaiIR/U3VKnpa4jcv4NMM6sQD+wmU+j4GLTp70EmhYuAjUm0I8YJoekD7jfGDLiHekqe44NS6gGmEq94RBu57VT9xJMYPUJGXWvB80yHtfZe9KKzoRp4e9r3fxPKmCkLQplIHyVTlCEbHkKhEoWiH5GbT35V1UcbUqv7PIhK+t9H4bFi/UgSy5fgP4AChR9AFblPRRP4+GHHpd2YrkMq5qrQ+UFx1HzvMu3u6SFkY5qNiLYaPfuFqMPkm5BN9T2oUOwlZE817XPXHE3zoF53D0Pmevs5F1C/shHPM8+55/ItpFHEMYMF8JEt7gsGHvQh26ZqkcT3BCKWM9yzP4nsHv/d7hleiyT+thjjaENk1YnSDS8YNsYN6P0lyOTc+4jAPPK/19XuGV2P6suegN5hePwDaAO+Ac2hP+DeaRK9+DgpcZtwfVHcl8bBYmut52oqziBeqM4mcttvAm9WPgwpDNG+dFAN+TPaJXNNpH9IeN58CqsDWFI0rBzMdDmR6LADi15oO7ZgHr8JTfo4J6bRxN+F1MnRtEX9BSK7jTGPX4MqSP8QZVgsQpKi58azlkzRiXA6xx3Ik19IJ7mglmS2kmUJJC0Gja4uRqR5MSM36quQuWk0Jpkfonc7ojhzx7LlwVzegMwGf3L/Hke0h9dHUvVVSBLbkrbQEa1pbEdaQTPS/hYjiTrl/t6J3uUz7hNV7i0b1qH6iUcjqXOme57PItV6A0Ml863IZJIgnACSG3vQXL8FCX+HkKnm34u4Zq37niEbWRKp0XE8xavISHLPuC/NV2njcCNVvR1Ji3FCdVrJ7dzJV5MwgCG7Xe4h9yBy2VQPQ3amqxpamojbEraUMKq4PAUReJSndA2y+wRdBwvBRuKTUymxgcIJw0fB/i8SP1xlK/Gap8VFGsWKfhnZUn8MfB6R/PBCtM8Tb9FGIUjDy4rB1sDNjV145mostyHn0JtynNKBQqp+aa1tzdU3PQJ7kI2+mPjUfOh2z/GBGMf2xjxuOPoQCcftAoCH1N04cVwPkDFcthLPKzePjMv8MOLFRq0lKB4xEoUQ49B8SWvp7+7ZRLRROIFioOrK4ZsOFQs+DUk8UbjeGLN+IhRg3I+QRkVqL0WmoeUUpsaWFG1Llwcy/x6Gxu0OxwtIzV3fvnR5oaS4X2Kw3FAe9DGUqbcRb+efTiZw/MgYx4N25lyN4y1Fxo61LV1O1aRakJQVVSLpDTg75OQrflDMVxUN56RagKSRqIn+LNBsrS2LVLufwyKTwDZke1wwusuVBDPI1MHMhs2UpmrRfgOPeBkfmwkFSPvW7kaqXD5UAy/D2sDTlA9dlKYYQhSB/g0l/+fCVNSIqLpqapyykaVBqqUJa20Sef+Oy3P41cYzT4/b4CoYjlakLs9AIWnlxgKiU1pf8K3t2Qe0Cw85Uo4Z7YXifFGc6PancDYpa8EzxpJxxOTD8RizgOg0rQDbiXZ8jFSRc2NkOIXU83Zk2I6aJ6cgyZH65itifl3xCMJzjDHvQZ7oKDwBXG19G6d/dwUjcRoKyRhNYHEvksCSlLHIcchRt4hoe/9znjG2CCfdRIOH8tRLmb6c84vi2P0eAnqsHeLaf5J4fRWORr0j4nikW4kO6IzbhiCrLTLkpPgdTmXPgRQKfD3JM4lkqBlRydHQ0hiE57wULdio8Jl+ZPhv3fvneNnwDhSQXkyz+gBBKM8AxbdHGD2MCeZOlATl47S79sKddKMeYRHH5zunkJlftKsgSX4JrB9Xf21YvNMzyPOXLzZxIQpxiEPAq9u2de1umJFzE04SL6XQkitMw4LxzPPW2uvRAsmF1yHJ8hsWfpxqaUqPhYTm7IrTULZBPhXhNlxJryIDuivIFEs4FhFcMfGG89C8bmP86jGOgOu6ORkJH7nQQZaqPAXCc99xMsoUSSMt8jZGFmdJopjBtyO7Zz8i5t+7f8PE9la0saxFsZLHopCjVuTtv9991yTUyXAuSvE7E0WWVLlrXk9GEKpGweEnI2GsB5kBb2GkV/rdyFfShgqKHI4cWfd45A+Y3kZ2tXkT0VJXgEkooyQOez+RmjkZP/emUEW8ahw5A2zblp4X5GCvJH9fkmnA14weGqWUHKde1xhcbxLKCT4tzymbUWWXDlsRF0eDu9FCOBE5T4rB+9Cie4Iyxrw6zCVatXxhlGOsQYHYP0X+iKDPyytRjGM4YLoOzeVvovV3G3re8935SxnKAyej2NDLEJnej5IHqoD/RmmdHpJ6N7t76UYRMUHo1zYyZNuABIwvIZPZ75FPYTGqhnQqQxEEll+B4ihXufNek0Q7SpQ09wyhorEgU50x7EEq9kkxHm4cUtyDJhqduUX+akZJjIMDMuZpa+1VKHsgCil3zFrg8fqVK+hYVnz7g4bmRjxjsJI63o/Cct5CtOSeBn5kff8e43nlUIn2JTyCypudjd7rcgpr3PVuFGM6gAKfy90E6iiizVSrrLU7TPFFmD+Csn3+ETktAxJKoJTVsCnh00gIWs5QJ+q1aCO6GJHbne73Fm0y30DNt8Jr9rXA/yBOuBMFaifRerkdpaKGkUC52vORdBnmrGtQssSXEJk+EjrnVBQF8nMyUqfnkX83eRRDV1iIC6lx91G6xjcbyT9BJxEvbiwy7axtyaDU2Ew8L/gRaAeb73keDdeuiHHKSKSamzCewUpNvxH4AaoCks/McAtwmfG8Sgza6NGLcn8fQwHRP0cbVFTqpUGS2b8gyWM2CtkZz0K1QxDSXl5HtLDwgDEmXWSBtNkoh/xiJASFr5JGqYVBGN9hSLP6JtnXVFAS7CwyglICkdQKRgoy96KsnvfEHOsxKNf96wwT5Ny4b0GZb2eGfp9ErXbDqjiA77kB5IIPPIyVCpoFj2YZRLFYRf5shcnEq9HXRzzH0A7it9E8CaVqzTVJL1wWLBZSLU1g8LC8G5XFejXxJOk1qGbgLr88hWj3RaxCqWj3IAnnOlQ4ITBpvB55rf8BVX/5AfB/iFCnIzNMnEIJY416cuf+gzLIHoCc6zcfjkTrKE45uVcgE8XTaKOvG/apQSS4gIwn3yD1OddzfAppVnEcrq9F0mBrlu+uQ7zxMLIjBhziI8If4Y9IIhF1KdkLRrYTHZbzgnvwBxXz1Ifhb0BfHvtZPfGIsZd8zc6DjAHDOuBVMcf4Pvf9n8OYZ8LtYXNhyspGkqrxWI+KqH6B3BV+hmMXLg3NAh3l6wC4L+IBJD18AlXjeTMiQh9trD5aH8F860eCwOUo+6XcpAjSZKLig9chcikW0xEHxPG8T0dqfS51yiIbYLhbpyXaFNGP3kEcAWIGsntemeN468a4k6Gtb7MKUEmwfwWzEtkQhl8wp+HW+hbjmX60k57O6FoWduEk1zz2sxG9dXOgmzzEaK3FeGYAFSh9D/nzvgO8CwX1fhW4NdXS1J+LHJ26U4V2sy8gT13cUu49wMXW2puMMZUMl7HBi+g9XomI8QTkYEghKaUXaTGrkRPhHkqbg10UGjJq9ElE1x+4N51Ob0t4RS/N3Uj6qyJ/MY49yB/xHbKn7QadHzsZm+6BQT73f+X4e/D9bcSIREiC6UdG6CpkkA4XLshZMaM9U+Xjz4g8DxrFTa0lQjKtzwSyHkg8Au4ijyodGv/NyDj88QLGe6w751fAVamWpkettZ1grDFgrU0YYw5AO9hZyGAfp4JRGJcDlxhj/IpdcUwRVJ15FhVkDdpiBBEbcYsjjxuc9NJAdPWlfuCPiUTCtl15d7FftcZ9zyLyFyJ+EgkX2xn7jpXZJMLHkYDzAiXYvDy36LYjz8ypyHh6IzKW3kHugg4AWGvXoTpmo8Fds6e/JOfNhJ5C3B4OHcTYFdy9B+XQC509Dchj9zvgVmPMFcbwH8B3jTHXIc/ZjahMUqGkuAPZIXsqpDiusGg+tCGVq5MJRoqhIsavITp1dB2BbfD/flbs1z0H3IWcTtlMbQeRyRV/HNkX/5XsVaHmoHYJqVHcfhq9k2zpj/eidXMh2bW/g9BajKUZJmGQIHpTKxvvpN/eSbWXcA3rByC3euv7Pp7n+cjmcibx+nMMRzfw+807XsxprzPGw/rWGM/MjXnNXcQM3HWhRxvcA72OeKmLYdQjVXk0LT+H4y/Er3lZwX4EYwwWmzCYM4lOR7zd9/0XveLVaNBG8d8oNvBq5IV/CknTr0QmtMuRxtiLwm5+iNZRM9IEa5GGdSqa078c5Xj+ioLBn0TcMROtl93ISfkDVHPyehR8Xod8CO9GppDhan5W++UQe11bppx/mhgG145l5wfq6L3I1nhGETf7OPm8XgaMMbXET9jfboxJ+/kbAdG+9DwaWhrx8O6z2M+jYM846YtjhSeQaaObdHq016pgH4TBHE10Bf0u4DdeacK7dpDRJt9JZo2/iLz0fwodG1QWX4pMSFORqeIFFMT9e4byymqieWYTQ2MnQWFSs1EEAcjTfS+ZDJcPo0iCDyNSTKOCH99FWlzYxPYEOdoqx3FkRMI5YXpQqfe3UHjj+huAnTGyOeqJLq0UxlZrLf5AvLYc7Utkb/St/zvPeJ9Hu05cz3EpcTcKkn0UC21D+3BUsJ8j1dwUaE4fIrr4y4MoyqNU6EJa4S8Q2Vjk7Mi2wLYhqfEyMsTURXaHzFV5vvcutCbC53YiCXYqkvZ2D/v7RuDbbgx1yBTSlWOsl+T4/ag8yUCmk5/FBs2RCgm2W4UTrWNkcxxIfLLaBND5ofjE0rbkPDzjWbAtwGcoXXxmHAygQOOzgUextti4swr2UUy9rlGak2eORX1ecsFHamT7GKSOBja+3eRvBtaLTFod5C4unSa68LRPdm+4dddtjzi/h4ydONdYBxgrYgSRisGkkT0ibvn5AeCnGLM+5gs8mHg2zAGKLNsvtcNY69vrkWPliVI8nzzYjrqZfRx4HvxiWhVUsI8jkTBgqUL9XaJMSqtwLS8qqaPFoyTECATNureiQqt3xTjj18A1WBv5AhtWDnrhjiBeDGAXo6h40rbkvKCn8x+QQ+kGRtfxLRcGUKjTB7H220BH25LzaFtSfB52BfsmBteA4W3kt+OvRDa9CkaBkhFj27Lzg/zjNSiJ+zqyd5SzyFHzRWJUijGeAWs9VP0iDnahlohFo33pefgDaci0YPys+7kUuomPPHsXAh8A/uRjx6SkWQX7Bowyp+agIghRWtMqZAusFDEeJUbtfAmjfely6q+7HC+RfA6phjegxX8seqE7kJh/GbAJbDxxX8HSR+U/EJAaHTf/OSc6zpF9MtXS1Gl9/zLjebchG+BZKOC10GfXg1Tz64Eb0ta2ekaOnwoqyAUX9ZFEpb+i8qJ9YIWB9ZWc+tFjzJrhhap/JFFQ52SgE2t3YbBtMQnBXecE4Fbi2RivsdZ+uIgWkdHjaG7CYo0xJqg48i1yB4tapH63oxCGvyEp+T7fT+/wPI+491/B/otUSyNuiX4QhZFFtdO9G1UJ2lqRFkePcnQJjY1Uc1Mwwn9E7vc4uNDA963skdVkajgG1cqDnMnA49XvPgMW+knbtPFy92p2RP065GTKVarqD8D3UbWPF7G2A4yteJoriIuG5sGWF69C6nFU4sFuFLt3k7UxtbAKIlFSVbrkEIVVYXI2Eh8OHzjRquLILDKSag0ix6B8kUVu/qBuYzfQZaCDhNkF7Ei1NG1DzqQtKDZrB9COte0Y00B0lZ/HMdxm03YwnKmCCuKifuUgKR6CirXmy8ZqRvUGK6RYIkxsYgQwHEz8smAeI8uXjwaBStyN1OIdGPMiItkox1V3xcxTQTEIqc9zkZb0+jynPIYq2vRVVOjSYcISYyhZ/s2oWGU5YJC0WYOkz4XA8THOSwHHGc9sTrU07bTW9hqoxCdWEImGay8nRIo/Rvm9UWhDFatL0Yu9ghAmrI3R2fLqUGbMO0d3tXFHkCGwBeVpPoVCKda4/2+31nYDtqL6VABDbIoHI0kxX0n/NPBNi/2WwVTCvUqMCUmMIafLm1GF8WKq9kw0BPmlW8mQ5ZPu3+eBbVjbVYjHvoJ9A6nmRrVfsvZ4RIpviHHaSpQF01EhxdJjwhHjlOuuIJlIgAJaLyd+M5y9EUH9v22odNPTZMjyWWCztXY3xvimu5u2j3663OOtoMRItTSBJYnhNFQc4fAYp/0JVY/Z4Pf103HuBeW+jX0OE44YnQo9E/WhPb3c4ykTelGQeitSv59AqvhaYDPW7sbgVyTLvRep5sYg9XQWSqP9BPE0owdRZtmqSmjO2GFCEaMjxWpUO+0zRY5vAElhQd+XXhSnmEYSmofCdoJGR9XIuRLEO1ZNtOfiEJDlepSeGJDls6gZ+W6g0l51L0CoF9BbgK8gz3Oc9NxHUYrqwxZbyZoaQ0wYAgjtoKejasGTY5zmo+o0a5AKugYRx1bksduDCGV4eaGAGKsQKU5Cwdr1yKM8HUmtB6Idfab73QLiN80aD/S6+29lKFmuAzZjVYeuElg+MSC12RqMOQL4FCromop5+oMozfZhsJXMqTHGxCFG7aLTUNWdfAHdQa/bX6IKNc9ba7swxpaqm15DSxNWDyhpJE0ehHK/c7WrDFoxVlPKqkWFI0yWa1DntAxZQicWH5OmbUmlEO54INXShLXWGGMOQRkq51JY87g/owLGT1qodIwcB0wIYgyFKpyG0p9qIw5/BhmpfwV0WKtqOGM3tiYnyHI20ETujJdNKNHfAIe5z8EoBnMGkkjLRZiBGv6Ce36ryDh4NmJp6+9J9ydrvEqmTgkRKgBxBMp3Pgtls8Rdd2m0GX8RaK3YFMcPEyLAW6RoPTCnEU2Kq4CPYLgf345LwLQjxenAx4hOA/wT2BvBDACBhFCHpOCXoAWxCHgpSvGah7oHRt1vqVCDvPxzyGQRDaBsnk0YnqualFgDrE61ND2Dcry3Y+nCyARRsV3GQ6q5CbBBRajXovqJbyO6FUE2dKDQne8D7WnSdC6tSPjjhQkhMbqddR5qVrMox2FdKEThl+NleA5VCPokmqS5NpJuVNT25lxjm3TV5dTUJtVzGjMF2S8Pcvd7FCLMg9zvJ1OedxPEWu5ArR3WoRCiZ1CVoE36m90Dxh9raX1vgcvSMkY59EcjInyH+3lSEZd8CvgqiuEdqGxK44+JRIxvQbUaczk3fotUke7xmihuXIvRBH1pxKG3o5JPBQfbOgnDoC6IM1Da4WK0qI5EkuYsykeWIMdVFyLMjSggfR1SxdcjwtwOdFpr+8w+HqRe37yCaq+aATsQSOIvA97oPkeiRk3FoBvZzS82nlnjp21l4ykTyq5KT736kuDHRUTXN7wJ6Kb0DX6ywpHiJGTfiSLFPuBnxKhGng3OY2zRotgAbGhY2fhXcC1jDTORJHkEWoABWR5IcdJIMfDQYp/qxhIUTA0C1NsRMW40xrQCramWplYkdW5BhNoJdFvfH8CYvcZWVn/NZXjV1eD7CYwJOlUuGrADxwGvQO9jLtFmlnywqI3wd5HtvHvXBz9a7lvfr1F2iTGU/vcN1DA7G9qRenL/eEiLqZYm0r5PwvM+hWw8NRGH/xFVKW8fy7E1NDeCtcZ43iREiociFfzlaHEuRLbQ0SzQUiLw0u9GoVPbkVd8M5I6NyPS3O7+3oGk0h6w/b5vB8DQMQ7OoIbrVmCtxUt6STA1aAOYhqTBBciRtoihtuFSCRUvoDaiVxhjNvi+v9dsGvsyyi4xhqg5Kj5wgOjG3CVDYFdMeN4piKijSLET+BHQPtZlxtxiCWyAzwPPp5obb7eybU1Fi/hwpIIfjRZysIhrivnOUcIgx1JgIjgsyzFBMP4eRKAdQBuYXZ5n2oBdqZamNvf7DndMF5ng/T4yhYbTSOW37mPcxyMTsxoE89eRiVudBkw3GmMQuzrDPbepbvxjIUBsQR7nFVj+jsHfdVZFSpwoKD8xZtAb8bc6YDZj3M405Gx5A/ADZNuLwi9Qy4Ky9IFuy5BlB9DRcN3la0wiebPFegYz1T2zg5HN8khkEliACKCO8msMSURQgTMqH9LDPgOhn31yE2NAjkFgfzL0//GERfGl6pAp9TldCcCfeCg7MVrfx3geRHf2mwScnKip+UOquXFMwnRSLY14xsO3/ptQnna+ZP7VwPeA3jFobF4U2s/+ePCjj8wP7cCaVEvTraHwoRmIHBeRIcuDEYnWMwHmRAQSjD+ZlQJBI7QbgV9jWatc9wohTlSUW2IABiW1tyMHS664vlbk+X0IW9oYxlAg7vuBb5M/K2E3cAEq/bRXxvilWhqx0sOrydjTDkFEuZiMPW06ki7Lmc2zN2IAeez/DPwGuCedHtjheYmKDXEvwEQixvnIkbEo4tDbgeXIxjZqQmpoacQwWOHksyheMV+FEx85ZL4C9O+NpJgLrgQWFuuFpMt5ZILTD0ObxlxEphXCHIoeNDfvQyaWe7C8gKFSSHYvw4QhRqfqXYoksSjcCfyLtfYBU2TnvfqWRjzd+hQUiPt54DXEW+S/RMn8O/eXyR5K2UygeMrpiBwPQqR5CPKKzyGT/ljDBJlfY4heZAJajVrk3g08gWVLhQz3bkyIiZta2QieAZVf+g1aeFF4AfXZXYnl+bj2GhEwnjHMRgHlZwMnEj8e8HZU9mk9/dD2of174qcubYJpVglwmFokbc9ABDkfkeV8lBI5C73XqUjSTDJB5l8MBKFHbSg282nkOHkMFerYOLCrqyeZqqv09dlHMGEmZsjO933URzoffKS23IYI6++o3Fi3tTYNYIzxkOQyFUk4RyHyfQOyoVUVMMQ7kKT4TKltnPsiUhc2wSssWAyyY04mU9LtQOTsCT5BiMw05ACagmzNw9vejhUsCvkJ4i7bUVX1jchOuM59WoEtWDoxpLHliUaoYOwxYYgRBslxAfBzlIAfF/1kAoh3ojg3ixZWIMXMQARZqE3MAjejKsvrfGvpqJBiSTC1pYle6zPJGM8osLoWEegU9K7q0ftrcD9PdX+bjKTOSWTIswptrB6ZeR3uH95HphXuHhSD6uIm2YXmzU73czuw21q6kwn8nn7oOrtCgPsTJhQxhkp8vRZlAywazfVKgB6U7ncRsNW3Ph1Lzy/zkCpItazAYjHWMxY8pxmYYR+w1mKMi2u0vrX4YG3NATNt1/pWuj4eRzGpYH/EhCJGGNJw/M3ApShHuBx4HvhP4FrGsXBFBRVUUH5MOGKEIfnTxwLfAU5i/MJCupAD6DvW2seMMXtlnGIFFVRQPCYkMcKQEJGZqIPax5EDZaywB7gLZb38H9Cd9i2dlYrWFVSw32HCEmMAF3TsYXg5cD7wXhQOUoqxW+SwuQO1VLgT6Kw4WCqoYP/GhCfGAM5jnUApa6eQqZA8nfj5vUF1mo0oBu1PKGVrLdCPD23LKmpzBRXs79hriDFAQ0sjNdTSR+8URJKvRAVDF6NA4hSZfOs+FJKxGVWbfhL15l2FAnX7fD9Nx7JKL40KKqggg/8HVVLEy8Yc0A8AAAAldEVYdGRhdGU6Y3JlYXRlADIwMjEtMDEtMDVUMTM6MjM6MTktMDU6MDCdJIZkAAAAJXRFWHRkYXRlOm1vZGlmeQAyMDIxLTAxLTA1VDEzOjIzOjE5LTA1OjAw7Hk+2AAAAABJRU5ErkJggg==
                    " />     
        </div> --}}
        {{-- <div class="parent">
            <div class="left-arrow">
                <img src="{{ public_path('images/leftarrow.png') }}" alt="">
             </div>
            <div class="right-arrow">
                <img src="{{ public_path('images/rightarrow.png') }}" alt="">
             </div>
             <div style="clear: both;"></div>
        </div> --}}
     
       
 
        <div style="text-align: left;">
        <img src="{{ public_path('images/logonew.png') }}" alt="" style="width: 250px; height:80px; margin-top: 30px"></div>
        <div  style="text-align:right; top:0; right:0; position:absolute; margin:0; padding:0">
            <div class="text-ceaster">
                <!-- <center> -->
                    <p>Aдрес: Мирзо Улугбекский район, <br> массив Ахмад Югнакий, 45-дом.</p>
                    <p> Телефон: +998 71 200 67 77</p>
                    <p> promed.uz</p>
                    {{-- <p style="margin-top:15px"><img src="{{ public_path('images/address_icon.png') }}" alt="" class="icon_header"> Aдрес: Мирзо Улугбекский район, <br> массив Ахмад Югнакий, 45-дом.</p>
                    <p><img src="{{ public_path('images/phone_icon.png') }}" alt="" class="icon_header"> Телефон: +998 71 200 67 77</p>
                    <p><img src="{{ public_path('images/instagram_icon.png') }}" alt="" class="icon_header"> promed.uz</p> --}}
                    
                <!-- </center> -->
            </div>
            
        </div>
    </div> 
    <br><br>
    <div class="row" style="margin-top:30px">
        <div class="col-md-12">
            <center>
                <table class="result" style="font-size:14px">
                    <tr>
                        <td>Дата исследования:</td>
                        <td colspan="2"> {{  $patientService->created_at }}</td>
                    </tr>
                    <tr>
                        <td>ФИО/возраст пациента:</td>
                        <td> {{  $patientService->patient->last_name.' '.$patientService->patient->first_name }}</td>
                        <td>{{ $patientService->patient->birth_date }}</td>
                    </tr>
                    <tr>
                        <td>Область исследования</td>
                        <td colspan="2">{{ $patientService->service->name }}</td>
                    </tr>
                </table>
            </center>    
            
            <div> {!! $content !!} </div>
            <br> <br>
            <footer class="footer">
                Примечания: <br>
                Заключение не является диагнозом и требует дальнейшей <br> консультации с врачом, направившим на обследование!
        
            </footer>
        </div>
    </div>

  
    
    
</body>
</html>



