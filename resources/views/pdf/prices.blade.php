<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-4-grid@3.4.0/css/grid.min.css" rel="stylesheet" >
	
    
    <style>
        body{
            /* margin:0;
            padding: 0; */
            /* height: 100%; */
        }
        
        table, th, td {
		border: 1px solid;
		border-collapse: collapse;
		
	}

     /* .result > table, th, td  {
        font-size: 12px !important;
		padding: 6px !important;
     } */

        
        /* body, table, th, tr, td, div {
            line-height: 1.1;

        } */


        .footer {
            position: fixed;
            left: 0;
            bottom: 0;
            width: 100%;
            text-align: left;
            font-size: 10px;
        }

        .parent {
            margin-bottom: 20px;
        }

        .left-arrow { float: left; }
        .right-arrow { float: right; }

        .icon_header{
            width: 25px;
            height: 25px; 
            margin-top: 10px;  
        }

        tr > td, tr > th {
            padding: 4px;
            text-align: left;
            font-size: 14px;
        }

        table {
            margin: 0 auto;
        }

    
        
    </style>
</head>
<body>
   

    <div class="row" style="border-bottom: 1px solid; padding-bottom: 20px">
     
     
       
 
        <div style="text-align: left;">
        <img src="{{ public_path('images/logonew.png') }}" alt="" style="width: 250px; height:80px; margin-top: 30px"></div>
        <div  style="text-align:right; top:0; right:0; position:absolute; margin:0; padding:0">
            <div class="text-ceaster">
                <!-- <center> -->
                    <p>Aдрес: Мирзо Улугбекский район, <br> массив Ахмад Югнакий, 45-дом.</p>
                    <p> Телефон: +998 71 200 67 77 
                    {{-- <p style="margin-top:15px"><img src="{{ public_path('images/address_icon.png') }}" alt="" class="icon_header"> Aдрес: Мирзо Улугбекский район, <br> массив Ахмад Югнакий, 45-дом.</p>
                    <p><img src="{{ public_path('images/phone_icon.png') }}" alt="" class="icon_header"> Телефон: +998 71 200 67 77</p>
                    <p><img src="{{ public_path('images/instagram_icon.png') }}" alt="" class="icon_header"> promed.uz</p> --}}
                    
                <!-- </center> -->
            </div>
            
        </div>
    </div> 
    <br>
    {{-- <p>Narxlar</p> --}}
    <br>
    <div class="row">
        <div class="col-md-12">
            <center>
                <table>
                    <thead>
                        <tr  style="font-size:12px">
                            <th>Услуга</th>
                            <th>Цена (сум)</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($services as $service)
                            <tr  style="font-size:12px">
                                <td>{{ $service->name }}</td>
                                <td style="width: 100px">{{ $service->price }}</td>
                            </tr>                 
                        @endforeach
                    </tbody>
                </table>
            </center>
            <br> <br>
            <footer class="footer">
                Примечания: <br>
                Цены действительны на: {{ date('Y-m-d') }} 
        
            </footer>
        </div>
    </div>

    
</body>
</html>



