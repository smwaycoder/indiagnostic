
@extends('layouts.app')
@section('content')
	<div class="pd-20 card-box mb-30">
		<div class="clearfix mb-30">
			<div class="text-center">
				<h4 class="text-blue h4">Kirim mahsulotlari qo'shish </h4>
			</div>
		</div>
		@if ($errors->any())
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
				@if ($errors->has('email'))
				@endif
			</div>
		@endif
		<form method="POST" action="{{ route('debit-products.store') }}">
			@csrf
			<div id="discounts">
				<div class="row">
                    <div class="col-md-3 col-sm-12">
                        <div class="form-group">
							<input class="form-control"  type="text"  placeholder="Mahsulot nomi" name="name">
						</div>
                    </div>
				</div>
			</div>
			<hr>

			<br>
			<div class="row">
				<div class="form-group offset-11">
					<button type="submit" class="btn btn-primary">Saqlash</button>
				</div>
			</div>
		</form>
	</div>
@endsection
