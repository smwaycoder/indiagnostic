
@extends('layouts.app')

@section('content')
<div class="pd-20 card-box mb-30">
	<div class="clearfix mb-20">
		<div class="text-center">
			<h4 class="text-blue h4">Chegirmalar ro'yxati</h4>
		</div>
		<div class="pull-right">
			<a href="{{ route('discounts.create') }}" class="btn btn-primary btn-sm scroll-click">Chegirma qo'shish  </a>
		</div>
	</div>
	<div class="table-responsive">
		<table class="table table-bordered table-striped" id="myTable">
			<thead>
				<tr>
					<th>#</th>
					<th>Chegirma nomi</th>
					<th>Chegirma miqdori</th>
					<th>Chegirma holati</th>
					<th>Chegirma qo'shilgan vaqt</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				@forelse($discounts as $discount)
					<tr>
						<td>{{ $loop->index+1 }}</td>
                    	<td> {{ $discount->name }}</td>
                        <td> {{ $discount->percentage }} %</td>
						<td> {{ $discount->is_active ? 'Faol' : 'Faol emas' }}</td>	
						<td> {{ $discount->created_at }}</td>
						<td><a href="{{ route('discounts.edit', ['discount'=>$discount->id])}}" class="btn btn-success">Tahrirlash</a></td>
					</tr>
				@empty
					<tr>
						<td colspan="6">Ma'lumotlar topilmadi</td>
					</tr>
				@endforelse	
			</tbody>
			
		</table>
	</div>
</div>	
@endsection


	<!-- <script src="/vendor/datatables/buttons.server-side.js"></script> -->