
@extends('layouts.app')

@section('content')
<div class="pd-20 card-box mb-30">
	<div class="clearfix mb-20">
		<div class="text-center">
			<h4 class="text-blue h4">Foydalanuvchilar ro'yxati</h4>
		</div>
		<div class="pull-right">
			<a href="{{ route('users.create') }}" class="btn btn-primary btn-sm scroll-click">Foydalanuvchi qo'shish  </a>
		</div>
	</div>
	<div class="table-responsive">
		<table class="table table-bordered table-striped" id="myTable">
			<thead>
				<tr>
					<th>#</th>
					<th>Foydalanuvchi nomi</th>
					<th>Login</th>
                    {{-- <th>Amallar</th> --}}
				</tr>
			</thead>
			<tbody>
				@forelse($users as $item)
					<tr>
						<td>{{ $loop->index+1 }}</td>
                    	<td> {{ $item->name }}</td>
						<td> {{ $item->username }}</td>
						
					</tr>
				@empty
					<tr>
						<td colspan="3">Ma'lumotlar topilmadi</td>
					</tr>
				@endforelse
			</tbody>

		</table>
	</div>
</div>
@endsection


	<!-- <script src="/vendor/datatables/buttons.server-side.js"></script> -->
