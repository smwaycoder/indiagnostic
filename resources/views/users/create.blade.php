
@extends('layouts.app')
@section('content')
	<div class="pd-20 card-box mb-30">
		<div class="clearfix mb-30">
			<div class="text-center">
				<h4 class="text-blue h4">Foydalanuvchi qo'shish </h4>
			</div>
		</div>
		@if ($errors->any())
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
				@if ($errors->has('email'))
				@endif
			</div>
		@endif
		<form method="POST" action="{{ route('users.store') }}">
			@csrf
		
				<div class="row">
                    <div class="col-md-3 col-sm-12">
                        <div class="form-group">
							<input class="form-control"  type="text"  placeholder="Foylanuvchi nomi" name="name">
						</div>
                    </div>
					<div class="col-md-3 col-sm-12">
                       
						<select class="custom-select form-control" name="role">
							<option value="">Rolni tanlang</option>
							<option value="doctor">Shifokor</option>
							<option value="reception">Qabulxona</option>
							<option value="maindoctor">Katta vrach</option>
							<option value="cashier">Kassa</option>
						</select>
                    </div>
					<div class="col-md-3 col-sm-12">
                        <div class="form-group">
							<input class="form-control"  type="text"  placeholder="Login" name="username">
						</div>
                    </div>
					<div class="col-md-3 col-sm-12">
                        <div class="form-group">
							<input class="form-control"  type="password"  placeholder="Parol" name="password">
						</div>
                    </div>
					<div class="col-md-3 col-sm-12">
						<div class="form-group">
							<select class="custom-select form-control serviceType" name="service_type_id">
								<option value="">Xizmatni turini tanlang</option>
								@foreach($serviceTypes as $service)
									<option value="{{ $service->id }}">{{ $service->name }}</option>
								@endforeach
							</select>
						</div>
					</div>
				</div>
		
			<hr>

			<br>
			<div class="row">
				<div class="form-group offset-11">
					<button type="submit" class="btn btn-primary">Saqlash</button>
				</div>
			</div>
		</form>
	</div>
@endsection
