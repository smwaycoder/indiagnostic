@extends('layouts.app')

@section('content')
<div class="pd-20 card-box mb-30">
	<div class="clearfix mb-20">
		<div class="text-center">
			<h4 class="text-blue h4">Ko'rsatilgan xizmatlar ro'yxati</h4>
		</div>
		<div class="pull-left">
			<form method="GET" action="{{ route('patient-services.index') }}">
			<div class="row">
				<div class="col-md-3 col-sm-12">
					<div class="form-group">
						<label>Mijoz</label>
						<input type="text" class="form-control" name="full_name"  value="{{ old('full_name', request('full_name')) }}">
					</div>
				</div>
				<div class="col-md-2 col-sm-12">
					<div class="form-group">
						<label>Mijoz Telefoni</label>
						<input type="text" class="form-control" name="phone"  value="{{ old('phone', request('phone')) }}">
					</div>
				</div>
				<div class="col-md-2 col-sm-12">
					<div class="form-group">
						<label>Holati</label>
						<select class="form-control" id="status" name="status">
							<option value="">Statusni tanlang</option>
							<option value=1 {{ (request()->has('status') && request('status') == 1)  ? 'selected' : '' }}>Ro'yxatga olingan</option>
							<option value=2 {{ (request()->has('status') && request('status') == 2)  ? 'selected' : '' }}>To'lov qilingan</option>
							<option value=3 {{ (request()->has('status') && request('status') == 3)  ? 'selected' : '' }}>Xulosa olingan</option>
							<!-- Add more options as needed -->
						</select>
						
					</div>
				</div>
				<div class="form-group col-md-2">
					<label for="date_from">Vaqti(boshlanish)</label>
					<input type="date" class="form-control" id="date_from" name="date_from" value="{{ old('date_from', request('date_from')) }}">
				</div>
				<div class="form-group col-md-2">
					<label for="date_to">Vaqti(tugash)</label>
					<input type="date" class="form-control" id="date_to" name="date_to" value="{{ old('date_to', request('date_to')) }}">
				</div>
				
				<div class="form-group col-md-12">
					<button type="submit" class="btn btn-primary">Izlash</button>
					<a href="{{ route('patient-services.index') }}" class="btn btn-secondary">Filterni tozalash</a>
				</div>
			</div>
			</form>
			<a href="{{ route('patient-services.excel', ['status' => request('status'), 'full_name' => request('full_name'), 'phone' => request('phone'), 'date_from' => request('date_from'), 'date_to' => request('date_to')]) }}" class="btn btn-info btn-sm scroll-click">Excelga yuklash </a>
		</div>
		<div class="row">
			
		</div>
		@if(!\Auth::user()->isAdmin())
			<div class="pull-right">
				<a href="{{ route('patient-services.create') }}" class="btn btn-primary btn-sm scroll-click">Xizmat qo'shish </a>
			</div>
		@endif
	</div>
	<div class="table-responsive">
		<table class="table table-bordered table-hover">
			<thead>
				<tr>
					<th>#</th>
					<th>Mijoz ismi</th>
					<th>Telefon raqam</th>
					<th>Xizmat turi</th>
					<th>Xizmat nomi</th>
					<th>Shifokor</th>
					<th>Qo'shilgan vaqt</th>
					<th>Telegram</th>
					<th>Xizmat narxi</th>
					<th>Xizmat holati</th>
					<th>Mijoz manbasi</th>
					<th>Amal</th>
				</tr>
			</thead>
			<tbody>
				@foreach ($patientServices as $patientService)
					<tr>
						<td>{{ (request()->has('page')) ?  ((request('page')-1)*$perPage + $loop->index+1) : $loop->index+1  }}</td>
						
						<td>
							<a href="/patients-services/profile/{{ $patientService->id }}">{{  $patientService->patient->full_name }}</a>
						</td>
						<td>{{ $patientService->patient->phone }}</td>
						<td><span class="badge badge-{{ $patientService->statusClass }}">{{ $patientService->service->type->name }}</span>  </td>
						<td>{{ $patientService->service->name }}</td>
						<td>{{ ($patientService->patientSender) ? $patientService->patientSender->fullname  : '' }}</td>
						<td>{{ $patientService->created_at }}</td>
						<td>{{ (($patientService->patient->user) && $patientService->patient->user->telegram_user_id) ? 'Ulangan' : 'Ulanmagan' }}</td>
						<td>{{ number_format($patientService->price, 2, ',', '  ') }}</td>
						<td> <span class="badge badge-{{ $patientService->statusClass }}"> {{  $patientService->status->name }}</span> </td>
						
						<td>{{ $patientService->source }}</td>
						<td>   @if(Auth::user()->isReception())  
								<a href="/repetition/create/{{ $patientService->patient->id }}" class="btn btn-primary btn-sm scroll-click"> Qo'shish </a> <br><br>
								@elseif((Auth::user()->isReception() && $patientService->status->name == "Ro'yxatga olindi") || (\Auth::user()->isAdmin()))
			
								<form action="/patient-services/'.$patientService->id.'" id="deleteForm" method="post">
									<input type="hidden" name="_method" value="delete">
									<button type="submmcit" class="btn btn-danger btn-sm" onclick="return confirm(\'Ishonchingiz komilmi?\')" > O\'chirish </button> 
								</form> 
								@endif
					 </td>
				@endforeach
			</tbody>
		</table>
	</div>

	<div class="d-flex justify-content-center">
		{{ $patientServices->onEachSide(5)->links() }}
	</div>
</div>	
@endsection

@section('javascripts')


@endsection