@extends('layouts.app')

@section('content')
<style>
	table, th, td {
		border: 1px solid;
		border-collapse: collapse;
	}

	th, td {
		padding: 8px;
		text-align: left;
	}
</style>
	<div class="pd-20 card-box mb-30">
    <div class="clearfix mb-20">
			<div class="text-center">
				<h4 class="text-blue h4">Mijoz haqida ma'lumot</h4>
			</div>
		</div>
		<table class="table table-bordered">
			<thead>
				<tr>
                    <th>Mijoz ID raqami</th>
					<th>Mijoz familiyasi</th>
                	<th>Mijoz ismi</th>
                    <th>Mijoz tug'ilgan sanasi</th>
					<th>Mijzo jinsi</th>
				</tr>
			</thead>
			<tbody>
                <tr>
                    <td>{{ $patientService->patient->id }}</td>
                    <td> {{ $patientService->patient->last_name }}</td>
                    <td> {{ $patientService->patient->first_name }}</td>
                    <td> {{ $patientService->patient->birth_date }}</td>
                    <td> {{ $patientService->patient->genderLabel }}</td>
                </tr>
   		</tbody>

		</table>
    	<div class="clearfix mb-20">
			<div class="text-center">
				<h4 class="text-blue h4">Ko'rsatilishi kerak bo'lgan xizmat</h4>
			</div>
		</div>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>#</th>
					<th>Xizmat turi</th>
					<th>Xizmat nomi</th>
					<th>Xizmat narxi</th>
					<th>Xizmat holati</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td> 1 </td>
					<td> {{ $patientService->service->type->name }}</td>
					<td> {{ $patientService->service->name }}</td>
					<td> {{ $patientService->service->price }}</td>
					<td> {{ $patientService->status->name }}</td>
				</tr>
			</tbody>

		</table>

		@if(auth()->user()->isReception() && $patientService->service_status_id == 2)
			<h4 class="text-blue h4">Tekshiruv xulosasi</h4>
			<div id="toolbar-container"></div>
			<div id="conclusion" style="border:1px solid #dee2e6;"></div>
	
			
			<form action="{{ route('patient-services.conclusion') }}" method="post" id="conclusionForm" enctype="multipart/form-data">
				@csrf
				<input type="hidden" name="patient_service_id" value="{{ $patientService->id }}">
				<textarea  name="conclusion" style="display:none" id="editorConclusion"></textarea>
				<br>
				<div class="form-group">
					<label>Shifokor xulosasini PDF formatda yuklang</label>
					<input type="file" name="conclusion_file" class="form-control-file form-control height-auto">
				</div>
				
				<div class="form-group offset-11 align-right mt-20">
					<button type="button"  onclick="submitForm()" class="btn btn-primary">Tasdiqlash</button>
				</div>
			</form>
		@endif
		@if($patientService->conclusion)
     
		<div class="col-sm-12 col-md-12 mb-30">
			<div class="card card-box">
				<div class="card-header">
					<div class="row">
						<div class="col-md-6 col-sm-12 col-xs-12 mb-20">Shifokor xulosasi</div>
						<div class="col-md-6 col-sm-12 col-xs-12">
							<div class="text-right">
								<a href="{{ route('printConclusion', ['service_id' => $patientService->id ]) }}" class="btn btn-success">Xulosani chop etish</a>
							</div>
						</div>
					</div>	
				</div>
				<div class="card-body">
					{!! $patientService->conclusion !!}
				</div>
			</div>
		</div>
		@endif
		@if($patientService->conclusion_file)
     
		<div class="col-sm-12 col-md-12 mb-30">
			<div class="card card-box">
				<div class="card-header">
					<div class="row">
						<a href="{{ "/storage/".$patientService->conclusion_file }}" target="_blank">Xulosani yuklab olish</a>
					</div>	
				</div>
				<div class="card-body">
					{!! $patientService->conclusion !!}
				</div>
			</div>
		</div>
		@endif
		@if($patientService->recommendation)
       
		<div class="col-sm-12 col-md-12 mb-30">	
			<div class="card card-box">
				<div class="card-header">
					<div class="row">
						<div class="col-md-6 col-sm-12 col-xs-12 mb-20">Shifokor tavsiyasi</div>
						<div class="col-md-6 col-sm-12 col-xs-12">
							<div class="text-right">
								<a href="{{ route('printRecommendation', ['service_id' => $patientService->id ]) }}" class="btn btn-info">Tavsiyani chop etish</a>
							</div>
						</div>
					</div>	
				</div>
				<div class="card-body">
					{!! $patientService->recommendation !!}
				</div>
			</div>
		</div>
		@endif
	
    </div>


	
    <script>
        DecoupledEditor
            .create( document.querySelector( '#conclusion' ) )
            .then( editor => {
                const toolbarContainer = document.querySelector( '#toolbar-container' );

                toolbarContainer.appendChild( editor.ui.view.toolbar.element );
            } )
            .catch( error => {
                console.error( error );
            } );

       

            function submitForm(){
                var textConclusion =  document.getElementById('conclusion').innerHTML;
        
 
                var modifiedConclusion = removeSvgElements(textConclusion);
        
                document.getElementById('editorConclusion').value = modifiedConclusion;
                document.getElementById("conclusionForm").submit();

            }

            function removeSvgElements(htmlString) {
                // create a temporary div element to hold the HTML content
                const tempDiv = document.createElement('div');
                tempDiv.innerHTML = htmlString;

                // get all SVG elements
                const svgElements = tempDiv.querySelectorAll('svg');

                // remove each SVG element
                svgElements.forEach((svgElement) => {
                    svgElement.remove();
                });

                // return the modified HTML string
                return tempDiv.innerHTML;
            }
    </script>
@endsection
