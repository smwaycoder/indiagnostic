@extends('layouts.app')
@section('content')
	<div class="pd-20 card-box mb-30">
		<div class="clearfix mb-30">
			<div class="text-center">
				<h4 class="text-blue h4">   {!! ($patient) ? ('<b>'.$patient->last_name.' '.$patient->first_name.'</b> uchun yangi xizmat qo\'shish'):'Yangi xizmatni ro\'yxatga olish' !!}</h4>
			</div>
		</div>

		<table class="table table-bordered">
			<thead>
				<tr>
                    <th>Mijoz ID raqami</th>
					<th>Mijoz familiyasi</th>
                	<th>Mijoz ismi</th>
                    <th>Mijoz tug'ilgan sanasi</th>
					<th>Mijzo jinsi</th>
				</tr>
			</thead>
			<tbody>
                <tr>
                    <td>{{ $patient->id }}</td>
                    <td> {{ $patient->last_name }}</td>
                    <td> {{ $patient->first_name }}</td>
                    <td> {{ $patient->birth_date }}</td>
                    <td> {{ $patient->genderLabel }}</td>
                </tr>
   		</tbody>
        </table>
        <div class="clearfix mb-15 mt-15">
            <div class="text-center">
                <h4 class="text-blue text-center h4"> Yangi xizmat qo'shish </h4>
            </div>
        </div>
		@if ($errors->any())
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
				@if ($errors->has('email'))
				@endif
			</div>
		@endif
        <div class="text-center">
        </div>
		<form method="POST" action="{{ route('repetition.store') }}">
			@csrf
			<div class="clearfix mb-15 mt-15">
				<div class="pull-left">
					<h4 class="text-blue h4">  </h4>
				</div>
			</div>
            <input type="hidden" name="patient_id" value="{{ $patient->id }}">
			<div id="services">
				<div class="row">
					<div class="col-md-2 col-sm-12">
						<div class="form-group">
							<select class="custom-select form-control serviceType" name="services[0][serciceType]">
								<option value="">Xizmat turi</option>
								@foreach($services as $service)
									<option value="{{ $service->id }}">{{ $service->name }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-4 col-sm-12">
						<div class="form-group">
						<select class="custom-select2 form-control serviceNames" id="serviceNames0" style="width: 100%; height: 38px;" name="services[0][serviceName]">
							<option value="">Xizmatni tanlang</option>
						</select>
						</div>
					</div>
					<div class="col-md-3 col-sm-12 diagnosticBox0 hidden">
						<div class="form-group">
						<select class="custom-select form-control diagnosticType" id="diagnosticType0"  readonly="true" style="pointer-events:none" name="services[0][diagnosticType]">
							<option value="">Diagnostikani tanlang</option>
						</select>
						</div>
					</div>
					<div class="col-md-1 col-sm-12">
						<div class="form-group">
							<input type="text" class="form-control servicePrice0 price" readonly="" placeholder="" name="services[0][servicePrice]">
						</div>
					</div>
					<div class="col-md-2">
						<button  type="button" class="btn btn-success" id="addService"><i class="fa fa-plus"></i></button>
						<button  type="button" class="btn btn-danger" id="rmService"><i class="fa fa-minus"></i></button>
					</div>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-3 col-sm-12">
					<div class="form-group">
						<label for="">Jami narx</label>
						<input type="text" class="form-control allPrice" placeholder="Jami narx " readonly="">
					</div>
				</div>
				<div class="col-md-3 col-sm-12">
					<div class="form-group">
						<label for="">Chegirma</label>
						<input type="text" class="form-control discount" placeholder="Chegirma " readonly="" value="{{ ($patientCard) ? number_format($patientCard->balance,0,' ',' ') : 0  }}" name="discount_price">
					</div>
				</div>
				<div class="col-md-3 col-sm-12">
					<div class="form-group">
						<label for="">Jami to'lovga</label>
						<input type="text" class="form-control forPayment" placeholder="To'lovga " readonly="" name="price">
					</div>
				</div>
			</div>
			<br>
			<div class="row">
			<div class="col-md-2 col-sm-12">
					<div class="form-group">
						<select class="custom-select2 form-control" name="patient_sender_id">
							<option value="">Shifokorni tanlang </option>
							@foreach($patientSenders as $patientSender)
								<option value="{{ $patientSender->id }}">{{ $patientSender->id }}  {{ $patientSender->fullname }}</option>
							@endforeach
						</select>
					</div>
				</div>

			</div>
			<div class="row">
				<div class="form-group offset-11">
					<button type="submit" class="btn btn-primary">Saqlash</button>
				</div>
			</div>
		</form>
	</div>
@endsection
