@extends('layouts.app')

@section('content')
<div class="pd-20 card-box mb-30">
	<div class="clearfix mb-20">
		<div class="text-center">
			<h4 class="text-blue h4">Ko'rsatilgan xizmatlar ro'yxati</h4>
		</div>
		@if(!\Auth::user()->isAdmin())
			<div class="pull-right">
				<a href="{{ route('patient-services.create') }}" class="btn btn-primary btn-sm scroll-click">Xizmat qo'shish </a>
			</div>
		@endif
	</div>
	<div class="table-responsive">
		{!! $dataTable->table(['class' => 'table table-bordered', false]) !!}
	</div>
    
</div>	
@endsection

@section('javascripts')

<script src="/vendor/datatables/buttons.server-side.js"></script>
{!! $dataTable->scripts() !!}

@endsection