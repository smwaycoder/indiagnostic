@extends('layouts.app')

@section('content')

	<div class="pd-20 card-box mb-30">
       <div class="text-center col-lg-9 col-md-9 offset-1">
            <div class="clearfix mb-20">
                <div class="text-center">
                    <h4 class="text-blue h4">Mijoz haqida ma'lumot</h4>
                </div>
            </div>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>Mijoz ID raqami</th>
                        <th>Mijoz familiyasi</th>
                        <th>Mijoz ismi</th>
                        <th>Mijoz tug'ilgan sanasi</th>
                        <th>Mijzo jinsi</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{ $patientService->patient->id }}</td>
                        <td> {{ $patientService->patient->last_name }}</td>
                        <td> {{ $patientService->patient->first_name }}</td>
                        <td> {{ $patientService->patient->birth_date }}</td>
                        <td> {{ $patientService->patient->genderLabel }}</td>
                    </tr>
                </tbody>
            </table>
            <div class="clearfix mb-20">
                <div class="text-center">
                    <h4 class="text-blue h4">Xulosa berilishi kerak bo'lgan</h4>
                </div>
            </div>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Xizmat turi</th>
                        <th>Xizmat nomi</th>
                        <th>Xizmat narxi</th>
                        <th>Xizmat holati</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td> 1 </td>
                        <td> {{ $patientService->service->type->name }}</td>
                        <td> {{ $patientService->service->name }}</td>
                        <td> {{ $patientService->service->price }}</td>
                        <td> {{ $patientService->status->name }}</td>
                    </tr>
                </tbody>

            </table>
            <h4 class="text-blue h4">Tekshiruv xulosasi</h4>
            <div id="toolbar-container"></div>
            <div id="conclusion" style="border:1px solid #dee2e6;"></div>
       
            
            <form action="{{ route('patient-services.conclusion') }}" method="post" id="conclusionForm">
                @csrf
                <input type="hidden" name="patient_service_id" value="{{ $patientService->id }}">
                <textarea  name="conclusion" style="display:none" id="editorConclusion"></textarea>
                <br>
                <h4 class="text-blue h4">Shifokor tavsiyasi</h4>
                <textarea  name="recommendation" style="display:none" id="editorRecommendation"></textarea>
                <div id="toolbar-container-recommendation"></div>
                <div id="recommendation" style="border:1px solid #dee2e6;"></div>
                <br><br>
                <div class="form-group offset-10 align-right mt-20">
                    <button type="button"  onclick="submitForm()" class="btn btn-primary">Tasdiqlash</button>
                </div>
            </form>
       </div>
    </div>

    <script>
        DecoupledEditor
            .create( document.querySelector( '#conclusion' ) )
            .then( editor => {
                const toolbarContainer = document.querySelector( '#toolbar-container' );

                toolbarContainer.appendChild( editor.ui.view.toolbar.element );
            } )
            .catch( error => {
                console.error( error );
            } );

            DecoupledEditor
            .create( document.querySelector( '#recommendation' ) )
            .then( editor => {
                const toolbarContainer = document.querySelector( '#toolbar-container-recommendation' );

                toolbarContainer.appendChild( editor.ui.view.toolbar.element );
            } )
            .catch( error => {
                console.error( error );
            } );


            function submitForm(){
                var textConclusion =  document.getElementById('conclusion').innerHTML;
                var textRecommendation =  document.getElementById('recommendation').innerHTML;
 
                var modifiedConclusion = removeSvgElements(textConclusion);
                var modifiedRecommendation = removeSvgElements(textRecommendation);

                document.getElementById('editorConclusion').value = modifiedConclusion;
                document.getElementById('editorRecommendation').value = modifiedRecommendation;
                document.getElementById("conclusionForm").submit();

            }

            function removeSvgElements(htmlString) {
                // create a temporary div element to hold the HTML content
                const tempDiv = document.createElement('div');
                tempDiv.innerHTML = htmlString;

                // get all SVG elements
                const svgElements = tempDiv.querySelectorAll('svg');

                // remove each SVG element
                svgElements.forEach((svgElement) => {
                    svgElement.remove();
                });

                // return the modified HTML string
                return tempDiv.innerHTML;
            }
    </script>
@endsection
