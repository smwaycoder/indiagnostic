@extends('layouts.app')

@section('content')
	<div class="pd-20 card-box mb-30">
		<div class="clearfix mb-20">
			<div class="text-center">
				<h4 class="text-blue h4">Shifokor xulosasi uchun xizmatlar</h4>
			</div>
		</div>
		<table class="table table-bordered" id="myTable">
			<thead>
				<tr>
					<th>#</th>
					<th>Mijoz ismi</th>
					<th>Xizmat turi</th>
					<th>Xizmat nomi</th>
					<th>Xizmat narxi</th>
					<th>Xizmat holati</th>
				</tr>
			</thead>
			<tbody>
				@forelse($patientServices as $patientService)
					<tr>
						<td>{{ $loop->index+1 }}</td>
						<td>
							<a href="{{ route('patient-services.show',['patient_service'=> $patientService->id]) }}">
								{{ $patientService->patient->last_name.' '.$patientService->patient->first_name }}
							</a>
						</td>
						<td> {{ $patientService->service->type->name }}</td>
						<td> {{ $patientService->service->name }}</td>
						<td> {{ $patientService->service->price }}</td>
						<td> {{ $patientService->status->name }}</td>
					</tr>
				@empty
					<tr>
						<td colspan="6">Ma'lumotlar topilmadi</td>
					</tr>
				@endforelse	
			</tbody>
			
		</table>
	</div>	
@endsection