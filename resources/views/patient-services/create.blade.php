@extends('layouts.app')
@section('content')
	<div class="pd-20 card-box mb-30">
		<div class="clearfix mb-30">
			<div class="text-center">
				<h4 class="text-blue h4">   {!! ($patient) ? ('<b>'.$patient->last_name.' '.$patient->first_name.'</b> uchun yangi xizmat qo\'shish'):'Yangi xizmatni ro\'yxatga olish' !!}</h4>
			</div>
		</div>
		@if ($errors->any())
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
				@if ($errors->has('email'))
				@endif
			</div>
		@endif
		<form method="POST" action="{{ route('patient-services.store') }}">
			@csrf
			<div class="row">
				<!-- <div class="col-md-1 col-sm-12">
					<div class="form-group">
						<input type="text" name="pass_seria" class="form-control" placeholder="AA" value=" {{ ($patient) ?  substr($patient->passport,0,2)  :  old('pass_seria') }}">
					</div>
				</div>
				<div class="col-md-2 col-sm-12">
					<div class="form-group">
						<input type="text" name="pass_number" class="form-control" placeholder="1234567" value="{{ ($patient) ?  substr($patient->passport,2)  :  old('pass_number') }}">
					</div>
				</div> -->
				<div class="col-md-3 col-sm-12">
					<div class="form-group">
						<input type="text" name="last_name" class="form-control" placeholder="Familiya" value="{{ ($patient) ? ($patient->last_name) : old('last_name') }}">
					</div>
				</div>
				<div class="col-md-3 col-sm-12">
					<div class="form-group">
						<input type="text" name="first_name" class="form-control" placeholder="Ismi" value="{{ ($patient) ? ($patient->first_name) : old('first_name') }}">
					</div>
				</div>
				<!-- <div class="col-md-3 col-sm-12">
					<div class="form-group">
						<input type="text" name="patronymic" class="form-control" placeholder="Otasining ismi" value="{{ ($patient) ? ($patient->patronymic) : old('patronymic') }}">
					</div>
				</div> -->
			</div>
			<div class="row">
				<div class="col-md-3 col-sm-12">
					<div class="form-group">
						<select class="custom-select form-control" name="birth_date">
							<option value="">Tug'ilgan yili</option>
							@foreach($birthYears as $year)
								<option value="{{ $year }}">{{ $year }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="col-md-3 col-sm-12">
					<h6>Jinsi:</h6>
					<div class="form-group">
						<div class="form-check form-check-inline">
						  <input class="form-check-input" type="radio" name="gender" id="inlineRadio1" value="1" {{ (($patient) ? ($patient->gender == 'Erkak') : old('gender')) ? 'checked' : '' }}>
						  <label class="form-check-label" for="inlineRadio1">Erkak</label>
						</div>
						<div class="form-check form-check-inline">
						  <input class="form-check-input" type="radio" name="gender" id="inlineRadio2" value="0" {{ (($patient) ? ($patient->gender == 'Ayol') : !old('gender')) ? 'checked' : '' }} >
						  <label class="form-check-label" for="inlineRadio2">Ayol</label>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-12">
					<div class="form-group">
						<input type="text" class="form-control" placeholder="Telefon raqami" name="phone" value="{{ ($patient) ? $patient->phone : old('phone') }}">
					</div>
				</div>
				
			</div>	
			
			<div id="services">
				<div class="row">
					<div class="col-md-2 col-sm-12">
						<div class="form-group">
							<select class="custom-select form-control serviceType" name="services[0][serciceType]">
								<option value="">Xizmat turi</option>
								@foreach($services as $service)
									<option value="{{ $service->id }}">{{ $service->name }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-4 col-sm-12">
						<div class="form-group">
						<select class="custom-select2 form-control serviceNames" id="serviceNames0" style="width: 100%; height: 38px;" name="services[0][serviceName]">
							<option value="">Xizmatni tanlang</option>
						</select>
						</div>
					</div>
					<div class="col-md-3 col-sm-12 diagnosticBox0 hidden">
						<div class="form-group">
						<select class="custom-select form-control diagnosticType" id="diagnosticType0"  readonly="true" style="pointer-events:none" name="services[0][diagnosticType]">
							<option value="">Diagnostikani tanlang</option>
						</select>
						</div>
					</div>
					<div class="col-md-1 col-sm-12">
						<div class="form-group">
							<input type="text" class="form-control servicePrice0 price" readonly="" placeholder="" name="services[0][servicePrice]">
						</div>
					</div>
					<div class="col-md-2">
						<button  type="button" class="btn btn-success" id="addService"><i class="fa fa-plus"></i></button>
						<button  type="button" class="btn btn-danger" id="rmService"><i class="fa fa-minus"></i></button>
					</div>
				</div>
			</div>
			<div class="loyality hidden">
			<div class="clearfix mb-15 mt-15">
				<div class="pull-left">
					<h4 class="text-blue h4"> Sodiqlik kartasi</h4>
				</div>
			</div>
			<div class="row">
				<div class="col-md-3 col-sm-12">
					<div class="form-group">
						<input type="number" min="10000" max="90000" class="form-control loyalityCard" placeholder="Karta raqami.. "  name="loyality_card_number">
					</div>
				</div>
				<div class="col-md-6 col-sm-12">
					<div class="form-group">
						<select class="custom-select2 form-control" name="chain_id">
							<option value="">Tavsiya qilganni tanlang </option>
							@foreach($patientCards as $patientCard)
								<option value="{{ $patientCard->id }}">{{ $patientCard->number }}  {{ $patientCard->patient->first_name.' '.$patientCard->patient->last_name }}</option>
							@endforeach
						</select>
					</div>
				</div>
			</div>
			</div>
			
			<hr>
			<div class="row">
				<div class="col-md-3 col-sm-12">
					<div class="form-group">
						<input type="text" class="form-control allPrice" placeholder="Jami to'lov: " readonly="" name="price">
					</div>
				</div>
				<!-- <div class="col-md-3 col-sm-12">
					<div class="form-group">
						<input type="text" class="form-control" placeholder="Shifokor "  name="doctor">
					</div>
				</div> -->
				<div class="col-md-4 col-sm-12">
					<div class="form-group">
						<select class="custom-select2 form-control" name="patient_sender_id">
							<option value="">Shifokorni tanlang </option>
							@foreach($patientSenders as $patientSender)
								<option value="{{ $patientSender->id }}">{{ $patientSender->id }}  {{ $patientSender->fullname }}</option>
							@endforeach
						</select>
					</div>
				</div>
				
			</div>

			<br>

			<div class="row">
				<div class="col-md-4 col-sm-12">
					<div class="form-group">
						<input type="text" class="form-control" placeholder="Mijoz qayerdan kelgan: " name="source">
					</div>
				</div>
			</div>
			<div class="row">
				<div class="form-group offset-11">
					<button type="submit" class="btn btn-primary">Saqlash</button>
				</div>
			</div>
		</form>
	</div>
@endsection
