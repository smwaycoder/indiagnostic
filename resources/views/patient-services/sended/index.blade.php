@extends('layouts.app')

@section('content')

<div class="pd-20 card-box mb-30">
		<div class="clearfix mb-20">
			<div class="text-center">
				<h4 class="text-blue h4">Xizmatlar ro'yxati</h4>
			</div>
            <div class="text-left">
				<h4 class="h4">Shifokor: {{ $patientSender->fullname}}</h4>
			</div>
            <div class="row mb-30">
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Boshlanish sanasi</label>
                        <input class="form-control date-picker" placeholder="Boshlanish sanasi" type="text" id="begin_date">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Tugash sanasi</label>
                        <input class="form-control date-picker" placeholder="Tugash sanasi" type="text" id="end_date">
                    </div>
                </div>
                <div class="col-md-3">
                    <button class="mb-30 btn btn-success" style="position:absolute; top:35%" onclick="updateUrl()">Qo'llash</button>
                </div>
            </div>
		</div>
        <div class="tab">
           
            <div class="tab-content">
                <br>
                <div class="tab-pane fade show active" id="registered" role="tabpanel">                  
                    <table class="table table-bordered" id="myTable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Mijoz ismi</th>
                                <th>Xizmat vaqti</th>
                                <th>Xizmat nomi</th>
                                <th>Xizmat turi</th>
                                <th>Xizmat narxi</th>
                                <th> % </th>
                                <th>Bonus</th>
                                
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($patientServices as $patientService)
                            <tr>
                                    <td>{{ $loop->index+1 }}</td>
                                    <td> 
                                        {{ $patientService->patient->last_name.' '.$patientService->patient->first_name }}
                                        
                                    </td>
                                    <td>{{ $patientService->created_at }}</td>
                                    <td>{{ $patientService->service->name }}</td>
                                    <td>{{ $patientService->service->type->name }}</td>
                                    <td> {{ $patientService->service->price }}</td>
                                    <td>{{ $patientService->service->type->doctor_percentage }}</td>
                                    <td>{{ round((int)str_replace(' ', '',$patientService->service->price) * (int)$patientService->service->type->doctor_percentage/100) }} </td>
                                    
                                    
                                    
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="6">Ma'lumotlar topilmadi</td>
                                </tr>
                            @endforelse	
                        </tbody>
                        
                    </table>
                </div>
                <div class="tab-pane fade" id="payed" role="tabpanel">
                    <div class="pd-20">
                        Bu yerda to'langanlar chiqadi
                    </div>
                </div>
                <div class="tab-pane fade" id="conclused" role="tabpanel">
                    <div class="pd-20">
                    Bu yerda xulosa olganlar chiqadi
                    </div>
                </div>
            </div>
        </div>
</div>
@endsection


<script>

    function updateUrl(){
    
        var begin_date = document.getElementById("begin_date").value;
        var end_date = document.getElementById("end_date").value;
        var patient_sender_id = {{ $patientSender->id }};
        var base_url_full = window.location.origin + '/sended-patients/'+ patient_sender_id;
        
        if(begin_date == "" && end_date == "") {
            window.location.href  = base_url_full;
        }
    
        if(begin_date == "" && end_date != "") {
            window.location.href  = base_url_full+'/?end_date='+formatDate(end_date);
        }
        
        if(begin_date != "" && end_date == "") {
            window.location.href  = base_url_full+ '/?start_date='+formatDate(begin_date);
        }
    
        if(begin_date != "" && end_date != "") {
            window.location.href  = base_url_full + '/?start_date='+formatDate(begin_date)+'&end_date='+formatDate(end_date);
        }
    
      
    }
    
    function formatDate(date) {
        var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();
    
        if (month.length < 2) 
            month = '0' + month;
        if (day.length < 2) 
            day = '0' + day;
    
        return [year, month, day].join('-');
    }
    
    </script>