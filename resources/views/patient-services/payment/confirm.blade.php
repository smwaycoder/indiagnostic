@extends('layouts.app')

@section('content')
<style>
@media print {
  body * {
    visibility: hidden;
  }
  #printDiv, #printDiv * {
    visibility: visible;
  }

  /* #nonPrint {
	  visibility: hidden;
  } */
  #printDiv {
    position: absolute;
    left: 0;
    top: 0;
  }
}
</style>
	<div class="pd-20 card-box mb-30" >
	<div id="printDiv">
	<div class="clearfix mb-20">
			<div class="text-center">
				<h4 class="text-blue h4">Mijoz haqida ma'lumot</h4>
			</div>
		</div>
		<table class="table table-bordered">
			<thead>
				<tr>
                    <th>Mijoz ID raqami</th>
					<th>Mijoz familiyasi</th>
                	<th>Mijoz ismi</th>
                    <th>Mijoz tug'ilgan sanasi</th>
					<th>Mijzo jinsi</th>
				</tr>
			</thead>
			<tbody>
                <tr>
                    <td>{{ $patient->id }}</td>
                    <td> {{ $patient->last_name }}</td>
                    <td> {{ $patient->first_name }}</td>
                    <td> {{ $patient->birth_date }}</td>
                    <td> {{ $patient->genderLabel }}</td>
                </tr>
   		</tbody>

		</table>
    <div class="clearfix mb-20">
			<div class="text-center">
				<h4 class="text-blue h4">To'lanishi lozim bo'lgan xizmatlar</h4>
			</div>
		</div>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>#</th>
					<th>Xizmat turi</th>
					<th>Xizmat nomi</th>
					<th>Xizmat narxi</th>
					<th>Xizmat holati</th>
				</tr>
			</thead>
			<tbody>
				@forelse($patient->services as $patientService)
					<tr>
						<td>{{ $loop->index+1 }}</td>
						<td> {{ $patientService->service->type->name }}</td>
						<td> {{ $patientService->service->name }}</td>
						<td> {{ $patientService->price }}</td>
						<td class="nonPrint"> {{ $patientService->status->name }}</td>
					</tr>
				@empty
					<tr>
						<td colspan="6">Ma'lumotlar topilmadi</td>
					</tr>
				@endforelse
			</tbody>

        </table>
	</div>
	<div class="d-grid gap-2 d-md-flex justify-content-md-end">
		<!-- <a href="{{ route('print.check', ['patient_id' => $patient->id  ]) }}" target="_blank" class="mr-3"><button type="button" class="btn btn-danger" ><i class="bi bi-printer-fill"></i> Chop etish</button></a> -->
		<button type="button" class="btn btn-primary" onclick="submitConfirm()">To'langanini tasdiqlash</button>
	</div>
</div>


<script>
function submitConfirm()
{
	window.location.href = "{{ route('print.check', ['patient_id' => $patient->id]) }}";


}
</script>
@endsection
