@extends('layouts.app')

@section('content')

<div class="pd-20 card-box mb-30">
		<div class="clearfix mb-20">
			<div class="text-center">
				<h4 class="text-blue h4">Xizmatlar ro'yxati</h4>
			</div>
		</div>
        <div class="tab">
            <ul class="nav nav-tabs customtab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" data-toggle="tab" href="#registered" role="tab" aria-selected="true">To'lovga</a>
                </li>
              </ul>
            <div class="tab-content">
                <br>
                <div class="tab-pane fade show active" id="registered" role="tabpanel">                  
                    <table class="table table-bordered" id="myTable">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Mijoz ismi</th>
                                <th>Xizmat narxi</th>
                                <th>Xizmat holati</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($patients as $patient)
                                <tr>
                                    <td>{{ $loop->index+1 }}</td>
                                    <td> 
                                        <a href="{{ route('get-patient-services',['patient_id'=> $patient->id]) }}">
                                            {{ $patient->last_name.' '.$patient->first_name }}
                                        </a>
                                    </td>
                                    <td> {{ $patient->price }}</td>
                                    <td> Ro'yxatga olingan</td>
                                </tr>
                            @empty
                                <tr>
                                    <td colspan="6">Ma'lumotlar topilmadi</td>
                                </tr>
                            @endforelse	
                        </tbody>
                        
                    </table>
                </div>
                <div class="tab-pane fade" id="payed" role="tabpanel">
                    <div class="pd-20">
                        Bu yerda to'langanlar chiqadi
                    </div>
                </div>
                <div class="tab-pane fade" id="conclused" role="tabpanel">
                    <div class="pd-20">
                    Bu yerda xulosa olganlar chiqadi
                    </div>
                </div>
            </div>
        </div>
</div>
@endsection