@extends('layouts.app')
@section('content')
    <div class="mobile-menu-overlay"></div>
    <div class="pd-ltr-20">
        <h4 id="statistic_label">To'liq statistika</h4>
        <br>
        <div class="row">
            <div class="col-sm-2 col-md-2 col-sm-4 col-xs-8 mb-30">
                <select class="custom-select col-12 statisticRange">
                    <option selected="" value="all">To'liq statisika...</option>
                    <option value="today">Bugungi kun</option>
                    <option value="yesterday">Kechagi kun</option>
                    <option value="weekly">Joriy hafta</option>
                    <option value="monthly">Joriy oy</option>
                    <option value="yearly">Joriy yil</option>            
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-4 mb-30">
                <div class="card-box height-100-p widget-style1">
                    <div class="d-flex flex-wrap align-items-center">
                        <div class="progress-data">
                            <div id="chart"></div>
                        </div>
                        <div class="widget-data">
                            <div class="h4 mb-0" id="all_patients">{{ $statistic->all_patients }}</div>
                            <div class="weight-600 font-14">Jami mijozlar</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 mb-30">
                <div class="card-box height-100-p widget-style1">
                    <div class="d-flex flex-wrap align-items-center">
                        <div class="progress-data">
                            <div id="chart2"></div>
                        </div>
                        <div class="widget-data">
                            <div class="h4 mb-0" id="all_services" >{{ $statistic->all_services }}</div>
                            <div class="weight-600 font-14">Jami xizmatlar</div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 mb-30">
                <div class="card-box height-100-p widget-style1">
                    <div class="d-flex flex-wrap align-items-center">
                        <div class="progress-data">
                            <div id="chart4"></div>
                        </div>
                        <div class="widget-data">
                            <div class="h4 mb-0" id="all_benefit" >{{ number_format($statistic->all_benefit, 0, '', ' ') }} so'm </div>
                            <div class="weight-600 font-14">Jami kirim</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
     
        <div class="row">
            <div class="col-xl-8 mb-30">
                <div class="card-box height-100-p pd-20">
                    <h2 class="h4 mb-20">Qo'yilgan rejaning bajarilganligi</h2>
                    <div id="chart5"></div>
                </div>
            </div>
            <div class="col-xl-4 mb-30">
                <div class="card-box height-100-p pd-20">
                    <h2 class="h4 mb-20">Bu oy uchun reja</h2>
                    <div id="chart6"></div>
                </div>
            </div>
        </div>
        <div class="footer-wrap pd-20 mb-20 card-box">
            MyDiagnostic 
        </div>
    </div>
@endsection