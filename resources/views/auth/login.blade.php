<!DOCTYPE html>
<html>
<head>
	<!-- Basic Page Info -->
	<meta charset="utf-8">
	<title>MyDiagnostic-Tizimga kirish</title>

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="themes/deskapp/vendors/styles/core.css">
	<link rel="stylesheet" type="text/css" href="themes/deskapp/vendors/styles/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="themes/deskapp/vendors/styles/style.css">

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-119386393-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-119386393-1');
	</script>
</head>
<body class="login-page">
	<div class="login-header box-shadow">
		<div class="container-fluid d-flex justify-content-between align-items-center">
			<div class="brand-logo">
				<a href="login.html">
					<img src="themes/deskapp/src/images/logo-mydiagnos.png" alt="">
				</a>
			</div>
		
		</div>
	</div>
	<div class="login-wrap d-flex align-items-center flex-wrap justify-content-center">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-md-6 col-lg-7">
					<img src="themes/deskapp/vendors/images/login/login-illust.png" alt="">
				</div>
				<div class="col-md-6 col-lg-5">
					<div class="login-box bg-white box-shadow border-radius-10">
						<div class="login-title">
							<h2 class="text-center text-primary">Tizimga kirish</h2>
						</div>
						<form method="POST" action="{{ route('login') }}">
                            @csrf
							<div class="select-role">
								<div class="btn-group btn-group-toggle" data-toggle="buttons">
								
								</div>
							</div>
							<div class="input-group custom">
                                <input id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" 
                                required autocomplete="username" autofocus placeholder="Loginni kiriting">
								<div class="input-group-append custom">
									<span class="input-group-text"><i class="icon-copy dw dw-user1"></i></span>
                                </div>
                                @error('username')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
							</div>
							<div class="input-group custom">
                                <input type="password" class="form-control @error('password') is-invalid @enderror" 
                                name="password" required autocomplete="current-password" placeholder="Parolni kiriting">
								<div class="input-group-append custom">
									<span class="input-group-text"><i class="dw dw-padlock1"></i></span>
                                </div>
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
							</div>
							<div class="row pb-30">
							
							</div>
							<div class="row">
								<div class="col-sm-12">
									<div class="input-group mb-0">	
										<input class="btn btn-primary btn-lg btn-block" type="submit" value="Kirish">
                                    </div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- js -->
	<script src="themes/deskapp/vendors/scripts/core.js"></script>
	<script src="themes/deskapp/vendors/scripts/script.min.js"></script>
	<script src="themes/deskapp/vendors/scripts/process.js"></script>
    <script src="themes/deskapp/vendors/scripts/layout-settings.js"></script>
    
</body>
</html>