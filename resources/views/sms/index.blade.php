@extends('layouts.app')

@section('content')

<div class="pd-20 card-box mb-30">
		<div class="clearfix mb-20">
			<div class="text-center">
				<h4 class="text-blue h4">Sms jo'natish</h4>
			</div>
		</div>
        <div class="tab">
         
            <div class="tab-content">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if (\Session::has('success'))
                    <div class="alert alert-success">
                        <ul>
                            <li>{!! \Session::get('success') !!}</li>
                        </ul>
                    </div>
                @endif
                <form method="POST" action="{{ route('sms.send') }}">
                    @csrf
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <label>Xabar matni</label>
                                <textarea class="form-control" rows="2" cols="50" name="message"></textarea>
                            </div>
                        </div>
                        <div class="col-md-4">
                        <div class="form-group">
                            <label class="col-form-label">Mijoz raqami</label>
                            <input class="form-control" type="text" name="phones[]">
                        </div>
                        <br>
                        <div class="custom-control custom-checkbox mb-5">
                            <input type="checkbox" class="custom-control-input" id="customCheck1-1" name="for_all">
                            <label class="custom-control-label" for="customCheck1-1">Barcha mijozlarga yuborish</label>
                        </div>

                        <button type="submit" class="btn btn-success">Yuborish</button>
                    </div>
                    </div>
                    <br>   
                 </form>
            </div>
        </div>
</div>
@endsection