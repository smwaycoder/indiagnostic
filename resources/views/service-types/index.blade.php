@extends('layouts.app')

@section('content')
<div class="pd-20 card-box mb-30">
	<div class="clearfix mb-20">
		<div class="text-center">
			<h4 class="text-blue h4">Xizmat turlari</h4>
		</div>
		<div class="pull-right">
			<a href="{{ route('service-types.create') }}" class="btn btn-primary btn-sm scroll-click">Xizmat turi qo'shish  </a>
		</div>
	</div>
	<div class="table-responsive">
		<table class="table table-bordered table-striped" id="myTable">
			<thead>
				<tr>
					<th>#</th>
					<th>Xizmat turi</th>
					<th>Hamkorlar foizi</th>
					<th>Qo'shilgan vaqt</th>
					
                    <th>So'nggo yangilangan vaqt</th>
                    
					<th></th> 
				</tr>
			</thead>
			<tbody>
				@forelse($serviceTypes as $type)
					<tr>
						<td>{{ $loop->index+1 }}</td>
                    	<td> {{ $type->name }}</td>
						<td> {{ $type->doctor_percentage }}</td>
                    	<td> {{ $type->created_at }}</td>
						<td> {{ $type->updated_at }}</td>
                        <td><a href="{{ route('service-types.edit', ['service_type' => $type->id])}}" class="btn btn-success">Tahrirlash</a></td>
					</tr>
				@empty
					<tr>
						<td colspan="6">Ma'lumotlar topilmadi</td>
					</tr>
				@endforelse	
			</tbody>
			
		</table>
	</div>
</div>	
@endsection