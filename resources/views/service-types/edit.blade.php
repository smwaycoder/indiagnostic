@extends('layouts.app')
@section('content')
	<div class="pd-20 card-box mb-30">
		<div class="clearfix mb-30">
			<div class="text-center">
				<h4 class="text-blue h4"> Xizmatni tahrirlash  </h4>
			</div>
		</div>
		@if ($errors->any())
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
				@if ($errors->has('email'))
				@endif
			</div>
		@endif
		<form method="POST" action="{{ route('service-types.update', ['service_type' => $serviceType]) }}">
			@csrf
			@method('PUT')
			<div class="clearfix mb-15 mt-15">
				<div class="pull-left">
					<h4 class="text-blue h4">  </h4>
				</div>
			</div>
			<div id="services">
				<div class="row">
					<div class="col-md-4 col-sm-12">
						<div class="form-group">
							<input class="form-control"  type="text"  placeholder="Xizmat turi nomi" name="name" value="{{ $serviceType->name }}">
						</div>
					</div>
					<div class="col-md-4 col-sm-12">
						<div class="form-group">
							<input class="form-control"  type="number"  placeholder="Hamkorlar foizi" name="doctor_percentage" value="{{ $serviceType->doctor_percentage }}">
						</div>
					</div>
				</div>
			</div>
			<hr>

			<br>
			<div class="row">
				<div class="form-group offset-11">
					<button type="submit" class="btn btn-primary">Saqlash</button>
				</div>
			</div>
		</form>
	</div>
@endsection
