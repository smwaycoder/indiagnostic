
@extends('layouts.app')

@section('content')
<div class="pd-20 card-box mb-30">
	<div class="clearfix mb-20">
		<div class="text-center">
			<h4 class="text-blue h4">Mijoz sodiqlik kartalari</h4>
		</div>
	</div>
	<div class="table-responsive">
		<table class="table table-bordered table-striped" id="myTable">
			<thead>
				<tr>
					<th>#</th>
					<th>Mijoz nomi</th>
					<th>Karta raqami</th>	
					<th>Mijoz balansi</th>
					<th>Holati</th>	
					<th>Ro'yxatga olingan</th>
				</tr>
			</thead>
			<tbody>
				@forelse($cards as $card)
					<tr>
						<td>{{ $loop->index+1 }}</td>
                    	<td> {{ $card->patient->first_name.' '.$card->patient->last_name }}</td>
                        <td> {{ $card->number }} </td>
						<td> {{ $card->balance }} </td>
						<td> {{ $card->is_active ? 'Faol' : 'Faol emas' }}</td>	
						<td> {{ $card->created_at }}</td>
					</tr>
				@empty
					<tr>
						<td colspan="6">Ma'lumotlar topilmadi</td>
					</tr>
				@endforelse	
			</tbody>
			
		</table>
	</div>
</div>	
@endsection


	<!-- <script src="/vendor/datatables/buttons.server-side.js"></script> -->