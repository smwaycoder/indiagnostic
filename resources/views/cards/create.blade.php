
@extends('layouts.app')
@section('content')
	<div class="pd-20 card-box mb-30">
		<div class="clearfix mb-30">
			<div class="text-center">
				<h4 class="text-blue h4"> Yangi chegirma qo'shish </h4>
			</div>
		</div>
		@if ($errors->any())
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
				@if ($errors->has('email'))
				@endif
			</div>
		@endif
		<form method="POST" action="{{ route('discounts.store') }}">
			@csrf
			<div id="discounts">
				<div class="row">
					<div class="col-md-4 col-sm-12">
						<div class="form-group">
							<input class="form-control"  type="text"  placeholder="Chegirma nomi: Yanvar 20% li" name="name">
						</div>
					</div>
					<div class="col-md-2 col-sm-12">
						<div class="form-group">
							<input type="text" class="form-control"  placeholder="Chegirma miqdori (%)" name="percentage">
						</div>
					</div>
				</div>
			</div>
			<hr>
	
			<br>
			<div class="row">
				<div class="form-group offset-11">
					<button type="submit" class="btn btn-primary">Saqlash</button>
				</div>
			</div>
		</form>
	</div>
@endsection
