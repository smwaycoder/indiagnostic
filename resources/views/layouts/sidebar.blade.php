<div class="left-side-bar">
    <div class="brand-logo">
        <a href="/">
            <img src="/themes/deskapp/src/images/logo-mydiagnos.png" alt="" class="dark-logo">
            <img src="/themes/deskapp/src/images/logo-mydiagnos.png" alt="" class="light-logo">
        </a>
        <div class="close-sidebar" data-toggle="left-sidebar-close">
            <i class="ion-close-round"></i>
        </div>
    </div>
    <div class="menu-block customscroll">
        <div class="sidebar-menu">
            <ul id="accordion-menu">
                @if(!\Auth::user()->isMainDoctor())
                <li>
                    <a href="/" class="dropdown-toggle no-arrow">
                        <span class="micon dw dw-house-1"></span><span class="mtext">Asosiy sahifa</span>
                    </a>
                </li>
                @endif
                @auth
                @if(\Auth::user()->isAdmin() || \Auth::user()->isReception())
                    <li>
                        <a href="{{ route('patient-services.index')}}" class="dropdown-toggle no-arrow">
                            <span class="micon dw dw-edit2"></span><span class="mtext">Ko'rsatilgan xizmatlar</span>
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('sms.index')}}" class="dropdown-toggle no-arrow">
                            <span class="micon  dw dw-id-card"></span><span class="mtext">Sms</span>
                        </a>
                    </li>
                @endif

                @if(\Auth::user()->isRepresent() || \Auth::user()->isAdmin())

                <li>
                    <a href="{{ route('patient-senders.index')}}" class="dropdown-toggle no-arrow">
                        <span class="micon micon dw dw-group"></span><span class="mtext">Shifokorlar</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('reports.bysenders')}}" class="dropdown-toggle no-arrow">
                        <span class="micon micon dw dw-analytics"></span><span class="mtext">Shifokorlar statistikasi</span>
                    </a>
                </li>
                @endif
                @if(\Auth::user()->isAdmin())
                <li>
                    <a href="{{ route('service-types.index')}}" class="dropdown-toggle no-arrow">
                        <span class="micon micon dw dw-library"></span><span class="mtext">Xizmat turlari</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('users.index')}}" class="dropdown-toggle no-arrow">
                        <span class="micon micon dw dw-user"></span><span class="mtext">Foydalanuvchilar</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('cards.index')}}" class="dropdown-toggle no-arrow">
                        <span class="micon micon dw dw-id-card"></span><span class="mtext">Sodiqlik kartalari</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('discounts.index')}}" class="dropdown-toggle no-arrow">
                        <span class="micon micon dw dw-library"></span><span class="mtext">Chegirmalar</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('services.index')}}" class="dropdown-toggle no-arrow">
                        <span class="micon micon dw dw-list2"></span><span class="mtext">Xizmatlar ro'yxati</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('reports.all')}}" class="dropdown-toggle no-arrow">
                        <span class="micon dw dw-analytics-21"></span><span class="mtext">Umumiy statistika</span>
                    </a>
                </li>

                
                <li>
                    <a href="{{ route('debits.index') }}" class="dropdown-toggle no-arrow">
                        <span class="micon dw dw-layers"></span><span class="mtext">Kirimlar</span>
                    </a>
                </li> 

                <li>
                    <a href="{{ route('service-products.index') }}" class="dropdown-toggle no-arrow">
                        <span class="micon dw dw-shopping-basket"></span><span class="mtext">Xizmat mahsulotlari</span>
                    </a>
                </li> 

                <li>
                    <a href="{{ route('debit-products.index') }}" class="dropdown-toggle no-arrow">
                        <span class="micon dw dw-layers"></span><span class="mtext">Kirim/Chiqim mahsulotlari</span>
                    </a>
                </li> 

                {{-- <li>
                    <a href="{{ route('debits.index')}}" class="dropdown-toggle no-arrow">
                        <span class="micon dw dw-layers"></span><span class="mtext">Hisobotlar</span>
                    </a>
                </li> --}}


                @endif

                @if(\Auth::user()->isMainDoctor())
                  
                    <li>
                        <a href="{{ route('debits.index') }}" class="dropdown-toggle no-arrow">
                            <span class="micon dw dw-layers"></span><span class="mtext">Kirimlar</span>
                        </a>
                    </li> 

                    <li>
                        <a href="{{ route('service-products.index') }}" class="dropdown-toggle no-arrow">
                            <span class="micon dw dw-shopping-basket"></span><span class="mtext">Xizmat mahsulotlari</span>
                        </a>
                    </li> 

                    <li>
                        <a href="{{ route('debit-products.index') }}" class="dropdown-toggle no-arrow">
                            <span class="micon dw dw-layers"></span><span class="mtext">Kirim/Chiqim mahsulotlari</span>
                        </a>
                    </li> 
                @endif
                @endauth

            </ul>
        </div>
    </div>
</div>
