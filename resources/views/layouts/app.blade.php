<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
	<title>MyDiagnostic</title>

	<!-- Site favicon -->

	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Google Font -->
	<link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet">
	<!-- CSS -->
	<link rel="stylesheet" type="text/css" href="/themes/deskapp/vendors/styles/core.css">
	<link rel="stylesheet" type="text/css" href="/themes/deskapp/vendors/styles/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="/themes/deskapp/src/plugins/datatables/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="/themes/deskapp/src/plugins/datatables/css/responsive.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="/themes/deskapp/vendors/styles/style.css">


	
	<!-- switchery css -->
	<link rel="stylesheet" type="text/css" href="/themes/deskapp/src/plugins/switchery/switchery.min.css">

	<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.6.5/css/buttons.dataTables.min.css">
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
	<!-- Global site tag (gtag.js) - Google Analytics -->
	
	<script src="https://cdn.ckeditor.com/ckeditor5/24.0.0/decoupled-document/ckeditor.js"></script>
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-119386393-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'UA-119386393-1');
	</script>

</head>
<body class="sidebar-white">
	<!-- <div class="pre-loader">
		<div class="pre-loader-box">
			<div class="loader-logo"><img src="/themes/deskapp/src/images/logo.jpg" alt="" width="200" height=120"></div>
			<div class='loader-progress' id="progress_div">
				<div class='bar' id='bar1'></div>
			</div>
			<div class='percent' id='percent1'>0%</div>
			<div class="loading-text">
				Загружается...
			</div>
		</div>
	</div> -->
    @include('layouts.header')
    @include('layouts.sidebar')
    <div class="main-container">
		@yield('content')
    </div>

    
<!-- js -->
<script src="/themes/deskapp/vendors/scripts/core.js"></script>
	<script src="/themes/deskapp/vendors/scripts/script.min.js"></script>
	<script src="/themes/deskapp/vendors/scripts/process.js"></script>
	<script src="/themes/deskapp/vendors/scripts/layout-settings.js"></script>
	<script src="/themes/deskapp/src/plugins/apexcharts/apexcharts.min.js"></script>
	<script src="/themes/deskapp/src/plugins/datatables/js/jquery.dataTables.min.js"></script>
	<script src="/themes/deskapp/src/plugins/datatables/js/dataTables.bootstrap4.min.js"></script>
	<script src="/themes/deskapp/src/plugins/datatables/js/dataTables.responsive.min.js"></script>
	<script src="/themes/deskapp/src/plugins/datatables/js/responsive.bootstrap4.min.js"></script>

	<!-- switchery js -->
	<script src="/themes/deskapp/src/plugins/switchery/switchery.min.js"></script>
	
	<script src="/themes/deskapp/vendors/scripts/advanced-components.js"></script>

	<script src="/js/features.js"></script>

	<script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>


	<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>

	
	<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
	<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>
	<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script>
	
	<script src="/themes/deskapp/vendors/scripts/dashboard.js"></script>
	

	<!-- add sweet alert js & css in footer -->
	<script src="/themes/deskapp/src/plugins/sweetalert2/sweetalert2.all.js"></script>
	<script src="/themes/deskapp/src/plugins/sweetalert2/sweet-alert.init.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.3/jquery.inputmask.bundle.min.js"></script>

	<script src="/js/dinamicForm.js"></script>

	<script>
		$(document).ready(function(){
			$(":input").inputmask();
			/*
			or    Inputmask().mask(document.querySelectorAll("input"));*/
			});
		$('#myTable').DataTable( {
			dom: 'Bfrtip',
			buttons: [
				'excel', 'print'
			],
			"pageLength": 40
			
		} );
	</script>

	@yield('javascripts')
</body>
</html>