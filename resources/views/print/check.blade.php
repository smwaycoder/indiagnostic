<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>

    <style>
        .text-center{
            text-align: center
        }
        table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
        }
        th, td {
        padding: 5px;
        }
        th {
        text-align: left;
        }
        .seq {
        text-align: center !important;
        }
        .line {
            margin: 15px 0;
            border: 1px dashed black;
            width: 100%;
        }
        .price {
            width: 100px;
        }
    </style>
</head>

<body>
    <center>
        <p><img src="/images/logoprint.png" alt="" width="200"></p>
        <p>Ф.И.О: <b> {{ $patient->last_name.' '. $patient->first_name }} </b> </p>
        <p>Год рождения: <b>{{ $patient->birth_date }}</b> </p>
        <p><b>Услуги:</b></p>   
        <table>
            <thead>
                <tr>
                    <th>Наименование</th>
                    <th class="price">Цена</th>
                    <th>Очередь</th>
                </tr>
            </thead>
            <tbody>
				@forelse($patient->services as $patientService)
					<tr>
						<td> {{ $patientService->service->name }}</td>
						<td class="price"> {{ substr($patientService->price,0,-3) }}</td>
						<td class="seq"> {{ $patientService->sequence }}</td>
					</tr>
				@empty
					<tr>
						<td colspan="6">Ma'lumotlar topilmadi</td>
					</tr>
				@endforelse	
			</tbody>
        </table>

        <div class="line"> </div> 
        <p>
            <b>Итого:</b>
            &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 
            <b> {{ number_format($patient->services->sum('price'),0,' ', ' ') }}</b>
        </p>

    </center>
    <form action="{{ route('payment.confirm') }}" method="post" id="confirmForm">
		@csrf
		<input type="hidden" name="patient_id" value="{{ $patient->id }}">
	</form>
    
    <script>
        window.print();
        setTimeout(function(){ document.getElementById("confirmForm").submit(); }, 1000);
        

    </script>
</body>

</html>