@extends('layouts.app')

@section('content')
<div class="pd-20 card-box mb-30">
	<div class="clearfix mb-20">
		<div class="text-center">
			<h4 class="text-blue h4">Ko'rsatilgan xizmatlar ro'yxati</h4>
		</div>
		@if(!\Auth::user()->isAdmin())
			<div class="pull-right">
				<a href="{{ route('patients.create') }}" class="btn btn-primary btn-sm scroll-click">Xizmat qo'shish  </a>
			</div>
		@endif
	</div>
	<div class="table-responsive">
		<table class="table table-bordered table-striped" id="patientServices">
			<thead>
				<tr>
					<th>#</th>
					<th>Mijoz ismi</th>
					<th>Telefon raqam</th>
					<th>Xizmat turi</th>
					<th>Xizmat nomi</th>
					<th>Qo'shilgan vaqt</th>
					<th style="width:85px !important">Xizmat narxi</th>
					<th>Xizmat holati</th>
					<th>Amal</th>
					
				</tr>
			</thead>
			<tbody>
				@forelse($patientServices as $patientService)
					<tr>
						<td>{{ $loop->index+1 }}</td>
						<td>
							<a href="{{ route('patient-services.profile',['patient_service'=> $patientService->id]) }}">
								{{ $patientService->patient->last_name.' '.$patientService->patient->first_name }}
							</a>
						</td>
						<td> {{ $patientService->patient->phone }} </td>
						
						<td> {{ $patientService->service->type->name }}</td>
						<td> {{ $patientService->service->name }}</td>
						<td> {{ $patientService->created_at }}</td>
						<td style="width:85px !important"> {{ $patientService->service->price }}</td>
						<td> {{ $patientService->status->name }}</td>
							<td> 
								@if(\Auth::user()->isReception())				
									<a href="{{ route('repetition.create', ['patient_id'=>$patientService->patient->id ]) }}" class="btn btn-primary btn-sm scroll-click"> Qo'shish </a> <br><br>
								@endif
		
								@if((\Auth::user()->isReception() && $patientService->status->name == "Ro'yxatga olindi") || (\Auth::user()->isAdmin()) )
									<form action="{{ route('patient-services.destroy', ['patient_service'=>$patientService->id ]) }}" id="deleteForm" method="post">
										@method('delete')
										@csrf
										<button type="submit" class="btn btn-danger btn-sm" onclick="return confirm('Ishonchingiz komilmi?')" > O'chirish </button> 
									</form>
								@endif
							</td>
					</tr>
				@empty
					<tr>
						<td colspan="6">Ma'lumotlar topilmadi</td>
					</tr>
				@endforelse	
			</tbody>
			
		</table>
	</div>
</div>	
@endsection
