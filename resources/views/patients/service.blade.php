@extends('layouts.patient')

@section('content')

	<div class="mb-20 text-left"><a href="/">Ortga qaytish</a></div>
	
	<div class="pd-20 card-box mb-30">
    <div class="clearfix mb-20">
		
		<div class="clearfix mb-20">
			<div class="text-center">
				<h4 class="text-blue h4">Xizmat haqida batafsil ma'lumot</h4>
			</div>
		</div>
		<div class="table-responsive-sm">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>#</th>
						<th>Nomi</th>
						<th>Narxi</th>
						<th>Holati</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td> 1 </td>
						<td> {{ $patientService->service->name }}</td>
						<td> {{ $patientService->service->price }}</td>
						<td> {{ $patientService->status->name }}</td>
					</tr>
				</tbody>
	
			</table>
		</div>
		
		@if($patientService->conclusion)
     
		<div class="col-sm-12 col-md-12 mb-30">
			<div class="card card-box">
				<div class="card-header">
					<div class="row">
						<div class="col-md-6 col-sm-12 col-xs-12 mb-20">Shifokor xulosasi</div>
						<div class="col-md-6 col-sm-12 col-xs-12">
							<div class="text-right">
								<a href="{{ route('printConclusion', ['service_id' => $patientService->id ]) }}" class="btn btn-success">Xulosani chop etish</a>
							</div>
						</div>
					</div>	
				</div>
				<div class="card-body">
					{!! $patientService->conclusion !!}
				</div>
			</div>
		</div>
		@endif
		@if($patientService->recommendation)
       
		<div class="col-sm-12 col-md-12 mb-30">	
			<div class="card card-box">
				<div class="card-header">
					<div class="row">
						<div class="col-md-6 col-sm-12 col-xs-12 mb-20">Shifokor tavsiyasi</div>
						<div class="col-md-6 col-sm-12 col-xs-12">
							<div class="text-right">
								<a href="{{ route('printRecommendation', ['service_id' => $patientService->id ]) }}" class="btn btn-info">Tavsiyani chop etish</a>
							</div>
						</div>
					</div>	
				</div>
				<div class="card-body">
					{!! $patientService->recommendation !!}
				</div>
			</div>
		</div>
		@endif
    </div>
@endsection
