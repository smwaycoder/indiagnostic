@extends('layouts.patient')
@section('content')
<div class="row">
	<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 mb-30 offset-md-3 offset-lg-3">
		<div class="pd-20 card-box height-100-p">
			<h3>Kabinetga kirish</h3>
			<hr>
			@if ($errors->any())
				<div class="alert alert-danger">
					<ul>
						@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
						@endforeach
					</ul>
				</div>
			@endif
		<form method="get" action={{ route("patient.profile") }}>
			@csrf
			<div class="form-group row">
				<label class="col-sm-12 col-md-4 col-form-label">Sodiqlik karta raqami</label>
				<div class="col-sm-12 col-md-8">
					<input class="form-control" type="text" name="number" placeholder="10000" value="{{ old('number') }}">
				</div>
			</div>
			<div class="form-group row">
				<label class="col-sm-12 col-md-4 col-form-label">Telefon raqam</label>
				<div class="col-sm-12 col-md-8">
					<input class="form-control" name="phone" placeholder="+998900000000" type="search"  value="{{ old('phone') }}">
				</div>
			</div>
			
			<div class="form-group row">
				<button class="btn btn-success btn-block" type="submit">Kabinetga kirish</button>
			</div>
			
		
	
			</form>
		</div>
	</div>
</div>
@endsection