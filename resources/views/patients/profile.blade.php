@extends('layouts.patient')
@section('content')

    <div class="row">
      
        <div class="col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-30">
            <div class="pd-20 card-box height-100-p">
            <a href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">Kabinetdan chiqish</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
                </form>
            </a>
                <div class="profile-photo">
                <img src="data:image/webp;base64,UklGRnIEAABXRUJQVlA4IGYEAABwFgCdASpkAGQAPm0ylUgkIqIhJpTaCIANiWMAebAnN5fBE2+mhVdRL6AHS3TMV75+T2UfuGvFPa33jTH/eUfu3oP9SdbYoAflv/W/bd8emfV6Q/Yf4EP1n61Xok/syUvAtSkS9PekItVcD3M1BFnDHUmASIrnpcLJfyNl2NW7ba/4sz/KbdF83Qs29T7j2aFfKMh81TxzBt1Na2wO0YlbKkB8mMuHIiwkDg8DxelQjnSJN1Vsdqq411WxUAD+/KgAB7r9qx47nWnLVbRw94rWKKMDwy25uiOAtwdIuxI3zhuo4zaT/8cD6SI+MuPjcfGrnvm0MSqAWwP/9U030387IedWgX42Yh6bu8jLPXOLzfGit/IsmqVOmXlOz5N1l4AN4G8BQJ8M8VXVt7vXKiaX37a7GaqBFo+dkq/7Ywz032NtOv/M2beDnHSRGE34lSH5aoh8vgxk10T/iC9YHi0o8RYnuh78++oThh8ihqWbEXWEZJtSoH/8BCvvabD0fG5EYMGdyKYt7U+uaOpeqZXHpwUDNi9+mHniXJD+JIHAmp38N183/RlQovyp8ciT78/6wmIam7TZcI0G7/3JJlxm3OUIcZiCyZ5kd6IVC93DlE+fxRjgffh+wGMXZ2fYPDZDW3sHYwemIX0ojWB0rxptKzqXK9r0oAS3rjOM/YmobxNDiXaF51wF6uh8V/4lr14ZYpG72hsPi5d3Mq2BusPPo7rMCc04H5QEw9HTsPOsozfjNH9GSa8ZgFzDO7iOykbwkDDjZfFKez975bbGhuk484DK6RRlSMxbQmKLawymuzYMzP0PZF9E0Y3RPogNPX22QO08Bn+afjv63BZY0FmKJrPJCbB+h9jtSQNAa4HS9y4Q1Fwu+iM6pEvvMry8u3Hg6on4ijrPqDhY09U+a7ictz5MuptcXdhPoCJEk2vpoWfyuq2BPw1usNiJCsygcJCXTQwOf9DpGVkrZkybQrKTWgs6xznHTTuFqcWUmukUMO6OTW7XzeKACinz3grBizwBhftSUvL7bzViIFB+cofQ36p6Eg1zDxa7zzGdzi8VTddv6/0tWKCC8EcKlLjHMFa6SyGN7E56FKU2YwOxI7T7eegZvM4DbFgGAoBj/UOePAfbBwr1YUAhhFf2cvgcZg+HwjEahOtZ8MF30vGgH3O8mIzri/p46StNogtwFirynKXig843JJexxHzD8A+i/+RB+R265vAN/dkghz/8o1EwMZZWNZO7uKq/akhNj4D0AT+KFEOjSqrJ0iYNdq28+Tp4cF0vegBOj/jZxx2r4NBm3Q0dwLfJr1fZ47IAVqTU8qBICIy6bQdzfCcGkj12k5P8RmSSJKeIZ3nuqfyIfwAIc/rxgPeBeFoTnM5m7UmiHxNKQT7mq7eIqsTyD5WNgIOaEfiMhNiPv7QoaI9nnCjAio/f20kFyQSW+wcEZrccfdBYjaofrxJ8Z36pwFm2ZhgDSbX+e+73WBHLsNAAXxHAQAAAAAAA" alt="" class="dark-logo">
                    <!-- <img src="/images/{{ $patient->icone }}" alt="" class="avatar-photo">  -->
                  
                    <!-- <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-body pd-5">
                                    <div class="img-container">
                                        <img id="image" src="/images/{{ $patient->icone }}" alt="Picture">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> -->
                </div>
                <h5 class="text-center h5 mb-03">{{ $patient->last_name.' '.$patient->first_name }}</h5>
                <div class="profile-info">
                    <ul>
                        <li>
                            <span>Telefon raqami:</span>
                            {{ $patient->phone }}
                        </li>
                        <li>
                            <span>Jinsi:</span>
                            {{ $patient->genderLabel }}
                        </li>
                        @if(isset($patientCard))
                        <li>
                            <span>Sodiqlik kartasi:</span>
                            {{ $patientCard->number }}
                        </li>
                        <li>
                            <span>Karta balansi:</span>
                            {{ $patientCard->balance }}
                        </li>
                        @endif
                    </ul>
                </div>

            </div>
        </div>
        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-12 mb-30">
            <div class="card-box height-100-p overflow-hidden">
                <div class="profile-tab height-100-p">
                    <div class="tab height-100-p">
                        <ul class="nav nav-tabs customtab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#timeline" role="tab">Xizmatlar</a>
                            </li>
                            @if(isset($patientCard))
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tasks" role="tab">Sodiqlik kartasidan to'lovlar</a>
                            </li>
                            @endif
                        </ul>
                        <div class="tab-content">
                            <!-- Timeline Tab start -->
                            <div class="tab-pane fade show active" id="timeline" role="tabpanel">
                                <div class="pd-20 table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Turi</th>
                                                <th>Nomi</th>
                                                <th>Ro'yxatga olingan</th>
                                                <th>Xizmat narxi</th>
                                                <th>Holati</th>
                                                
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @forelse ($patientServices as $patientService)
                                            <tr>
                                                <td> {{ $loop->index+1 }} </td>
                                                <td>
                                                    <a href="{{ route('patient.service-details', ['patient_service' => $patientService->id]); }}">
                                                        {{ $patientService->service->type->name }}
                                                    </a>
                                                </td>
                                                <td> {{ $patientService->service->name }}</td>
                                                <td> {{ $patientService->created_at }}</td>
                                                <td> {{ $patientService->service->price }}</td>                        
                        						<td> {{ $patientService->status->name }}</td>
                                            </tr>
                                            @empty
                                                <tr>
                                                    <td colspan="6">Ma'lumotlar topilmadi</td>
                                                </tr>
                                            @endforelse

                                        </tbody>

                                    </table>
                                </div>
                            </div>
                            <!-- Timeline Tab End -->
                            <!-- Tasks Tab start -->
                            <div class="tab-pane fade" id="tasks" role="tabpanel">
                                <div class="pd-20 profile-task-wrap table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Xizmat turi</th>
                                                <th>Xizmat nomi</th>
                                                <th>Xizmat narxi</th>
                                                <th>Cashback</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @php $index=1; @endphp
                                            @forelse ($patientServices as $patientService)
                                            @if($patientService->discount_price)
                                                <tr>
                                                    <td> {{ $index; }} </td>
                                                    <td> {{ $patientService->service->type->name }}</td>
                                                    <td> {{ $patientService->service->name }}</td>
                                                    <td> {{ $patientService->service->price }}</td>
                                                    <td> {{ $patientService->discount_price }}</td>
                                                </tr>
                                                @php $index+=1; @endphp
                                            @endif
                                            @empty
                                                <tr>
                                                    <td colspan="6">Ma'lumotlar topilmadi</td>
                                                </tr>
                                            @endforelse

                                        </tbody>
                                      
                                    </table>
                                  
                                </div>
                            </div>
                            {{ $patientServices->links() }}
                            <!-- Tasks Tab End -->
                            <!-- Setting Tab start -->
                            <div class="tab-pane fade height-100-p" id="setting" role="tabpanel">
                                <div class="profile-setting">
                                    <form>
                                        <ul class="profile-edit-list row">
                                            <li class="weight-500 col-md-6">
                                                <h4 class="text-blue h5 mb-20">Edit Your Personal Setting</h4>
                                                <div class="form-group">
                                                    <label>Full Name</label>
                                                    <input class="form-control form-control-lg" type="text">
                                                </div>
                                                <div class="form-group">
                                                    <label>Title</label>
                                                    <input class="form-control form-control-lg" type="text">
                                                </div>
                                                <div class="form-group">
                                                    <label>Email</label>
                                                    <input class="form-control form-control-lg" type="email">
                                                </div>
                                                <div class="form-group">
                                                    <label>Date of birth</label>
                                                    <input class="form-control form-control-lg date-picker" type="text">
                                                </div>
                                                <div class="form-group">
                                                    <label>Gender</label>
                                                    <div class="d-flex">
                                                    <div class="custom-control custom-radio mb-5 mr-20">
                                                        <input type="radio" id="customRadio4" name="customRadio" class="custom-control-input">
                                                        <label class="custom-control-label weight-400" for="customRadio4">Male</label>
                                                    </div>
                                                    <div class="custom-control custom-radio mb-5">
                                                        <input type="radio" id="customRadio5" name="customRadio" class="custom-control-input">
                                                        <label class="custom-control-label weight-400" for="customRadio5">Female</label>
                                                    </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label>Country</label>
                                                    <select class="selectpicker form-control form-control-lg" data-style="btn-outline-secondary btn-lg" title="Not Chosen">
                                                        <option>United States</option>
                                                        <option>India</option>
                                                        <option>United Kingdom</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>State/Province/Region</label>
                                                    <input class="form-control form-control-lg" type="text">
                                                </div>
                                                <div class="form-group">
                                                    <label>Postal Code</label>
                                                    <input class="form-control form-control-lg" type="text">
                                                </div>
                                                <div class="form-group">
                                                    <label>Phone Number</label>
                                                    <input class="form-control form-control-lg" type="text">
                                                </div>
                                                <div class="form-group">
                                                    <label>Address</label>
                                                    <textarea class="form-control"></textarea>
                                                </div>
                                                <div class="form-group">
                                                    <label>Visa Card Number</label>
                                                    <input class="form-control form-control-lg" type="text">
                                                </div>
                                                <div class="form-group">
                                                    <label>Paypal ID</label>
                                                    <input class="form-control form-control-lg" type="text">
                                                </div>
                                                <div class="form-group">
                                                    <div class="custom-control custom-checkbox mb-5">
                                                        <input type="checkbox" class="custom-control-input" id="customCheck1-1">
                                                        <label class="custom-control-label weight-400" for="customCheck1-1">I agree to receive notification emails</label>
                                                    </div>
                                                </div>
                                                <div class="form-group mb-0">
                                                    <input type="submit" class="btn btn-primary" value="Update Information">
                                                </div>
                                            </li>
                                            <li class="weight-500 col-md-6">
                                                <h4 class="text-blue h5 mb-20">Edit Social Media links</h4>
                                                <div class="form-group">
                                                    <label>Facebook URL:</label>
                                                    <input class="form-control form-control-lg" type="text" placeholder="Paste your link here">
                                                </div>
                                                <div class="form-group">
                                                    <label>Twitter URL:</label>
                                                    <input class="form-control form-control-lg" type="text" placeholder="Paste your link here">
                                                </div>
                                                <div class="form-group">
                                                    <label>Linkedin URL:</label>
                                                    <input class="form-control form-control-lg" type="text" placeholder="Paste your link here">
                                                </div>
                                                <div class="form-group">
                                                    <label>Instagram URL:</label>
                                                    <input class="form-control form-control-lg" type="text" placeholder="Paste your link here">
                                                </div>
                                                <div class="form-group">
                                                    <label>Dribbble URL:</label>
                                                    <input class="form-control form-control-lg" type="text" placeholder="Paste your link here">
                                                </div>
                                                <div class="form-group">
                                                    <label>Dropbox URL:</label>
                                                    <input class="form-control form-control-lg" type="text" placeholder="Paste your link here">
                                                </div>
                                                <div class="form-group">
                                                    <label>Google-plus URL:</label>
                                                    <input class="form-control form-control-lg" type="text" placeholder="Paste your link here">
                                                </div>
                                                <div class="form-group">
                                                    <label>Pinterest URL:</label>
                                                    <input class="form-control form-control-lg" type="text" placeholder="Paste your link here">
                                                </div>
                                                <div class="form-group">
                                                    <label>Skype URL:</label>
                                                    <input class="form-control form-control-lg" type="text" placeholder="Paste your link here">
                                                </div>
                                                <div class="form-group">
                                                    <label>Vine URL:</label>
                                                    <input class="form-control form-control-lg" type="text" placeholder="Paste your link here">
                                                </div>
                                                <div class="form-group mb-0">
                                                    <input type="submit" class="btn btn-primary" value="Save & Update">
                                                </div>
                                            </li>
                                        </ul>
                                    </form>
                                </div>
                            </div>
                            <!-- Setting Tab End -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
