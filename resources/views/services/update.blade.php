@extends('layouts.app')
@section('content')
	<div class="pd-20 card-box mb-30">
		<div class="clearfix mb-30">
			<div class="text-center">
				<h4 class="text-blue h4"> Xizmatni tahrirlash  </h4>
			</div>
		</div>
		@if ($errors->any())
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
				@if ($errors->has('email'))
				@endif
			</div>
		@endif
		<form method="POST" action="{{ route('services.update', ['service' => $service->id]) }}">
            @csrf
            {{ method_field('PUT') }}
			<div class="clearfix mb-15 mt-15">
				<div class="pull-left">
					<h4 class="text-blue h4">  </h4>
				</div>
			</div>
			<div id="services">
				<div class="row">
					<div class="col-md-3 col-sm-12">
						<div class="form-group">
							<select class="custom-select form-control serviceType" name="service_type_id" readonly="true" style="pointer-events: none;">
								@foreach($serviceTypes as $serviceType)
									<option value="{{ $serviceType->id }}" {{ ( $service->service_type_id == $serviceType->id ) ? 'selected' : '' }}>{{ $serviceType->name }}</option>
								@endforeach
							</select>
						</div>
                    </div>
                    @if($service->diagnostic_type_id)
                        <div class="col-md-3 col-sm-12 diagnosticBox0">
                            <div class="form-group">
                            <select class="custom-select form-control diagnosticType" readonly="true" style="pointer-events: none;" id="diagnosticType0" name="diagnostic_type_id">
                                <option value="{{ $service->diagnostic_type_id }}">{{ $service->diagnostic->name }}</option>
                            </select>
                            </div>
                        </div>
                    @endif
					
					<div class="col-md-4 col-sm-12">
						<div class="form-group">
							<input class="form-control"  type="text"  value="{{ $service->name }}" name="name">
						</div>
					</div>
					<div class="col-md-2 col-sm-12">
						<div class="form-group">
							<input type="text" class="form-control"  value="{{ str_replace(' ', '', str_replace(',','.',$service->price)) }}" name="price">
						</div>
					</div>
                </div>
                <div class="row">
                    <div class="col-md-3">
                        <label for="active">Xizmat faolligi </label>

                        <!-- <input type="checkbox" name=""checked data-size="small" class="switch-btn" data-color="#0099ff"> -->
                        
                        <select name="active" class="form-control" id="active">
                            <option value="1" {{ $service->active ? 'selected' : ''}}>Faol</option>
                            <option value="0" {{ $service->active ? '' : 'selected'}}>Faol emas</option>
                        </select>
                       
                    </div>


                </div>
			</div>
			<hr>
	
			<br>
			<div class="row">
				<div class="form-group offset-11">
					<button type="submit" class="btn btn-primary">Saqlash</button>
				</div>
			</div>
		</form>
	</div>
@endsection
