@extends('layouts.app')
@section('content')
	<div class="pd-20 card-box mb-30">
		<div class="clearfix mb-30">
			<div class="text-center">
				<h4 class="text-blue h4"> Yangi xizmat qo'shish </h4>
			</div>
		</div>
		@if ($errors->any())
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
				@if ($errors->has('email'))
				@endif
			</div>
		@endif
		<form method="POST" action="{{ route('services.store') }}">
			@csrf

			<div class="clearfix mb-15 mt-15">
				<div class="pull-left">
					<h4 class="text-blue h4">  </h4>
				</div>
			</div>
			<div id="services">
				<div class="row">
					<div class="col-md-3 col-sm-12">
						<div class="form-group">
							<select class="custom-select form-control serviceType" name="service_type_id">
								<option value="">Xizmatni turini tanlang</option>
								@foreach($serviceTypes as $service)
									<option value="{{ $service->id }}">{{ $service->name }}</option>
								@endforeach
							</select>
						</div>
					</div>
					<div class="col-md-3 col-sm-12 diagnosticBox0 hidden">
						<div class="form-group">
						<select class="custom-select form-control diagnosticType" id="diagnosticType0" name="diagnostic_type_id">
							<option value="">Diagnostika turini tanlang</option>
						</select>
						</div>
					</div>
					
					<div class="col-md-4 col-sm-12">
						<div class="form-group">
							<input class="form-control"  type="text"  placeholder="Xizmat nomi" name="name">
						</div>
					</div>
					<div class="col-md-2 col-sm-12">
						<div class="form-group">
							<input type="text" class="form-control"  placeholder="Xizmat narxi: 125000" name="price">
						</div>
					</div>
				</div>
			</div>
			<hr>
	
			<br>
			<div class="row">
				<div class="form-group offset-11">
					<button type="submit" class="btn btn-primary">Saqlash</button>
				</div>
			</div>
		</form>
	</div>
@endsection
