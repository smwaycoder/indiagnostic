@extends('layouts.app')

@section('content')
<div class="pd-20 card-box mb-30">
	<div class="clearfix mb-20">
		<div class="text-center">
			<h4 class="text-blue h4">Xizmatlar ro'yxati</h4>
		</div>
		<div class="pull-right">
			<a href="{{ route('services.create') }}" class="btn btn-primary btn-sm scroll-click">Xizmat qo'shish  </a>
		</div>
	</div>
	<div class="table-responsive">
		<table class="table table-bordered table-striped" id="myTable">
			<thead>
				<tr>
					<th>#</th>
					<th>Xizmat nomi</th>
					<th>Xizmat turi</th>
					<th>Xizmat narxi</th>
					<th>Xizmat holati</th>
					<th>Xizmat qo'shilgan vaqt</th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				@forelse($services as $service)
					<tr>
						<td>{{ $loop->index+1 }}</td>
                    	<td> {{ $service->name }}</td>
                        <td> {{ $service->type->name }}</td>
						<td> {{ $service->price }}</td>
						<td> {{ $service->active ? 'Faol' : 'Faol emas' }}</td>	
						<td> {{ $service->created_at }}</td>
						<td><a href="{{ route('services.edit', ['service'=>$service->id])}}" class="btn btn-success">Tahrirlash</a></td>
					</tr>
				@empty
					<tr>
						<td colspan="6">Ma'lumotlar topilmadi</td>
					</tr>
				@endforelse	
			</tbody>
			
		</table>
	</div>
</div>	
@endsection


	<!-- <script src="/vendor/datatables/buttons.server-side.js"></script> -->