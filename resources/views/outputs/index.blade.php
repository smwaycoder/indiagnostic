
@extends('layouts.app')

@section('content')
<div class="pd-20 card-box mb-30">
	<div class="clearfix mb-20">
		<div class="text-center">
			<h4 class="text-blue h4">Chiqimlar ro'yxati</h4>
		</div>
	</div>
	<div class="table-responsive">
		<table class="table table-bordered table-striped" id="myTable">
			<thead>
				<tr>
					<th>#</th>
                    <th>Mijoz</th>
                    <th>Xizmat turi</th>
                    <th>Xizmat</th>       
					<th>Mahsulot nomi</th>
					<th>Mahsulot soni</th>
					<th>Summa</th>
					<th>Chiqim vaqti</th>
				</tr>
			</thead>
			<tbody>
				@forelse($outputs as $item)
					<tr>
						<td>{{ $loop->index + 1 }}</td>
                        <td> {{ $item->patientService->patient->fullName }}</td>
                        <td> {{ $item->patientService->serviceType->name }}</td>
                        <td> {{ $item->patientService->service->name }}</td>
						<td> {{ $item->debitProduct->name }}</td>
						<td> {{ $item->amount }}</td>
						<td> {{ $item->created_at }}</td>
					</tr>
				@empty
					<tr>
						<td colspan="8">Ma'lumotlar topilmadi</td>
					</tr>
				@endforelse
			</tbody>

		</table>
	</div>
</div>
@endsection


	<!-- <script src="/vendor/datatables/buttons.server-side.js"></script> -->
