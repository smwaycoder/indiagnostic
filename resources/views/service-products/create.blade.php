
@extends('layouts.app')
@section('content')
	<div class="pd-20 card-box mb-30">
		<div class="clearfix mb-30">
			<div class="text-center">
				<h4 class="text-blue h4"> Xizmatga yangi mahsulot biriktirish </h4>
			</div>
		</div>
		@if ($errors->any())
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
				@if ($errors->has('email'))
				@endif
			</div>
		@endif
		<form method="POST" action="{{ route('service-products.store') }}">
			@csrf
			<div id="discounts">
				<div class="row">
					<div class="col-md-4 col-sm-12">
						<div class="form-group">
							<select name="service_type_id" class="form-control serviceTypeSelect" id="service_type_id">
                                <option value="">Xizmat turini tanlang</option>
                                @foreach($serviceTypes as $item)
                                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
						</div>
					</div>
					<div class="col-md-4 col-sm-12">
						<div class="form-group">
							<select name="service_id" class="form-control serviceNameSelect" id="service_id">
                                <option value="">Xizmat nomi</option>
                            </select>
						</div>
					</div>
				</div>
				<div id="serviceProducts">	
					<div class="row">
						<div class="col-md-4 col-sm-12">
							<div class="form-group">
								<select name="serviceProducts[0][debit_product_id]" class="form-control" id="debit_product_id">
									<option value="">Mahsulotni tanlang</option>
									@foreach($products as $item)
										<option value="{{ $item->id }}">{{ $item->name }}</option>
									@endforeach	

								</select>
							</div>
						</div>
						<div class="col-md-3 col-sm-12">
							<div class="form-group">
								<input class="form-control"  type="number"  placeholder="Kerakli miqdor" name="serviceProducts[0][required_amount]">
							</div>
						</div>
						<div class="col-md-2">
							<button  type="button" class="btn btn-success" id="addServiceProduct"><i class="fa fa-plus"></i></button>
							<button  type="button" class="btn btn-danger" id="rmServiceProduct"><i class="fa fa-minus"></i></button>
						</div>
					</div>
				</div>
			</div>
			<hr>

			<br>
			<div class="row">
				<div class="form-group offset-11">
					<button type="submit" class="btn btn-primary">Saqlash</button>
				</div>
			</div>
		</form>
	</div>
@endsection
