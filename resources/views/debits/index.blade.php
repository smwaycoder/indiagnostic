
@extends('layouts.app')

@section('content')
<div class="pd-20 card-box mb-30">
	<div class="clearfix mb-20">
		<div class="text-center">
			<h4 class="text-blue h4">Kirimlar ro'yxati</h4>
		</div>
		<div class="pull-right">
			<a href="{{ route('debits.create') }}" class="btn btn-primary btn-sm scroll-click">Kirim qilish  </a>
		</div>
	</div>
	<div class="table-responsive">
		<table class="table table-bordered table-striped" id="myTable">
			<thead>
				<tr>
					<th>#</th>
					<th>Mahsulot nomi</th>
					<th>Mahsulot soni</th>
					<th>Qolgan mahsulotlar soni</th>
					<th>Summa</th>
					<th>Kirim vaqti</th>
					{{-- <th>Amallar</th> --}}
				</tr>
			</thead>
			<tbody>
				@forelse($debits as $item)
					<tr>
						<td>{{ $loop->index + 1 }}</td>
						<td> {{ $item->debitProduct->name }}</td>
						<td> {{ $item->amount }}</td>
						<td> {{ $item->remained_amount }}</td>
						<td> {{ $item->price }}</td>
						<td> {{ $item->created_at }}</td>
						
                        {{-- <td class="d-flex">
                            <a href="" class="btn btn-sm btn-success mr-2">Tahrirlash</a>
                            <form action="{{ route('debits.destroy',  $item->id) }}" method="post" onsubmit="return confirm('Сиз ростдан ҳам ушбу маълумотни ўчиришни хохлайсизми ?')">
                                @csrf
                                @method('delete')
                                <button type="submit" class="btn btn-sm btn-danger">
                                    <span class="">O'chirish</span>
                                </button>
                            </form>
                        </td> --}}
					</tr>
				@empty
					<tr>
						<td colspan="8">Ma'lumotlar topilmadi</td>
					</tr>
				@endforelse
			</tbody>

		</table>
	</div>
</div>
@endsection


	<!-- <script src="/vendor/datatables/buttons.server-side.js"></script> -->
