
@extends('layouts.app')
@section('content')
	<div class="pd-20 card-box mb-30">
		<div class="clearfix mb-30">
			<div class="text-center">
				<h4 class="text-blue h4"> Yangi kirim qilish </h4>
			</div>
		</div>
		@if ($errors->any())
			<div class="alert alert-danger">
				<ul>
					@foreach ($errors->all() as $error)
					<li>{{ $error }}</li>
					@endforeach
				</ul>
				@if ($errors->has('email'))
				@endif
			</div>
		@endif
		<form method="POST" action="{{ route('debits.store') }}">
			@csrf
			<div id="discounts">
				<div class="row">
                    <div class="col-md-4 col-sm-12">
						<div class="form-group">
							<select name="debit_product_id" class="form-control" id="debit_product_id">
                                <option value="">Mahsulotni tanlang</option>
								@foreach($products as $item)
									<option value="{{ $item->id }}">{{ $item->name }}</option>
								@endforeach	

                            </select>
						</div>
					</div>
                    <div class="col-md-3 col-sm-12">
                        <div class="form-group">
							<input class="form-control"  type="number"  placeholder="Mahsulot soni" name="amount">
						</div>
                    </div>
                    <div class="col-md-3 col-sm-12">
                        <div class="form-group">
							<input class="form-control"  type="number"  placeholder="Mahsulot narxi" name="price">
						</div>
                    </div>
				</div>
			</div>
			<hr>

			<br>
			<div class="row">
				<div class="form-group offset-11">
					<button type="submit" class="btn btn-primary">Saqlash</button>
				</div>
			</div>
		</form>
	</div>
@endsection
