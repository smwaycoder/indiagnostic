<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class CitiesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cities = [
            ['region_id' => '22', 'name_uz' => 'Yunusobod tumani', 'name_ru' => 'Юнусабадский район', 'name_en' => 'Yunusobod district', 'name_cyrl' => 'Юнусобод тумани', 'c_order' => '184', 'ns11_code' => '3', 'soato' => '1726266'],
            ['region_id' => '22', 'name_uz' => 'Mirobod tumani', 'name_ru' => 'Мирабадский район', 'name_en' => 'Mirabad district', 'name_cyrl' => 'Миробод тумани', 'c_order' => '185', 'ns11_code' => '1', 'soato' => '1726273'],
            ['region_id' => '22', 'name_uz' => 'Yakkasaroy tumani', 'name_ru' => 'Яккасарайский район', 'name_en' => 'Yakkasaray district', 'name_cyrl' => 'Яккасарой тумани', 'c_order' => '186', 'ns11_code' => '4', 'soato' => '1726287'],
            ['region_id' => '22', 'name_uz' => 'Olmazor tumani', 'name_ru' => 'Алмазарский район', 'name_en' => 'Olmazor district', 'name_cyrl' => 'Олмазор тумани', 'c_order' => '187', 'ns11_code' => '9', 'soato' => '1726280'],
            ['region_id' => '22', 'name_uz' => 'Bektemir tumani', 'name_ru' => 'Бектемирский район', 'name_en' => 'Bektemir district', 'name_cyrl' => 'Бектемир тумани', 'c_order' => '188', 'ns11_code' => '11', 'soato' => '1726264'],
            ['region_id' => '22', 'name_uz' => 'Yashnobod tumani', 'name_ru' => 'Яшанабадский район', 'name_en' => 'Yashnobod district', 'name_cyrl' => 'Яшнобод тумани', 'c_order' => '189', 'ns11_code' => '8', 'soato' => '1726290'],
            ['region_id' => '22', 'name_uz' => 'Chilonzor tumani', 'name_ru' => 'Чиланзарский район', 'name_en' => 'Chilanzar district', 'name_cyrl' => 'Чилонзор тумани', 'c_order' => '190', 'ns11_code' => '6', 'soato' => '1726294'],
            ['region_id' => '22', 'name_uz' => 'Uchtepa tumani', 'name_ru' => 'Учтепинский район', 'name_en' => 'Uchtepa district', 'name_cyrl' => 'Учтепа тумани', 'c_order' => '191', 'ns11_code' => '10', 'soato' => '1726262'],
            ['region_id' => '22', 'name_uz' => 'Mirzo Ulug’bek tumani', 'name_ru' => 'Мирзо- Улугбекский район', 'name_en' => 'Mirzo Ulugbek district', 'name_cyrl' => 'Мирзо Улуғбек тумани', 'c_order' => '192', 'ns11_code' => '2', 'soato' => '1726269'],
            ['region_id' => '22', 'name_uz' => 'Sergeli tumani', 'name_ru' => 'Сергелийский район', 'name_en' => 'Sergeli district', 'name_cyrl' => 'Сергели тумани', 'c_order' => '193', 'ns11_code' => '7', 'soato' => '1726283'],
        ];
  
        DB::table('cities')->insert($cities);
    }
}
