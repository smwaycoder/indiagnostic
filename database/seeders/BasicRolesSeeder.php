<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\User;

class BasicRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $roles = [
            'reception' => 'Qabulxona',
            'cashier' => 'Kassir',
            'doctor' => 'Shifokor',
            'laboratory' => 'Labaratoriya',
            'admin'=>'Administrator'
        ];
        
        foreach ($roles as $role => $description) {
            $userRole = Role::create(['name' => $role]);
            $user = User::create(['name'=>$description, 'username'=>$role, 'password'=>\bcrypt($role.'123'), 'email'=>$role.'@gmail.com']);    
 
            $user->assignRole($userRole);
        }
        
        
    }
}
