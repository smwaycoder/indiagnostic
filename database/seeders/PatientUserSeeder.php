<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use App\Models\User;
use App\Models\Patient;
use Spatie\Permission\Models\Role;


class PatientUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $patients = Patient::whereNull('user_id')->get();
       

        $userRole = Role::firstOrCreate(['name' => 'patient']);
            
        foreach ($patients as $patient)
        {
            $password = rand(111111,999999);
            $phone = substr($patient->phone, -9);
               
            if(!str_contains($patient->phone, 'x') && !User::where('username', $phone)->exists()){
                
                if(strlen($phone) == 9 && $phone != 'xxxxxxxxx'){
                    try {
                        $user = User::create([
                            'name'=> $patient->fullName,
                            'username'=> $phone,
                            'password'=> \bcrypt($password),
                            'email'=> $password.'@gmail.com'
                        ]);  

                      } catch (\Exception $e) {
                        info($phone);
                      }
                      
                           
                    $user->assignRole($userRole);
        
                    $patient->user_id =  $user->id;
                    $patient->update();
       
             
                }
            }
            
        }
    }
}
