<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToPatientSendersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('patient_senders', function (Blueprint $table) {
            $table->string('uuid')->nullable();
            $table->string('telegram_user_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('patient_senders', function (Blueprint $table) {
            $table->dropColumn('uuid');
            $table->dropColumn('telegram_user_id');
        });
    }
}
