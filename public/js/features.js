$(document).on('change', '.statisticRange', function() {

    var range = $(this).val();
    var statisticLabels = {
        'today': 'Bugungi kun statistikasi',
        'yesterday': 'Kechagi kun statistikasi',
        'weekly': 'Joriy hafta statistikasi',
        'monthly': 'Joriy oy statistikasi',
        'yearly': 'Joriy yil statistikasi',
        'all': 'To\'liq statistika'
    };

    $.ajax({
        url: '/dashboard/' + range,
        success: function(result) {
            $('#all_patients').html(result.all_patients);
            if (result.all_benefit == null) {
                result.all_benefit = 0
            }
            var all_benefit = parseInt(result.all_benefit).toLocaleString();

            $('#all_benefit').html(all_benefit + ' so\'m');
            $('#all_services').html(result.all_services);
            $('#statistic_label').html(statisticLabels[range]);
        }
    });
    // var currentPrice  =  parseFloat($('.servicePrice'+counter).val());
    // $('.servicePrice'+counter).val('');
    // oldPrice =  localStorage.getItem('allPrice');
    // localStorage.setItem('allPrice', oldPrice-currentPrice);

});

$(document).on('change', '.serviceType', function(event) {
    var service_type_id = $(this).val();
    var counter = $(this).attr('name').substr(9, 1);

    if (isNaN(parseInt(counter))) {
        counter = 0;
    }
    if (service_type_id == 1) {
        console.log('.diagnosticBox' + counter)
        $('.diagnosticBox' + counter).removeClass('hidden');
        $.ajax({
            url: '/resources/diagnostics/' + service_type_id,
            success: function(result) {
                $('#diagnosticType' + counter).html(result);

            }
        });
        // $('.serviceNames' + counter).html('');
    } else {
        $('.diagnosticBox' + counter).addClass('hidden');
    }
    $.ajax({
        url: '/resources/services/' + service_type_id,
        success: function(result) {
            $('select[name="services[' + counter + '][serviceName]"]').html(result);
        }
    });


    $('.servicePrice' + counter).val('');
});



$(document).on('change', '.serviceTypeSelect', function(event) {

    var service_type_id = $(this).val();

    $.ajax({
        url: '/resources/services/' + service_type_id,
        success: function(result) {
            $('.serviceNameSelect').html(result);
        }
    });
});

$(document).on('change', '.diagnosticType', function() {

    var counter = $(this).attr('name').substr(9, 1);

    var diagnostic_type_id = $(this).val();
    $.ajax({
        url: '/resources/services/1/' + diagnostic_type_id,
        success: function(result) {
            $('#serviceNames' + counter).html(result);
        }
    });
    // var currentPrice  =  parseFloat($('.servicePrice'+counter).val());
    // $('.servicePrice'+counter).val('');
    // oldPrice =  localStorage.getItem('allPrice');
    // localStorage.setItem('allPrice', oldPrice-currentPrice);

});


$(document).on('keyup', '#patients', function() {
    alert(123);
    // $value = $(this).val();
    // $.ajax({
    //     type : 'get',
    //     url : '/search-patients/{phone}',
    //     success:function(data){
    //         $('#patients').html(data);
    //     }
    // });
})



$(document).on('change', '.serviceNames', function() {
    var counter = $(this).attr('name').substr(9, 1);

    var price = $(this).find(':selected').data('price').split(',')[0];
    var diagnostic_type = $(this).find(':selected').data('diagnostic');


    $('.servicePrice' + counter).val(price);
    $('#diagnosticType' + counter).val(diagnostic_type);

    var priceSum = 0;
    $('.price').each(function() {
        priceSum += parseFloat(this.value.replace(/\s/g, ''));
    });

    if (priceSum >= 250000) {
        $('.loyality').removeClass('hidden');
    } else {
        $('.loyality').addClass('hidden');
    }

    let validPrice = priceSum.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ');


    console.log($('.discount').val());
    if ($('.discount').val()) {
        let discount = parseFloat($('.discount').val().replace(/\s/g, ''));
        var forPayment;
        forPayment = priceSum - discount;
        forPayment = forPayment.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ');

        $('.forPayment').val(forPayment);

    }


    $('.allPrice').val(validPrice);


});



$(document).ready(function() {
    localStorage.removeItem('allPrice');

    var region = $('.region_id').val();

    if (region > 0) {
        setCity(region)
    }

    $('.region_id').change(function() {
        var region_id = $(this).val();
        setCity(region_id)
    });

    $('.serviceType').change(function() {
        var service_type_id = $(this).val();

        // if (service_type_id == 1) {
        //     $('.diagnosticBox').removeClass('hidden');
        //     $.ajax({
        //         url: '/resources/diagnostics/' + service_type_id,
        //         success: function(result) {
        //             $('.diagnosticType').html(result);
        //         }
        //     });
        //     $('.serviceNames').html('');
        // } else {
        //     $.ajax({
        //         url: '/resources/services/' + service_type_id,
        //         success: function(result) {
        //             $('.serviceNames').html(result);
        //         }
        //     });
        //     $('.diagnosticBox').addClass('hidden');
        // }

        $('.servicePrice').val('');
    });

    $('.diagnosticType').change(function() {

        var diagnostic_type_id = $(this).val();
        $.ajax({
            url: '/resources/services/1/' + diagnostic_type_id,
            success: function(result) {
                $('.serviceNames').html(result);
            }
        });
        $('.servicePrice').val('');

    });

    $('.serviceNames').change(function() {

        var price = $(this).find(':selected').data('price');
        $('.servicePrice').val(price);

    });



    function setCity(region_id) {
        $.ajax({
            url: '/resources/cities/' + region_id,
            success: function(result) {
                $('.city_id').html(result);
            }
        });
    }



});