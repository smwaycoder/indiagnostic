var counter = 1;

$(document).on('click', '#addService', function(event) {
    // if (counter >= 5) {
    //     alert("Faqatgina 5 ta holatni qo'shishingiz mumkin");
    //     return false;
    // }

    //creating divs
    var newRowDiv = $(document.createElement('div'))
        .attr("class", 'row');
    var serviceType = $(document.createElement('div'))
        .attr("class", 'col-md-2  adding' + counter);
    var diagnosticType = $(document.createElement('div'))
        .attr("class", 'col-md-3 diagnostic_type hidden diagnosticBox' + counter + ' adding' + counter);
    var serviceName = $(document.createElement('div'))
        .attr("class", 'col-md-4 service_name adding' + counter);

    var servicePrice = $(document.createElement('div'))
        .attr("class", 'col-md-1 service_price adding' + counter);

    var formGroup = '<div class="form-group"><div class="input-wrapper">';
    var firstFtml = formGroup + '<select class="custom-select form-control serviceType" id="serviceType' + counter + '" name="services[' + counter + '][serviceType]">';
    firstFtml = firstFtml + '</select><span class="required">*</span></div></div>';



    var formGroup = '<div class="form-group"><div class="input-wrapper">';
    var secondHtml = formGroup + '<select class="custom-select form-control diagnosticType" readonly="true" style="pointer-events:none"  id="diagnosticType' + counter + '" name="services[' + counter + '][diagnosticType]">';
    secondHtml = secondHtml + '<option value="">Diagnostika turini tanlang</option>';
    secondHtml = secondHtml + '</select><span class="required">*</span></div></div>';

    var formGroup = '<div class="form-group"><div class="input-wrapper">';
    var thirdHtml = formGroup + '<select class="custom-select2 form-control serviceNames" style="width: 100%; height: 38px;" id="serviceNames' + counter + '" name="services[' + counter + '][serviceName]">';
    thirdHtml = thirdHtml + '<option value="">Xizmatni tanlang</option>';
    thirdHtml = thirdHtml + '</select><span class="required">*</span></div></div>';

    var formGroup = '<div class="form-group">';
    var fourthHtml = formGroup + '<input type="text" class="form-control price servicePrice' + counter + '" placeholder=""  readonly="" name="services[' + counter + '][servicePrice]"></div>';

    serviceType.after().html(firstFtml);
    diagnosticType.after().html(secondHtml);
    serviceName.after().html(thirdHtml);
    servicePrice.after().html(fourthHtml);


    $.ajax({
        url: '/resources/service-types',
        success: function(result) {
            $('#serviceType' + (counter - 1)).html(result);
        }
    });
    newRowDiv.appendTo("#services");
    serviceType.appendTo(newRowDiv);
    serviceName.appendTo(newRowDiv);
    diagnosticType.appendTo(newRowDiv);
    servicePrice.appendTo(newRowDiv);
    $('.serviceNames').select2();
    counter++;
});

$(document).on('click', '#rmService', function(event) {
    if (counter == 1) {
        alert("Bu amalni bajara olmaysiz");
        return false;
    }
    counter--;
    $(".adding" + counter).remove();

    var priceSum = 0;
    $('.price').each(function() {
        priceSum += parseFloat(this.value.replace(/\s/g, ''));
    });

    if (priceSum >= 250000) {
        $('.loyality').removeClass('hidden');
    } else {
        $('.loyality').addClass('hidden');
    }


    let validPrice = priceSum.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ');

    if ($('.discount').val()) {
        let discount = parseFloat($('.discount').val().replace(/\s/g, ''));


        var forPayment;
        forPayment = priceSum - discount;
        $('.forPayment').val(forPayment);

    }

    priceSum = priceSum.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ');
    forPayment = forPayment.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ' ');

    $('.allPrice').val(validPrice);

});


// dinamic form for assigning products to service start

$(document).on('click', '#addServiceProduct', function(event) {
    // if (counter >= 5) {
    //     alert("Faqatgina 5 ta holatni qo'shishingiz mumkin");
    //     return false;
    // }

    //creating divs
    var newRowDiv = $(document.createElement('div'))
        .attr("class", 'row');
    var debitProduct = $(document.createElement('div'))
        .attr("class", 'col-md-4 col-sm-12 adding' + counter);
    var amount = $(document.createElement('div'))
        .attr("class", 'col-md-3 col-sm-12 adding' + counter);


    var formGroup = '<div class="form-group">';
    var firstFtml = formGroup + '<select class="custom-select form-control"  id="debit_product_id' + counter + '" name="serviceProducts[' + counter + '][debit_product_id]">';
    firstFtml = firstFtml + '<option value="">Mahsulotni tanlang</option>';
    firstFtml = firstFtml + '</select></div>';

    var formGroup = '<div class="form-group">';
    var secondHtml = formGroup + '<input class="form-control"  type="number"  placeholder="Kerakli miqdor" name="serviceProducts[' + counter + '][required_amount]"></div>';

    debitProduct.after().html(firstFtml);
    amount.after().html(secondHtml);

    $.ajax({
        url: '/resources/debit-products',
        success: function(result) {
            $('#debit_product_id' + (counter - 1)).html(result);
        }
    });
    newRowDiv.appendTo("#serviceProducts");
    debitProduct.appendTo(newRowDiv);
    amount.appendTo(newRowDiv);

    $('.serviceNames').select2();

    counter++;
});





// dinamic form for assigning products to service end



$(document).on('click', '#rmServiceProduct', function(event) {
    if (counter == 1) {
        alert("Bu amalni bajara olmaysiz");
        return false;
    }
    counter--;
    $(".adding" + counter).remove();
});