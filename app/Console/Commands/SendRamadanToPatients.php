<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\SmsService;
use App\Models\User;

class SendRamadanToPatients extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'patients:send-ramadan';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(SmsService $smsService)
    {
        $users = User::where('id','>', 9)->get();
        $counter = 0; 


        foreach ($users as $user)
        {
            if($user->patient)
            {
               $phone = $user->username;
               if(strlen($phone) == 9 && (substr($phone,0,2) != 71)){
                    $params['phones'] = [$phone];
                    
                    $params['message'] = "VARIKOZDAN aziyat chekayapsizmi?Faqat 30-aprel ProMed klinikasida 12 yillik tajribaga ega flebolog Mirazim Miryulchievdan BEPUL konsultatsiya.Telefon: 712006777";
                    $resultCode = $smsService->send($params);

                    $params['message'] = "Zamuchilis’ ot VARIKOZA?Tol’ko 30-aprelya v klinike ProMed BESPLATNAYA konsultaciya flebologa s 12-letnim stajem Mirazima Miryulchieva.Zapis’po nomeru:712006777";
                    $resultCode = $smsService->send($params);

                    $smsSended =  ($resultCode == 200) ? true : false;
                    $counter++;
                    echo 'Sended: '.$counter.PHP_EOL;
                    
                    if($smsSended){
                        echo 'Sended phone : '.$phone.PHP_EOL;
                    }else{
                        echo 'Not Sended phone : '.$phone.PHP_EOL;
                        info('Not Sended phone : '.$phone);
                    }
                }
            }

        }
    }
}
