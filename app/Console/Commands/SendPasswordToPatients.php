<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\SmsService;
use App\Models\User;

class SendPasswordToPatients extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'patients:send-password';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(SmsService $smsService)
    {
        $users = User::whereNull('sended_pass')->get();
        $counter = 0; 
        foreach ($users as $user)
        {
            if($user->patient)
            {
               $phone = $user->username;
               if(strlen($phone) == 9 && (substr($phone,0,2) != 71)){
                    $params['phones'] = [$phone];
                    $password = substr($user->email, 0, 6);
                    $params['message'] = 'MyDiagnosticdan tekshiruv natijalarini bilish uchun kabinetingizga kirish: bit.ly/3XCguiK Login: '.$phone.', Parol: '.$password.'. Bizni kuzatib boring: bit.ly/3lubdw7';
                    $resultCode = $smsService->send($params);

                    $smsSended =  ($resultCode == 200) ? true : false;
                    $user->sended_pass = $smsSended;
                    $user->update();
                }
            }

            $counter++;

            echo 'Sended: '.$counter.PHP_EOL;
        }
    }
}
