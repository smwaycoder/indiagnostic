<?php 

namespace App\Repositories;

use App\Models\Patient;
use App\Models\PatientService;
use Illuminate\Support\Facades\DB;


class PatientServiceRepository {

    private $model;

    public function __construct(PatientService $model)
    {
        $this->model = $model;
    }
    public function patientsForCashier()
    {
        $users = DB::table('patient_services')
                ->select(DB::raw('patient_id as id, first_name, last_name, sum(price) as price'))
                ->leftJoin('patients', 'patients.id', '=', 'patient_id')
                ->where('service_status_id',$this->model::STATUS_REGISTERED)
                ->whereNull('deleted_at')
                ->groupBy('patient_id','first_name','last_name')
                ->get();

        return $users;


    }

    public function getPatientServices($patient_id)
    {
        return Patient::where('id',$patient_id)->with('services')->first();
        // return PatientService::registered()->with('patient', 'service')->where('patient_id', $patient_id)->get();
    }

    public function getPayedServices($service_type_id = 1)
    {
        $patientServices = PatientService::payed()->matchedServices($service_type_id)->with('patient', 'service')->paginate(100);
        
        return $patientServices; 
    }

    public function getSendedPatients($patient_sender_id, $filter)
    {


        $patientServices = PatientService::payedOrConclused()->with('patient', 'service','service.type')->where('patient_sender_id', $patient_sender_id);

        if(isset($filter['start_date'])){
            $patientServices->whereDate('created_at', '>=', $filter['start_date']);
        }

        if(isset($filter['end_date'])){
            $patientServices->whereDate('created_at', '<=', $filter['end_date']);
        }
        
        return $patientServices->get(); 
    }
}