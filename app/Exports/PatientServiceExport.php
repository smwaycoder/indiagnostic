<?php 

namespace App\Exports;

use App\Models\PatientService;
use Maatwebsite\Excel\Concerns\FromCollection;

use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithMapping;
use Carbon\Carbon;

use PhpOffice\PhpSpreadsheet\Style\Style;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

use Maatwebsite\Excel\Concerns\WithStyles;



class PatientServiceExport implements FromCollection,WithHeadings, ShouldAutoSize,WithMapping,WithStyles
{
    protected $status;
    protected $fullName;
    protected $phone;
    protected $dateFrom;
    protected $dateTo;
    
    
    // Add more filter parameters as needed...

    public function __construct($status, $fullName, $phone, $dateFrom, $dateTo)
    {
        $this->status = $status;
        $this->fullName = $fullName;
        $this->phone = $phone;
        $this->dateFrom = $dateFrom;
        $this->dateTo = $dateTo;
        
        // Initialize other filter parameters...
    }


    public function collection()
    {
        $query =  PatientService::with('patient', 'service','service.type','status','patientSender','patient.user');

        if($this->status){
            $query->where('service_status_id',$this->status);
        }

            
        if ($this->phone) {
         
            $query->whereHas('patient', function($q) {
                $q->where('phone', 'like', "%{$this->phone}%");
            });
        }

        if ($this->fullName) {
          
            $query->whereHas('patient', function($q){
                $q->whereRaw("CONCAT(first_name, ' ', last_name) LIKE ?", ["%{$this->fullName}%"]);
            });
            // Filter by the combined full_name (first_name + last_name)
         
        }

        if ($this->dateFrom) {
            $query->whereDate('created_at', '>=', Carbon::parse($this->dateFrom)->toDateString());
        }
    
        if ($this->dateTo) {
            $query->whereDate('created_at', '<=', Carbon::parse($this->dateTo)->toDateString());
        }
       // filters

       return $query->orderBy('created_at','desc')->get();
    }

    public function headings(): array
    {
        // Define Excel headings
        return [
            'Mijoz ismi',
            'Telefon raqami',
            'Xizmat turi',
            'Xizmat nomi',
            'Shifokor',
            'Qo\'shilgan vaqti',
            'Xizmat narxi',
            'Xizmat holati',
            'Manbasi'
            // Add more columns as needed...
        ];
    }

    public function map($patientService): array
    {
        return [
            $patientService->patient->full_name,
            $patientService->patient->phone,
            $patientService->service->type->name,
            $patientService->service->name,
            ($patientService->patientSender)  ? $patientService->patientSender->fullname : '',  
            $patientService->created_at,
            number_format($patientService->price, 0, '', '  '),
            $patientService->status->name,
            $patientService->source,
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return [
            // Style the first row (headers) to be bold
            1 => ['font' => ['bold' => true]],
        ];
    }
}

