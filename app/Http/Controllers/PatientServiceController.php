<?php

namespace App\Http\Controllers;

use App\DataTables\PatientServicesDataTable;
use App\Models\PatientService;
use App\Models\ServiceType;
use App\Models\Patient;
use App\Models\PatientSender;
use App\Models\User;
use App\Models\PatientCard;
use App\Repositories\PatientServiceRepository;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Services\PatientService as ServicePatient;
use App\Events\GivedConclusion;
use App\Events\PatientServicePayed;
use App\Jobs\NewOutputRegisteredJob;
use App\Services\FileService;
use DOMDocument;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\PatientServiceExport;




class PatientServiceController extends Controller
{

    private $repo;
    private $model;
    private $servicePatient;

    public function __construct(PatientServiceRepository $repo, PatientService $model, ServicePatient $servicePatient)
    {
        $this->repo = $repo;
        $this->model = $model;
        $this->servicePatient = $servicePatient;
        
        ini_set('memory_limit', '-1');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function index(PatientServicesDataTable $dataTable)
    // {

    //     // $patientServices = PatientService::with('patient', 'service')->orderBy('created_at','desc')->get();
    //     // // dd($patientServices[0]->status);
    //     // return view('patients.index', [
    //     //     'patientServices' => $patientServices
    //     // ]);


    //     return $dataTable->render('patient-services.index');


    //     // return view('patients.index');
    // }

    public function index(Request $request)
    {
        $perPage = 30;

        $filters = $request->validate([
            'full_name' => 'nullable',
            'phone' => 'nullable',
            'status' => 'nullable',
            'start_date' => 'nullable',
            'end_date' => 'nullable',
            'service_type' => 'nullable',
        ]);

        // if($filters){
        //     return $filters;
        // }

       
        $query =  PatientService::with('patient', 'service','service.type','status','patientSender','patient.user');

        if(isset($filters['status'])){
            $query->where('service_status_id',$filters['status']);
        }

            
        if ($request->filled('phone')) {
         
            $query->whereHas('patient', function($q) use($request){
                $q->where('phone', 'like', '%' . $request->input('phone') . '%');
            });
        }

        if ($request->filled('full_name')) {
            $fullName = $request->input('full_name');
            $query->whereHas('patient', function($q) use($fullName){
                $q->whereRaw("CONCAT(first_name, ' ', last_name) LIKE ?", ["%$fullName%"]);
            });
            // Filter by the combined full_name (first_name + last_name)
         
        }

        if ($request->filled('date_from')) {
            $query->whereDate('created_at', '>=', Carbon::parse($request->input('date_from'))->toDateString());
        }
    
        if ($request->filled('date_to')) {
            $query->whereDate('created_at', '<=', Carbon::parse($request->input('date_to'))->toDateString());
        }
       // filters

       $patientServices = $query->orderBy('created_at','desc')->paginate($perPage);



        // $patientServices = PatientService::with('patient', 'service')->orderBy('created_at','desc')->get();
        // // dd($patientServices[0]->status);
       
        return view('patient-services.list', [
            'patientServices' => $patientServices,
            'perPage' => $perPage
        ]);


      


        // return view('patients.index');
    }

    public function patientsForCashier()
    {
        $patients =  $this->repo->patientsForCashier();

        return view('patient-services.payment.index', [
            'patients' => $patients
        ]);
    }

    public function getPatientServices($patient_id)
    {

        $patient = $this->repo->getPatientServices($patient_id);

        return view('patient-services.payment.confirm', [
            'patient' => $patient
        ]);
    }

    public function paymentConfirm(Request $request)
    {
        $patient_id = $request->input('patient_id');
        $patientServices = PatientService::where('patient_id',$patient_id)->registered();

        
        $patientServices->each(function ($item){
        
           $updateItem =  $item->update(['service_status_id'=> $this->model::STATUS_PAYED]);
            
           if($updateItem){
                event(new PatientServicePayed($item));
           }

           NewOutputRegisteredJob::dispatch($item);
        });



        if($patientServices){
            return redirect()->route('patient-services.cashier');
        }

        return 'Error occured';
    }

    public function saveConclusion(Request $request)
    {

        $inputs = $request->validate([
            'patient_service_id'=> 'required|integer',
            'conclusion'=> 'nullable',
            'conclusion_file' => 'nullable|mimes:pdf|max:4096', 
            'recommendation' => 'nullable'
        ]);

     

        $patientService = $this->model::findOrFail($inputs['patient_service_id']);

        $recommendation = (isset($inputs['recommendation']) && strlen($inputs['recommendation'])) >= 40 ?  $inputs['recommendation'] : null;
    

        $patientService->update([
            'conclusion'=>  $inputs['conclusion'],
            'service_status_id' => $this->model::STATUS_CONCLUSED,
            'recommendation' => $recommendation
        ]);

        if($patientService){

            if($request->hasFile('conclusion_file')){
                $file = $request->file('conclusion_file');

                // Generate a unique name for the file
                $fileName = '/conclusions/conclusion_file_' . time() . '.' . $file->getClientOriginalExtension();

                // Store the file in the storage/app/public directory
                if($file->storeAs('public', $fileName)){

                    $patientService->update([
                        'conclusion_file'=>  $fileName,
                    ]);

                    event(new GivedConclusion($patientService));
                }
            } else {
                $saveFile = FileService::saveConclusion($patientService);
          
                if($saveFile){
                    event(new GivedConclusion($patientService));
                }
            }
          
        
            if(auth()->user()->isReception()){
                return redirect()->route('patient-services.index');        
            }
            
            return redirect()->route('patient-services.payed');
        }

        return 'Error occured';
    }

    public function getPayedServices()
    {
        $user  = Auth::user();
        $service_type_id = $user->doctor->service_type_id;
        $patientServices = $this->repo->getPayedServices($service_type_id);

        return view('patient-services.conclusion.index', [
            'patientServices' => $patientServices
        ]);
    }

    public function forPatient($patientService)
    {
        
        $user = auth()->user();

        if($user && $user->patient){
            
            $patientIDs = Patient::where('user_id', $user->id)->pluck('id')->toArray();
            $patientService = PatientService::where('id',$patientService)->with('patient','service','status')->first();

            if(in_array($patientService->patient_id, $patientIDs)){
                 return view('patients.service',[
                    'patientService' => $patientService
                ]);
            }

        }
        
        abort(404);
      
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($patient_id = null)
    {
        $serviceTypes = ServiceType::get();
        $patientSenders = PatientSender::active()->get();
        $patientCards = PatientCard::with('patient')->get();
        $lastCard = $patientCards->sortBy('created_at')->toArray();
        $lastCardNumber = 10000;
        if(count($patientCards)){
            $lastCardNumber = end($lastCard)['number'];
        }
        if($patient_id) {
            $patient = Patient::find($patient_id);
        }
        
        return view('patient-services.create', [
            'services' => $serviceTypes,
            'patient' => ($patient_id) ? $patient : null,
            'patientSenders' => $patientSenders,
            'patientCards' => $patientCards,
            'lastCardNumber' => $lastCardNumber
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $inputs = $request->validate([
            'pass_seria'=> 'nullable',
            'pass_number'=> 'nullable',
            'first_name'=>'required',
            'last_name'=>'required',
            'patronymic'=>'nullable',
            'birth_date'=>'required',
            'gender'=>'required',
            'phone'=>'required|unique:patients',
            'services' => 'required',
            'doctor' => 'nullable',
            'patient_sender_id' => 'nullable',
            'loyality_card_number' => 'integer|nullable',
            'chain_id' => 'nullable',
            'source' => 'required',
        ]);

        // if($request->has(['pass_seria', 'pass_number'])){
        //     $inputs['passport'] = $inputs['pass_seria'].$inputs['pass_number'];
        // }

        $fields  = Arr::except($inputs,['_token', 'services', 'source','doctor', 'patient_sender_id','loyality_card_number','chain_id']);

        $patient = Patient::create($fields);
        
        if($patient){
            $phone = substr($patient->phone, -9);
            if(!User::where('username',$phone)->exists()){
                $this->servicePatient->createUser($patient);
            }
        }
        // dd($request->all());
        if($patient && $request->has('loyality_card_number') && $request->loyality_card_number &&  $request->has('services'))
        {

           
            $allPrice = array_sum(str_replace(' ', '', array_column($request->services, 'servicePrice')));
            $cashback = $allPrice/20;


            $patientCard = new PatientCard();
            $patientCard->patient_id = $patient->id;
            $patientCard->phone = $patient->phone;
            $patientCard->number = $request->loyality_card_number;
            $patientCard->balance = $cashback;

            if($request->has('chain_id') && $request->chain_id){
                $patientCard->chain_id = $request->chain_id;
                $patientCardRecommended = PatientCard::find($request->chain_id);
                $patientCardRecommended->balance = $patientCardRecommended->balance + $cashback;
                $patientCardRecommended->update();
                //tavsiya qilgan odamga  ham pul tushadigan bo'lishi kerak
            }
            $patientCard->save();

        }

        $services = $request->input('services');
        if($patient && $request->has('services') && count($services)){
    
            foreach($services as $service){
                if(isset($service['serviceName'])){
                    $patientService = new PatientService();
                    $patientService->patient_id = $patient->id;
                    $patientService->service_id = $service['serviceName'];
                    $patientService->service_status_id = $patientService::STATUS_REGISTERED;
                    $patientService->price = $service['servicePrice'];
                    $patientService->doctor = $request->doctor;
                    $patientService->patient_sender_id = $request->patient_sender_id;
                    $patientService->source = $request->source;
                    $patientService->save();
                }

            }

            return redirect()->route('patient-services.index');
        }

        return 'Error occured';
    }



    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PatientService  $patientService
     * @return \Illuminate\Http\Response
     */
    public function show($patientService)
    {
        $patientService = PatientService::where('id',$patientService)->with('patient','service','status')->first();
        return view('patient-services.conclusion.confirm',[
            'patientService' => $patientService
        ]);
    }

    public function profile($patientService)
    {
        $patientService = PatientService::where('id',$patientService)->with('patient','service','status')->first();
        return view('patient-services.profile',[
            'patientService' => $patientService
        ]);
    }

    public function createRepetetion($patient_id)
    {
        $patient = Patient::findOrFail($patient_id);
        $patientSenders = PatientSender::get();
        $services = ServiceType::get();
        $patientCard  = PatientCard::where('patient_id',$patient_id)->first();

        return view('patient-services.repetition',[
            'patient' => $patient,
            'services'=>$services,
            'patientSenders' => $patientSenders,
            'patientCard' => $patientCard
        ]);

    }
    public function storeRepetetion(Request $request)
    {

        $inputs = $request->validate([
            'patient_id' => 'required',
            'services' => 'required',
            'doctor' => 'nullable',
            'patient_sender_id' => 'nullable',
            'discount_price' => 'nullable'

        ]);
        $patient = Patient::find($inputs['patient_id']);
        $patientCard = PatientCard::where('patient_id', $inputs['patient_id'])->first();
        $services = $request->input('services');
        if($patient && $request->has('services') && count($services)){
            
            $this->servicePatient->sendTelegramBotLink($patient);

            foreach($services as $service){

                $price = floatval(str_replace(' ', '', $service['servicePrice']));

                if ($request->has('discount_price') && $request->discount_price){
                    $discount = $patientCard->balance/count($services);
                    $price -= $discount;
                }

                if(isset($service['serviceName'])){
                    $patientService = new PatientService();
                    $patientService->patient_id = $patient->id;
                    $patientService->service_id = $service['serviceName'];
                    $patientService->service_status_id = $patientService::STATUS_REGISTERED;
                    $patientService->price = $price;
                    $patientService->discount_price = ($request->discount_price) ? $discount : 0;
                    $patientService->patient_sender_id = $request->patient_sender_id;
                    $patientService->save();

                    if($request->discount_price){
                        $dicprice = floatval(str_replace(' ', '', $request->discount_price));
                        $patientCard->balance = $patientCard->balance -$dicprice;
                        if($patientCard->balance < 0){
                            $patientCard->balance = 0;
                        }
                        $patientCard->update();
                    }
                }

            }

            return redirect()->route('patient-services.index');
        }

        return 'Error occured';

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PatientService  $patientService
     * @return \Illuminate\Http\Response
     */
    public function edit(PatientService $patientService)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PatientService  $patientService
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PatientService $patientService)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PatientService  $patientService
     * @return \Illuminate\Http\Response
     */
    public function destroy(PatientService $patientService)
    {
        $patientService->delete();


        return redirect()->route('patient-services.index');
    }

    public function getSendedPatients($patient_sender_id, Request $request)
    {
        $filter = $request->validate([
            'start_date' => 'nullable|date',
            'end_date' => 'nullable|date',
        ]);

        $patientSender = PatientSender::findOrFail($patient_sender_id);

        if($patientSender){
            $patientServices =  $this->repo->getSendedPatients($patient_sender_id,$filter);
           
            return view('patient-services.sended.index', [
                'patientServices' => $patientServices,
                'patientSender' => $patientSender
            ]);
        }

        return 'Error occured';

    }

    public function excelExport()
    {
        $status = request('status');
        $fullName = request('full_name');
        $phone = request('phone');
        $dateFrom = request('date_from');
        $dateTo = request('date_to');
        

        return Excel::download(new PatientServiceExport($status,$fullName,$phone,$dateFrom,$dateTo), 'patient_services.xlsx');
    }
}
