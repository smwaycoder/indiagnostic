<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DebitProduct;

class DebitProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $debit_products = DebitProduct::orderBy('id','desc')->get();

        return view('debit-products.index', [
            'debit_products' => $debit_products
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('debit-products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // $rules = [
        //     'name'=> 'required',
        // ];

        // $inputs = $request->validate($rules);
        // $this->$inputs->store($request->all());


        $request->validate([
            'name' => 'required',
        ]);

        DebitProduct::create($request->post());

        return redirect()->route('debit-products.index')->with('success','Company has been created successfully.');
        // return view('debit-products.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DebitProduct::where('id', $id)->delete();
        return redirect()->route('debit-products.index')
                        ->with('success','list deleted successfully');
    }
}
