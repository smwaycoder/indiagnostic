<?php

namespace App\Http\Controllers;

use App\Models\PatientService;
use App\Models\Patient;
use App\Models\Service;
use File;


use Barryvdh\DomPDF\Facade as PDF;


class PrintController extends Controller
{
    public function printConclusion($patient_service_id){

        $patient_service = PatientService::where('id', $patient_service_id)->with('patient','service')->first();

        $pdf = PDF::loadView('pdf.conclusion', [
            'patientService' => $patient_service,
            'content' => $patient_service->conclusion
        ]);
        
        $file_name = $patient_service->patient->last_name.' '.$patient_service->patient->first_name;
        return $pdf->download($file_name.'.pdf');
            
    }
    public function printRecommendation($patient_service_id){

        $patient_service = PatientService::where('id', $patient_service_id)->with('patient','service')->first();

        $content = $patient_service->recommendation;
        $pdf = PDF::loadView('pdf.conclusion', [
            'patientService' => $patient_service,
            'content' =>  $content
        ]);
        
        $file_name = $patient_service->patient->last_name.' '.$patient_service->patient->first_name;
        return $pdf->download($file_name.'.pdf');
            
    }

    public function printCheck($patient_id){

        $patient = Patient::where('id',$patient_id)->with('services')->first();;
    
        return view('print.check',[
            'patient' => $patient
        ]);
    }

    public static function generatePrices($serviceTypeId){

        $services = Service::where('service_type_id',$serviceTypeId)->active()->get();
       


        
        $pdf = PDF::loadView('pdf.prices', [
            'services' => $services,
        ]);
        
        $today = date('Y-m-d');
        $file_name = 'price'.$serviceTypeId.'d'.$today.'.pdf';
        $fullPath = public_path().'/storage/'.$file_name;

        

        if(!File::exists($fullPath)){
            $pdf->save($fullPath);
        }


        return $fullPath;

    }
    
}
