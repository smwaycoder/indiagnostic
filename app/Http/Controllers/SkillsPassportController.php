<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Input;
use App\Http\Requests\SaveServiceRequest;
use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;

class SkillsPassportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getCompetitors(Request $request)
    {
        $expert = $request->expert ?? null;
        $competion = $request->competion ?? null;
        $fullname = $request->fullname ?? null;
        $only = $request->only ?? null;
        $where = "";
        if($fullname) {
            $fullname = str_replace("'", "\'", $fullname);
            $where = "and ((concat(p.first_name,' ',p.last_name) like '%".$fullname."%') or (concat(p.last_name,' ', p.first_name) like '%".$fullname."%'))";
        }
        if($expert) {
            $expert = str_replace("'", "\'", $expert);
            $where .= " and ((concat(pe.first_name,' ',pe.last_name) like '%".$expert."%') or (concat(pe.last_name,' ', pe.first_name) like '%".$expert."%'))";
        }
        if($competion) {
            $where .= " and i18n_value.i18n_id = ".$competion;
        }
        if($only) {
            $where .= " and p.passport is not null";
        }

        $competitors = DB::connection('cis')->select("
                SELECT p.id, p.birth_date, p.passport, p.first_name, p.last_name, i18n_value.text, sum(m.calculated_mark) max_ball, sum(asp.max_mark) as max_mark, 
                    pe.first_name as expert_first_name, pe.last_name as expert_last_name,
                    date(skill.marking_scheme_locked) as exam_date, skill.valid_date as valid_date
                    FROM mark as m 
                    left join person as p on p.id = m.competitor_id 
                    left join person_position as pp on p.id = pp.person_id 
                    left join skill on skill.id = pp.skill_id 
                    left join i18n_value on i18n_value.i18n_id = skill.name_id
                    left join aspect as asp on asp.id = m.aspect_id
                    left join sub_criterion as sub on sub.id = asp.sub_criterion_id
                    left join person pe on pe.id = sub.marking_supervisor_id
                    WHERE m.competitor_id is not null $where and 
                    p.last_name is not null and m.calculated_mark > 0
                    group by m.competitor_id, i18n_value.text, skill.marking_scheme_locked, pe.id, skill.valid_date
                UNION
                SELECT p.id, p.birth_date, p.passport, p.first_name, p.last_name, i18n_value.text, sum(m.calculated_mark) max_ball, sum(asp.max_mark) as max_mark, 
                    pe.first_name as expert_first_name, pe.last_name as expert_last_name, 
                    date(skill.marking_scheme_locked) as exam_date, skill.valid_date as valid_date
                    FROM mark as m
                    left join person_team as pt on pt.team_id = m.competitor_id 
                    left join person as p on pt.person_id = p.id 
                    left join person_position as pp on p.id = pp.person_id
                    left join skill on skill.id = pp.skill_id 
                    left join i18n_value on i18n_value.i18n_id = skill.name_id 
                    left join aspect as asp on asp.id = m.aspect_id 
                    left join sub_criterion as sub on sub.id = asp.sub_criterion_id 
                    left join person pe on pe.id = sub.marking_supervisor_id
                    WHERE m.competitor_id is not null $where and 
                        p.last_name is not null and m.calculated_mark > 0
                        group by m.competitor_id, i18n_value.text, skill.marking_scheme_locked, pe.id, pt.person_id, skill.valid_date
                        order by max_ball desc
            ");

        $competions = DB::connection('cis')->select("
                SELECT i18n_value.text, i18n_value.i18n_id 
                    FROM mark 
                    left join person as p on p.id = mark.competitor_id 
                    left join person_position as pp on p.id = pp.person_id 
                    left join skill on skill.id = pp.skill_id 
                    left join i18n_value on i18n_value.i18n_id = skill.name_id 
                    where i18n_value.text is not null 
                    group by i18n_value.text, i18n_value.i18n_id
                UNION 
                SELECT i18n_value.text, i18n_value.i18n_id 
                    FROM mark
                    left join person_team as pt on pt.team_id = mark.competitor_id 
                    left join person as p on pt.person_id = p.id 
                    left join person_position as pp on p.id = pp.person_id 
                    left join skill on skill.id = pp.skill_id 
                    left join i18n_value on i18n_value.i18n_id = skill.name_id 
                    where i18n_value.text is not null 
                    group by i18n_value.text, i18n_value.i18n_id
            ");
        
        if(count($competitors)) {
            return response()->json([
                'success' => true,
                'result' => $competitors,
                'competions' => $competions
            ]);
        }
        return response()->json([
                'success' => false,
                'result' => []
            ]);
        
    }

    public function getCompetitorResult($id, Request $request)
    {
        $data = [];
        $person = DB::connection('cis')->table('person')->select('*')->where('id', $id)->first();
        $person_team = DB::connection('cis')->select("SELECT team_id, person_id from person_team WHERE person_id = $id");

        if($person) {
            $data['first_name'] = $person->first_name;
            $data['last_name'] = $person->last_name;
            $data['birth_date'] = $person->birth_date;
            $data['passport'] = $person->passport;
            $data['profession'] = $person->profession;

            $place = DB::connection('cis')->select("SELECT translate.text, skill.marking_scheme_locked as date, skill.valid_date as valid_date, skill.exam_place, skill.skill_number
                   FROM person_position as pp
                   left join skill on skill.id = pp.skill_id
                   left join i18n_value as translate on translate.i18n_id = skill.name_id
                   WHERE person_id = $id
            ");

            $skills = DB::connection('cis')->select("SELECT translate.text,sum(mark.calculated_mark) as calculated_mark, sum(asp.max_mark) as max_mark, (sum(asp.max_mark) / 2) as avg_mark,
                (sum(asp.max_mark) / 3) as min_mark
                FROM mark 
                left join aspect as asp on asp.id = mark.aspect_id
                left join ws_standards_spec as ws on ws.id = asp.wsss_id
                left join i18n_value as translate on translate.i18n_id = ws.name_id 
                WHERE competitor_id=$id and translate.text is not null and mark.calculated_mark > 0
                GROUP by translate.text
            ");

            $results = DB::connection('cis')->select("SELECT translate.text,sum(mark.calculated_mark) as calculated_mark, sum(asp.max_mark) as max_mark, (sum(asp.max_mark) / 2) as avg_mark,
            (sum(asp.max_mark) / 3) as min_mark,
             person.first_name, person.last_name
                   FROM mark
                   left join aspect as asp on asp.id = mark.aspect_id
                   left join sub_criterion as sub on sub.id = asp.sub_criterion_id
                   left join criterion as cr on cr.id = sub.criterion_id
                   left join i18n_value as translate on translate.i18n_id = cr.name_id
                   left join person on person.id = sub.marking_supervisor_id
                   WHERE competitor_id=$id and translate.text is not null and mark.calculated_mark > 0
                   GROUP by translate.text, person.first_name, person.last_name
            ");
            if(!empty($place) && !empty($skills) && !empty($results)) {
                $data['speciality'] = $place[0]->text;
                $data['exam_place'] = $place[0]->exam_place;
                $data['skill_number'] = floor($place[0]->skill_number);
                $data['exam_date'] = Carbon::parse($place[0]->date)->format('Y-m-d');
                $data['valid_date'] = Carbon::parse($data['exam_date'])->addYears(2)->format('Y-m-d');

                $max_ball = 0;
                $calculated_ball = 0;
                if(count($results)) {
                    $data['supervisor_last_name'] = $results[0]->last_name;
                    $data['supervisor_first_name'] = $results[0]->first_name;
                    foreach ($results as $result) {
                        $max_ball += $result->max_mark; 
                        $calculated_ball += $result->calculated_mark;
                    }
                }
                

                $data['calculated_ball'] = $calculated_ball;
                $data['max_ball'] = $max_ball;
                $data['measures'] = $results ?? [];
                $data['skills'] = $skills ?? [];

                return response()->json([
                    'success' => true,
                    'result' => $data
                ]);
            } elseif (empty($skills) && isset($person_team[0])) {

                $id = $person_team[0]->team_id;

                $skills = DB::connection('cis')->select("SELECT translate.text,sum(mark.calculated_mark) as calculated_mark, sum(asp.max_mark) as max_mark, (sum(asp.max_mark) / 2) as avg_mark,
                    (sum(asp.max_mark) / 3) as min_mark
                    FROM mark 
                    left join aspect as asp on asp.id = mark.aspect_id
                    left join ws_standards_spec as ws on ws.id = asp.wsss_id
                    left join i18n_value as translate on translate.i18n_id = ws.name_id 
                    WHERE competitor_id=$id and translate.text is not null and mark.calculated_mark > 0
                    GROUP by translate.text
                ");

                $results = DB::connection('cis')->select("SELECT translate.text,sum(mark.calculated_mark) as calculated_mark, sum(asp.max_mark) as max_mark, (sum(asp.max_mark) / 2) as avg_mark,
                (sum(asp.max_mark) / 3) as min_mark,
                 person.first_name, person.last_name
                       FROM mark
                       left join aspect as asp on asp.id = mark.aspect_id
                       left join sub_criterion as sub on sub.id = asp.sub_criterion_id
                       left join criterion as cr on cr.id = sub.criterion_id
                       left join i18n_value as translate on translate.i18n_id = cr.name_id
                       left join person on person.id = sub.marking_supervisor_id
                       WHERE competitor_id=$id and translate.text is not null and mark.calculated_mark > 0
                       GROUP by translate.text, person.first_name, person.last_name
                ");
                if(!empty($place) && !empty($skills) && !empty($results)) {
                    $data['speciality'] = $place[0]->text;
                    $data['exam_place'] = $place[0]->exam_place;
                    $data['skill_number'] = floor($place[0]->skill_number);
                    $data['exam_date'] = Carbon::parse($place[0]->date)->format('Y-m-d');
                    $data['valid_date'] = Carbon::parse($data['exam_date'])->addYears(2)->format('Y-m-d');

                    $max_ball = 0;
                    $calculated_ball = 0;
                    if(count($results)) {
                        $data['supervisor_last_name'] = $results[0]->last_name;
                        $data['supervisor_first_name'] = $results[0]->first_name;
                        foreach ($results as $result) {
                            $max_ball += $result->max_mark;
                            $calculated_ball += $result->calculated_mark;
                        }
                    }                    

                    $data['calculated_ball'] = $calculated_ball;
                    $data['max_ball'] = $max_ball;
                    $data['measures'] = $results ?? [];
                    $data['skills'] = $skills ?? [];

                    return response()->json([
                        'success' => true,
                        'result' => $data
                    ]);
                }

            } else {
                return response()->json([
                    'success' => false,
                    'result' => []
                ]);
            }
            

        } else {
            return response()->json([
                'success' => false,
                'result' => []
            ]);
        }
    }

    public function updateCompetitor(Request $request, $id)
    {
        $person = DB::connection('cis')->table('person')->where('id', $id)->first();
        $person_position = DB::connection('cis')->table('person_position')->where('person_id', $id)->first();

        if($person) {
            $input = $request->all();
            
            $query =  DB::connection('cis')->table('person')->where('id', $id)->update([
                'passport' => $input['passport'],
                'profession' => $input['profession'],
                'first_name' => $input['first_name'],
                'last_name' => $input['last_name'],
                'birth_date' => Carbon::parse($input['birth_date'])->format('Y-m-d'),
            ]);

            $skill = DB::connection('cis')->table('skill')->where('id', $person_position->skill_id)->update([
                'exam_place' => $input['exam_place']
            ]);

            // if (!$query || !$skill) { 
            //     return response()->json([
            //         'success' => false,
            //         'result' => [],
            //         'message'   => 'Error update person'
            //     ]);
            // }

            return response()->json([
                'success' => true,
                'result' => [],
                'message' => 'Updated successfully!' 
            ]);
        } else {
            return response()->json([
                    'success' => false,
                    'result' => [],
                    'message'   => 'Person not found!'
                ]);
        }
    }
}
