<?php

namespace App\Http\Controllers;

use App\Models\Statistic;


class DashboardController extends Controller
{
    public function statistic($range)
    {
        $statistic = Statistic::dashboard($range);

        return response()->json($statistic);
    }
}
