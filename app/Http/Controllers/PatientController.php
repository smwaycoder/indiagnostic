<?php

namespace App\Http\Controllers;

use App\Models\Region;
use App\Models\Patient;
use App\Models\ServiceType;
use App\Models\PatientSender;
use App\Models\PatientCard;
use App\Models\PatientService;
use App\Http\Requests\ProfileLoginRequest;


use Illuminate\Http\Request;

class PatientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $patients = Patient::all()->first();
        return view('patients.index', [
            'patients' => $patients
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($patient_id = null)
    {
        $regions = Region::orderBy('c_order')->get();
        $serviceTypes = ServiceType::get();
        $patientSenders = PatientSender::active()->get();

        if($patient_id) {
            $patient = Patient::find($patient_id);
        }

        return view('patients.create', [
            'regions' => $regions,
            'services' => $serviceTypes,
            'patient' => ($patient_id) ? $patient : null,
            'patientSenders' => $patientSenders
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function profile($code = null)
    {
        return view('patients.profile-login');
    }

    public function profileDetail(ProfileLoginRequest $request)
    {
        $input =  $request->validated();

        // dd($input);
        $patientCard = PatientCard::where('number', '=', $input['number'])->with('patient.services')->first();
 
        if($patientCard){
            $patientServices = PatientService::where('patient_id', $patientCard->patient_id)->paginate(15);

            return view('patients.profile', [
                'patientCard' => $patientCard,
                'patientServices' => $patientServices,
                'patient' => $patientCard->patient
    
            ]);
        }

        abort(404);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
