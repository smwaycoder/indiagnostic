<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\DebitProduct;
use App\Models\DiagnosticType;
use App\Models\Service;
use App\Models\ServiceType;
use Illuminate\Http\Request;

class ResourceController extends Controller
{
    public function getCitiesByRegion($region_id)
    {
        $cities = City::where('region_id', $region_id)->get();

        echo "<option value=''>Tumanni tanlang</option>";
        
        foreach($cities as $city){
            echo "<option value=".$city->id.">".$city->name_cyrl."</option>";
        }

    }


    public function getDiagnosticTypes($service_type_id)
    {
        
        $diagnostics = DiagnosticType::where('service_type_id', $service_type_id)->get();

        echo "<option value=''>Diagnostika turini tanlang</option>";
        
        foreach($diagnostics as $diagnostic){
            echo "<option value=".$diagnostic->id.">".$diagnostic->name."</option>";
        }

    }

    public function getServiceTypes() 
    {
        $serviceTypes = ServiceType::all();
        echo "<option value=''>Xizmat turini tanlang</option>";
        
        foreach($serviceTypes as $type){
            echo "<option value=".$type->id.">".$type->name."</option>";
        }
    }

    
    public function getDebitProducts() 
    {
        $debitProducts = DebitProduct::all();
        echo "<option value=''>Mahsulotni tanlang</option>";
        
        foreach($debitProducts as $debitProduct){
            echo "<option value=".$debitProduct->id.">".$debitProduct->name."</option>";
        }
    }

    public function getServices($service_type_id, $diagnostic_type_id=null)
    {
        
        $serviceQuery = Service::query();
        $serviceQuery =  $serviceQuery->where('service_type_id', $service_type_id);
        if($diagnostic_type_id){
            $serviceQuery = $serviceQuery->where('diagnostic_type_id', $diagnostic_type_id);
        }

        $services = $serviceQuery->active()->get();

        echo "<option value=''>Xizmatni tanlang</option>";
        
        foreach($services as $service){
            echo "<option data-price='$service->price' data-diagnostic='$service->diagnostic_type_id' value=".$service->id.">".$service->name."</option>";
        }

    }

}
