<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Telegram;
use App\Models\TelegramUser;
use App\Models\User;
use App\Models\PatientSender;

use Telegram\Bot\Keyboard\Keyboard;
use  Telegram\Bot\Exceptions\TelegramResponseException;

//services

use App\Services\TelegramBotService;

class TelegramController extends Controller
{
    public $service;


    public function __construct(TelegramBotService $service) {
        $this->service = $service;
    }


    public function handle()
    {
        $this->service->handle();
    }
}

