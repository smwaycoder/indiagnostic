<?php

namespace App\Http\Controllers;

use App\Models\PatientSender;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PatientSenderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $patientSenders =  PatientSender::all();

        return view('patient-senders.index', [
            'patientSenders' => $patientSenders
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('patient-senders.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $rules = [
            'fullname'=> 'required',
            'add_info' => 'nullable'    
        ];

        $inputs = $request->validate($rules);
        $inputs['uuid'] = Str::random(12);
        $patientSender = PatientSender::create($inputs);
        
        if($patientSender){
            return redirect()->route('patient-senders.index');
        }

        return 'Xatolik sodir bo\'ldi';
    
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PatientSender  $patientSender
     * @return \Illuminate\Http\Response
     */
    public function show(PatientSender $patientSender)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PatientSender  $patientSender
     * @return \Illuminate\Http\Response
     */
    public function edit($patientSenderId)
    {
        $patientSender = PatientSender::where('id',$patientSenderId)->first();
     
        return view('patient-senders.update', [
            'patientSender'=> $patientSender
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PatientSender  $patientSender
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PatientSender $patientSender)
    {

        $rules = [
            'fullname'=> 'required',
            'add_info' => 'nullable',
            'is_active' => 'required',
        ];

       
        $inputs = $request->validate($rules);
        
        $isUpdate = $patientSender->update($inputs);

       if($isUpdate){           
        return redirect()->route('patient-senders.index');
       }
       
       return 'Error occured';

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PatientSender  $patientSender
     * @return \Illuminate\Http\Response
     */
    public function destroy(PatientSender $patientSender)
    {
        //
    }
}
