<?php

namespace App\Http\Controllers;

use App\Models\Output;
use App\Http\Requests\StoreOutputRequest;
use App\Http\Requests\UpdateOutputRequest;

class OutputController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $outputs = Output::with('patientService','debitProduct')->get();
 
        return view('outputs.index', [
            'outputs' => $outputs
        ]);
    }

 
}
