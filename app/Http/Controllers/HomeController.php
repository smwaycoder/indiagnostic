<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\Statistic;
use App\Models\PatientService;
use App\Models\Patient;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        $role = $user->getRoleNames()[0];
        
        switch($role){
            case 'patient':
                 if($user->patient){
                    $patientIDs = Patient::where('user_id', $user->id)->pluck('id');
                    $patientServices = PatientService::whereIn('patient_id', $patientIDs)->paginate(15);
                        return view('patients.profile', [
                            'patientServices' => $patientServices,
                            'patient' => $user->patient
                        ]);
                    }
               
            case 'reception':
                return redirect()->route('patient-services.index');
            break;
            case 'cashier':
                return redirect()->route('patient-services.cashier');
            break;
            case 'maindoctor':
                return redirect()->route('debits.index');
            break;
            case 'doctor':
                return redirect()->route('patient-services.payed');
            case 'represent':
                return redirect()->route('patient-senders.index');
            case 'admin':
            default:
                $statistic = Statistic::dashboard();
               
                return view('dashboard', [
                    'statistic' => $statistic 
                ]);
                       
            break;
                 
        }

        return view('dashboard');
    }
}
