<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Debit;
use App\Models\DebitProduct;
use App\Models\Service;
use App\Models\ServiceType;




class DebitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $debits = Debit::with('debitProduct')->orderBy('created_at','desc')->get();

        return view('debits.index', [
            'debits' => $debits
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $debit_products = DebitProduct::all();
        $services = Service::all();
        $serviceTypes = ServiceType::all();

        return view('debits.create', [
            'products' => $debit_products,
            'services' => $services,
            'serviceTypes' => $serviceTypes

        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->validate([
            'debit_product_id' => 'required',
            'amount' => 'required',
            'price' => 'required',
        ]);

        $input['remained_amount'] = $input['amount'];

        Debit::create($input);

        return redirect()->route('debits.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $debits = Debit::where('id',$service_id)->with('diagnostic')->first();

        return view('debits.edit', [
            'debits'=> $debits
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'service_type_id'=> 'required',
            'service_id' => 'required',
            'debit_product_id' => 'required',
            'amount' => 'required',
            'remained_amount' => 'required',
            'price' => 'required',
        ]);

        Debit::update($request->post());

        return redirect()->route('debits.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Debit::where('id', $id)->delete();
        return redirect()->route('debits.index')
                        ->with('success','list deleted successfully');
    }
}
