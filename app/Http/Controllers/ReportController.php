<?php

namespace App\Http\Controllers;

use App\Models\Report;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function all(Request $request)
    {   
      
        $begin_date = $request->query('begin_date', Carbon::now()->startOfYear()->toDateString());
        $end_date = $request->query('end_date', Carbon::now()->toDateString());
        
        $statistics =  Report::fullStatistic($begin_date, $end_date);

        return view('reports.all', [
            'statistics' =>  $statistics
        ]);
    }

    public function bySenders(Request $request)
    {   
      
        $begin_date = $request->query('begin_date', Carbon::now()->startOfYear()->toDateString());
        $end_date = $request->query('end_date', Carbon::now()->toDateString());
        
        $statistics =  Report::statisticBySenders($begin_date, $end_date);
        // dd($statistics);
        return view('reports.bysenders', [
            'statistics' =>  $statistics
        ]);
    }


}
