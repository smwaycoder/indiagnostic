<?php

namespace App\Http\Controllers;

use App\Models\DebitProduct;
use App\Models\ServiceProduct;
use Illuminate\Http\Request;
use App\Models\ServiceType;



class ServiceProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = ServiceProduct::with('service','serviceType','product')->orderByDesc('created_at')->get();

        return view('service-products.index',['products' => $products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $serviceTypes = ServiceType::get();
        $debitProducts = DebitProduct::get();
        
        return view('service-products.create', [
            'serviceTypes' => $serviceTypes,
            'products' => $debitProducts,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->validate([
            'service_type_id' => 'required',
            'service_id' => 'required',
            'serviceProducts.*.debit_product_id' => 'required',
            'serviceProducts.*.required_amount' => 'required',
        ]);


        if(isset($input['serviceProducts']) && count($input['serviceProducts'])){
            foreach($input['serviceProducts'] as $serviceProduct){
                
                $inputDetail = [
                    'service_type_id' => $input['service_type_id'],
                    'service_id' => $input['service_id'],
                    'debit_product_id' => $serviceProduct['debit_product_id'],
                    'required_amount' => $serviceProduct['required_amount'],
                ];

                ServiceProduct::create($inputDetail);
            }
        }
      
        return redirect()->route('service-products.index');
    

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ServiceProduct  $serviceProduct
     * @return \Illuminate\Http\Response
     */
    public function show(ServiceProduct $serviceProduct)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ServiceProduct  $serviceProduct
     * @return \Illuminate\Http\Response
     */
    public function edit(ServiceProduct $serviceProduct)
    {
        return view('service-products.edit', ['serviceProduct' => $serviceProduct]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ServiceProduct  $serviceProduct
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ServiceProduct $serviceProduct)
    {
        $input = $request->validate([
            'service_type_id' => 'nullable',
            'service_id' => 'nullable',
            'debit_product_id' => 'nullable',
            'required_amount' => 'nullable',
        ]);

        if($serviceProduct->update($input)){
            return view('service-products.index');
        }

        abort(422,'Yangilashda xatolik');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ServiceProduct  $serviceProduct
     * @return \Illuminate\Http\Response
     */
    public function destroy(ServiceProduct $serviceProduct)
    {
        //
    }
}
