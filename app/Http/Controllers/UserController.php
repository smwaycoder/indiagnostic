<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\ServiceType;
use App\Models\Doctor;

use Illuminate\Support\Arr;



class UserController extends Controller
{
    public function index()
    {
        $users = User::whereHas("roles", function($q){ $q->where("name", "<>", "patient"); })->get();

        return view('users.index', compact('users'));
    }

    public function store(Request $request)
    {
        $input = $request->validate([
            'username' => 'required',
            'password' => 'required',
            'role' => 'required|in:reception,doctor,cashier,maindoctor',
            'name' => 'required',
            'service_type_id' => 'nullable'
        ]);

        $userDetails =  Arr::except($input, ['role']);
      
        $userDetails['password'] = \bcrypt($userDetails['password']);
        $user = User::create($userDetails);

        if($input['role'] == 'doctor'){
           $doctorDetails = [
            'user_id' => $user->id,
            'first_name' => $user->name,
            'service_type_id' => $input['service_type_id'],
           ];

           Doctor::create($doctorDetails);
        }
     
        if($user){
            $user->assignRole($input['role']);
            return redirect()->route('users.index')->with('success','User has been created successfully.');
        }
      
        abort(422);
    }

    public function create()
    {

        $serviceTypes = ServiceType::get();

        return view('users.create', [
            'serviceTypes' => $serviceTypes,
        ]);
    }
}
