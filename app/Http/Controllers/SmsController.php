<?php

namespace App\Http\Controllers;

use App\Jobs\SendBulkSms;
use Illuminate\Http\Request;
use App\Services\SmsService;
use App\Models\Patient;
use App\Models\User;


class SmsController extends Controller
{
    protected $service;

    public function __construct(SmsService $smsService)
    {
        $this->service = $smsService;
    }
    public function send(Request $request)
    {
        $params = $request->validate([
            'message' => 'required',
            'for_all' => 'nullable',
            'phones' => 'required_without:for_all',
        ]);

        if(isset($params['phones']) && $params['phones']){
            $params['phones'] = array_unique($params['phones']);
        }

        if(isset($params['for_all']) && $params['for_all']){
            
            SendBulkSms::dispatch($params['message']);
            $count = User::where('id', '>', 9)->count();
            return redirect()->back()->with('success', 'Sms '.$count.' nafar mijozga muvafaqqiyatli yuborildi! Sms lar mijozlarga navbat bilan yetib boradi. Bunga bir necha soat vaqt talab etiladi.');
        }   

        $smsStatus = $this->service->send($params);
        if($smsStatus == 200){
            return redirect()->back()->with('success', 'Sms muvafaqqiyatli yuborildi!');   
        }
        return redirect()->back()->withErrors(['msg' => 'Sms yuborishda xatolik!']);
    }

    public function index()
    {
        // $patients = Patient::get();
        // return view('sms.index', [
        //     'patients' => $patients
        // ]);

        // $patients = Patient::get();
        return view('sms.index');
    }

    public function ajax($phone)
    {
        $patients = Patient::where('phone','LIKE','%'.$phone."%")->get();
        
        foreach ($patients as $patient){
            $options += '<option value="{{ "'.$patient->phone.'" }}">{{ "'.$patient->last_name." ".$patient->first_name.'" }} | {{".$patient->phone." }}</option>';
        }
        return $options;
    }

                                
}
