<?php

namespace App\Http\Middleware;

use Closure;
use App\Utils\LogUtil;

class Logs
{

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //включили отсчёт времени выполнения запроса
        LogUtil::$requestTime -= microtime(true);

        return $next($request);
    }

    public function terminate($request, $response)
    {
        $status = $response->getStatusCode();
       
        if (!($status < 300) || ($status < 300 && LogUtil::getConfig('success_log'))) {
           
            //выключили отсчёт времени выполнения запроса
            LogUtil::$requestTime += microtime(true);

            $responseData = json_decode($response->getContent(), true);
            $user = LogUtil::resolveUser();

            $resContext = isset($responseData['data']) ? (is_array($responseData['data']) ? $responseData['data'] : ['data' => $responseData['data']])
                : (is_array($responseData) ? $responseData : ['data' => $responseData]);

            LogUtil::setContext('request', $request->toArray());
            LogUtil::setContext('response', LogUtil::getConfig('info_level_log') === 'DEBUG' ?
                $resContext : ['data' => '#DATA_HIDDEN#']);

            LogUtil::Log([
                $status < 300 ? 'SUCCESS' : ($status != 500 ? 'FAIL' : 'EXCEPTION'),
                $status,
                $request->ip(),
                $request->getMethod(),
                $request->getPathInfo(),
                $responseData['message'] ?? ($status == 401 ? 'User unauthorized' : (
                $status == 200 ? ($request->grant_type === 'password' ?
                    'Successfully authorization' : 'Successfully update authorization') : 'Unsuccessfully authorization')),
                $responseData['code'] ?? '0',
                round(LogUtil::$requestTime, 4),
                $user->id ?? 'null',
                $user->username ?? 'null',
                $request->userAgent(),
                'General'
            ]);
        }
    }
}
