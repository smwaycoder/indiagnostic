<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProfileLoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'phone' => 'required|exists:patient_cards,phone',
            'number' => 'required|exists:patient_cards,number'
        ];
    }

    public function messages()
    {
        return [
            'phone.required' => 'Iltimos, telefon raqamingizni kiriting!',
            'number.required' => 'Iltimos,karta raqamini kiriting!',
            'number.exists' => 'Bunday karta raqami mavjud emas!',
            'phone.exists' => 'Bunday  telefon raqam mavjud emas!'
        ];
    }
}
