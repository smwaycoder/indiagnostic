<?php

namespace App\Listeners;

use App\Events\PatientServicePayed;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use  Telegram\Bot\Exceptions\TelegramResponseException;
use Telegram;

class SendDoctorNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\PatientServicePayed  $event
     * @return void
     */
    public function handle(PatientServicePayed $event)
    {
        $patientService = $event->patientService;
        $patient = $event->patientService->patient;

        if($patientService->patientSender &&  $patientService->patientSender->telegram_user_id){
            $txt = 'Sizdan ProMed uchun '.$patient->fullName.' ismli bemor keldi!'.PHP_EOL.PHP_EOL.

            'Xizmat turi: <b>'.$patientService->service->type->name.PHP_EOL.'</b>'.
            'Xizmat nomi: <b>'.$patientService->service->name.PHP_EOL.'</b>'.
            'Xizmat ko\'rsatilgan vaqt: <b>'.$patientService->created_at.PHP_EOL.'</b>';

            $percentage = $patientService->service->type->doctor_percentage;
            if($percentage){
                $doctorBonus = round((int)str_replace(' ', '',$patientService->service->price) * (int)$percentage/100);
           
                $txt = $txt.'Sizga to\'lanadigan summa: <b>'.$doctorBonus.'</b>';
            }

            $overallTxt = $txt.PHP_EOL.PHP_EOL.PHP_EOL.'Biz bilan hamkorligingiz uchun raxmat!';

                   
            try {
                Telegram::sendMessage(
                    [
                        'chat_id' => $patientService->patientSender->telegram_user_id,
                        'parse_mode'=> 'html',
                        'text' => $overallTxt
                    ]);
                       
            } catch (TelegramResponseException $e) {
            
                \Log::error('Not sended for User - '. $e->getMessage());
            }

        }
    }
}
