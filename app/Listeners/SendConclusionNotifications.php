<?php

namespace App\Listeners;

use App\Events\GivedConclusion;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Telegram;
use  Telegram\Bot\Exceptions\TelegramResponseException;
use Telegram\Bot\FileUpload\InputFile;

class SendConclusionNotifications
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  \App\Events\GivedConclusion  $event
     * @return void
     */
    public function handle(GivedConclusion $event)
    {
        $patient = $event->patientService->patient;

        if($patient->user && $patient->user->telegram_user_id){
            $resultText = "Hurmatli ".$patient->user->name."! Sizning ".$event->patientService->created_at." da ProMed klinikasida ".$event->patientService->service->name." xizmati bo'yicha natijalaringiz tayyor";
            
            Telegram::sendMessage(['chat_id' => $patient->user->telegram_user_id, 'parse_mode'=>'html','text' => $resultText]);
                
            if($event->patientService->conclusion_file){
                try {
                    Telegram::sendDocument([
                        'chat_id' => $patient->user->telegram_user_id, 
                        'document' => new InputFile(public_path().'/storage/'.$event->patientService->conclusion_file),
                        'caption' => 'Shifokor xulosasi',
                    ]);
                 } catch (TelegramResponseException $e) {
                 
                    \Log::error('Not sended for User - '. $e->getMessage());
                }
            }
          
            if($event->patientService->recommendation_file){
       
                try {
                    Telegram::sendDocument([
                        'chat_id' => $patient->user->telegram_user_id, 
                        'document' => new InputFile(public_path().'/storage/'.$event->patientService->recommendation_file),
                        'caption' => 'Shifokor tavsiyasi',
                    ]);
                } catch (TelegramResponseException $e) {
                
                    \Log::error('Not sended for User - '. $e->getMessage());
                }
            }
         
          
        }
        
        //TODO: file generate qilib telegram orqali yuborish
    }
}
