<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class Statistic extends Model
{
    use HasFactory;

    public static function dashboard($range = 'yearly')
    {
      $statistics = DB::table('patient_services')->select(self::rawQuery())->whereNull('deleted_at')->whereBetween('created_at',  self::getRange($range))->first();
        
      return $statistics;
    }

    public static function rawQuery()
    {
        
        $sqlRow = 'count(*) as all_services,count(distinct patient_id) as all_patients,sum(case when service_status_id="2" or service_status_id="3"  then price else 0 end) as all_benefit';    

        if(config('database.default') == 'pgsql'){
            $sqlRow = 'count(*) as all_services,count(distinct patient_id) as all_patients,sum(case when service_status_id=2 or service_status_id=3  then price else 0 end) as all_benefit';
        }

        $raw = DB::raw($sqlRow);

        return $raw;
    }

    public static function getRange($range)
    {
        $start = Carbon::now()->startOfYear();
        $end = Carbon::now()->endOfYear();
        
        switch ($range) {
            case "today": 
                $start = Carbon::now()->startOfDay();
                $end = Carbon::now()->endOfDay();
                break;
            case "yesterday": 
                $start = Carbon::yesterday();
                $end = Carbon::now()->startofDay();
                break;
            case "weekly": 
                $start = Carbon::now()->startOfWeek();
                $end = Carbon::now()->endOfWeek();
                break;
            case "monthly": 
                    $start = Carbon::now()->startOfMonth();
                    $end = Carbon::now()->endOfMonth();
                    break;
            case "yearly": 
                $start = Carbon::now()->startOfYear();
                $end = Carbon::now()->endOfYear();
                break;                  
        }

        
        return [$start, $end];
    }
}
