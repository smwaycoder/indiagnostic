<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BotText extends Model
{
    protected $fillable = [
        'keyword',
        'uz',
        'ru'
    ];
    use HasFactory;

}
