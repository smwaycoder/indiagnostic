<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Patient extends Model
{
    use HasFactory;
    public $guarded = [];

    protected $appends = ['icone', 'genderLabel'];

   public function getGenderLabelAttribute($value)
   {
        return $this->gender ? 'Erkak' : 'Ayol';
   }

   public function getFullNameAttribute($value)
   {
        return $this->first_name.' '.$this->last_name;
   }

   public function getIconeAttribute($value)
   {
        return $this->gender == 1 ? 'male.jpg' : 'women.jpg';
   }

    public function services()
    {
        return $this->hasMany(PatientService::class)->registered();
    }
    
    public function user()
    {
        return  $this->belongsTo(User::class, 'user_id','id');
    }

    public function conclusedServices()
    {
        return $this->hasMany(PatientService::class)->conclused();
    }


}
