<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    use HasFactory;

    protected $attributes = [
       'active' => true 
    ];

    protected  $guarded = [];

    public function type()
    {
        return $this->belongsTo('App\Models\ServiceType','service_type_id','id');
    }

    public function diagnostic()
    {
        return $this->belongsTo('App\Models\DiagnosticType','diagnostic_type_id','id');
    }

    public function scopeActive($query)
    {
        return $query->where('active', true);
    }

    public function getPriceAttribute($value)
    {
        return number_format($value, 0, ',', ' ');
    }

    public function setPriceAttribute($value)
    {
        $this->attributes['price'] = str_replace(' ', '', $value);
    }
}
