<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PatientSender extends Model
{
    use HasFactory;

    
    protected $attributes = [
        'is_active' => true 
     ];
 
     protected  $fillable = [
        'fullname',
        'add_info',
        'is_active',
        'uuid'
     ];

     
    public function scopeActive($query)
    {
        return $query->where('is_active', true);
    }


     
}
