<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServiceProduct extends Model
{
    protected $fillable = [
        'service_type_id',
        'service_id',
        'debit_product_id',
        'required_amount'
    ];


    use HasFactory;


    public function service()
    {
        return $this->belongsTo(Service::class);
    }

    public function serviceType()
    {
        return $this->belongsTo(ServiceType::class);
    }

    public function product()
    {
        return $this->belongsTo(DebitProduct::class,'debit_product_id','id');
    }

}
