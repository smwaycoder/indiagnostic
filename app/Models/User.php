<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use App\Models\Patient;



class User extends Authenticatable
{
    use HasFactory, Notifiable,HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'username',
        'sended_pass',
        'telegram_user_id',
        'remember_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function doctor()
    {
        return $this->hasOne('App\Models\Doctor');
    }

    public function isAdmin()
    {
        return $this->hasRole('admin');
    }

    public function isReception()
    {
        return $this->hasRole('reception');
    }
    
    public function isRepresent()
    {
        return $this->hasRole('represent');
    }

    public function isMainDoctor()
    {
        return $this->hasRole('maindoctor');
    }

    public function isPatient()
    {
        return $this->hasRole('patient');
    }

    public function patient()
    {
        return $this->hasOne(Patient::class);
    }

}
