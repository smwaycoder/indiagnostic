<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Report extends Model
{
    use HasFactory;

    public static function fullStatistic($begin_date, $end_date)
    {   
        
        $statistics = DB::select('select  
        st.name,
        count(*) as all_services,
        sum(case when service_status_id="1" then 1 else 0 end) as registered_service,
        sum(case when service_status_id="2" then 1 else 0 end) as payed_service,
        sum(case when service_status_id="3" then 1 else 0 end) as conclused_service,
        count(distinct patient_id) as all_patients,
        count(distinct (case when service_status_id="1" then patient_id end)) as registered_patient,
        count(distinct (case when service_status_id="2" then patient_id end)) as payed_patient,
        count(distinct (case when service_status_id="3" then patient_id end)) as conclused_patient,
        sum(case when service_status_id="1" then ps.price else 0 end) as waiting_benefit,
        sum(case when service_status_id="2" or service_status_id="3"  then ps.price else 0 end) as payed_benefit
        from patient_services ps
        left join services s on s.id = ps.service_id
        left join service_types st on st.id = s.service_type_id
        where ps.created_at  between "'.$begin_date.'" and "'.$end_date.'" and ps.deleted_at is null
        GROUP by st.id, st.name');

        return $statistics;
    }

    public static function statisticBySenders($begin_date, $end_date)
    {
        $statistics = DB::select('select  psender.fullname as fullname, psender.id as patient_sender_id,
        count(*) as all_services,
        sum(case when s.service_type_id="1" then 1 else 0 end) as labaro,
        sum(case when s.service_type_id="2" then 1 else 0 end) as mskt,
        sum(case when s.service_type_id="3" then 1 else 0 end) as uzi,
        sum(case when s.service_type_id="4" then 1 else 0 end) as consult,
        sum(case when s.service_type_id="5" then 1 else 0 end) as proced,
        sum(case when s.service_type_id="7" then 1 else 0 end) as lor,
        sum(case when s.service_type_id="8" then 1 else 0 end) as detskiy_massaj,
        sum(case when s.service_type_id="9" then 1 else 0 end) as ekg,
        sum(case when s.service_type_id="10" then 1 else 0 end) as statsionar,
        sum(case when service_status_id="2" or service_status_id="3"  then s.price else 0 end) as payed_benefit
        from patient_services ps
   			
        left join services s on s.id = ps.service_id
        left join service_types st on st.id = s.service_type_id
        inner join patient_senders psender on psender.id = ps.patient_sender_id
        WHERE ps.service_status_id >=2 AND ps.created_at  between "'.$begin_date.'" and "'.$end_date.'" and ps.deleted_at is null
        GROUP by psender.fullname, psender.id
        ORDER BY count(*) desc');

        return $statistics;
    }
}
