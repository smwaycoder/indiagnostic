<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PatientCard extends Model
{
    protected $guarded = [];

    protected $attributes = [
        'is_active' => true,
    ];
    
    use HasFactory;


    public function patient()
    {
        return $this->belongsTo('App\Models\Patient');
    }

    
}
