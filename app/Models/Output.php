<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Output extends Model
{
    protected $fillable = [
        'service_type_id',
        'debit_product_id',
        'patient_service_id',
        'service_id',
        'amount'
    ];


    use HasFactory;

    public function patientService()
    {
        return $this->belongsTo(PatientService::class, 'patient_service_id', 'id');
    }

    public function debitProduct()
    {
        return $this->belongsTo(DebitProduct::class, 'debit_product_id', 'id');
    }
}
