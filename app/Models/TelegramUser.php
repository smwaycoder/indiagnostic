<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class TelegramUser extends Model
{
    use HasFactory;
    
    protected $fillable = [
        'id',
        'is_premium',
        'language_code',
        'is_bot',
        'first_name',
        'last_name',
        'username',
        'language_code',
        'step',
        'phone'
    ];  

    public function user()
    {
        return $this->hasOne(User::class,'telegram_user_id','id');
    }
}

