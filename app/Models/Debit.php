<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class Debit extends Model
{
    use HasFactory;

    protected $fillable = [
        'service_type_id',
        'service_id',
        'debit_product_id',
        'amount',
        'remained_amount',
        'price',
    ];


    public function debitProduct()
    {
        return $this->belongsTo(DebitProduct::class);
    }
}
