<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Builder;
use Carbon\Carbon;

class PatientService extends Model
{
    use HasFactory, SoftDeletes;
    protected $guarded = [];

    public const STATUS_REGISTERED = 1;
    public const STATUS_PAYED = 2;
    public const STATUS_CONCLUSED = 3;

    public static function boot()
  {
    parent::boot();

    self::creating(function($model){
      $last_sequence = self::getSequence($model->service_id, $model->patient_id);
      $model->sequence = $last_sequence+1;
    });
  }


    public function scopeRegistered($query)
    {
        return $query->where('service_status_id',self::STATUS_REGISTERED);
    }

    public function scopePayed($query)
    {
        return $query->where('service_status_id', self::STATUS_PAYED);
    }

    public function scopePayedOrConclused($query)
    {
        $statuses = [self::STATUS_PAYED, self::STATUS_CONCLUSED];
        return $query->whereIn('service_status_id', $statuses);
    }

    public function scopeConclused($query)
    {
        return $query->where('service_status_id', self::STATUS_CONCLUSED);
    }

    public function scopeMatchedServices($query, $service_type)
    {
        
        return $query->whereHas('service', function($q) use ($service_type)
        {
            $q->where('service_type_id', '=',$service_type);
        
        });
    }

    public function patient()
    {
        return $this->belongsTo('App\Models\Patient');
    }

    public function patientSender()
    {
        return $this->belongsTo('App\Models\PatientSender');
    }

    public function service()
    {
        return $this->belongsTo('App\Models\Service');
    }

    public function status()
    {
        return $this->belongsTo('App\Models\ServiceStatus', 'service_status_id', 'id');
    }

    public function setPriceAttribute($value)
    {
        $this->attributes['price'] = str_replace(' ', '', $value);
    }

    public static function getSequence($service_id, $patient_id)
    {
        $service = Service::find($service_id);

        return self::whereHas('service', function (Builder $query) use($service){
                $query->where('service_type_id', '=', $service->service_type_id );
            })->whereDate('created_at', Carbon::today())->where('patient_id', '!=', $patient_id)->count();
    }


    public function getStatusClassAttribute()
    {
        switch ($this->service_status_id) {
            case 1:
                return "secondary";
                break;
            case 2:
                return "primary";
                break;
            case 3:
                return "success";
                break;
            default:
                return "primary";
                break;
        }
        
    }

}
