<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Services\SmsService;
use App\Models\User;

class SendBulkSms implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $message;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($message){
        $this->message = $message;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(SmsService $smsService)
    {
        User::where('id','>', 9)->chunk(1000, function ($users) use($smsService){
            foreach ($users as $user)
            {
                if($user->patient)
                {
                   $phone = $user->username;
                   if(strlen($phone) == 9 && (substr($phone,0,2) != 71)){
                        $params['phones'] = [$phone];
                        
                        $params['message'] = $this->message;
                        $smsService->send($params);
                    }
                }
    
            }
        });
      

      
    }
}
