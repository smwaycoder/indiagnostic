<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Models\PatientService;
use App\Models\ServiceProduct;
use App\Models\Debit;
use App\Models\Output;



class NewOutputRegisteredJob implements ShouldQueue
{
    protected $patientService;

    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(PatientService $patientService){
        $this->patientService = $patientService;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //

        $serviceProducts = ServiceProduct::where('service_id',$this->patientService->service_id)->get();

        foreach($serviceProducts as $product){
            // TODO: ostatkadan ortib ketish logikasini qayta korib chiqish kerak, bir mahsulotga bir nechta kirim bolish holati
            // umumiy kirimdan chiqim bo'lgan mahsulotni ayirib qoyamiz
            $requiredAmount = $product->required_amount;
            $debit = Debit::where('debit_product_id', $product->debit_product_id)->first();
            
            if($debit && $debit->remained_amount > 0)
            {
                $debit->remained_amount = $debit->remained_amount - $requiredAmount;
                $debit->update();
                
                $outputDetails = [
                    'service_type_id' => $product->service_type_id,
                    'patient_service_id' => $this->patientService->id,
                    'service_id' => $product->service_id,
                    'debit_product_id' => $product->debit_product_id,
                    'amount' => $requiredAmount,
                ];

                Output::create($outputDetails);
            }
        }

    }
}
