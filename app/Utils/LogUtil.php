<?php
/**
 * Created by PhpStorm.
 * User: Pavel
 * Date: 17.06.2019
 * Time: 10:39
 */

namespace App\Utils;

use App\Models\User;
use Illuminate\Support\Facades\Log;

/**
 * Class LogUtil
 * @package App\Utils
 */
class LogUtil
{

    /**
     * @var double $requestTime Время выполнения запроса
     */
    public static $requestTime = 0;

    /**
     * @var \Illuminate\Http\Request|null $request Запрос
     */
    public static $request = null;

    /**
     * @var null|\App\Models\v2\User $user Авторизированный пользователь
     */
    private static $user = null;

    /**
     * @var string $format Формат логов
     *
     * 1.   STATUS - SUCCESS|FAIL
     * 2.   STATUS_CODE - HTTP code
     * 3.   IP - IP address
     * 4.   METHOD - OPTIONS|GET|POST|PUT|DELETE
     * 5.   PATH_INFO - URL|URI
     * 6.   MESSAGE - $response['message'] текст
     * 7.   CODE - $response['code'] код ответа
     * 8.   REQUEST_TIME - время выполнения запроса
     * 9.   ID_USER - ID авторизованного пользователя
     * 10.  COMPANY_TIN - ИНН ЮЛ
     * 11.  PERSON_TIN - ИНН ФЛ
     * 12.  PERSON_NAME - имя ФЛ
     * 13.  PERSON_SURNAME - фамилия ФЛ
     * 14.  PERSON_PATRONYMIC - отчество ФЛ
     * 15.  USER_AGENT - описание браузера клиента
     * 16.  TYPE_LOG - null|GovUz|Soliq|Military
     *
     * CONTEXT - {'request' => [данные в запросе], 'response' => [данные в ответе]}
     *
     * STATUS STATUS_CODE IP METHOD PATH_INFO "MESSAGE" CODE REQUEST_TIME ID_USER COMPANY_TIN PERSON_TIN "PERSON_NAME" "PERSON_SURNAME" "PERSON_PATRONYMIC" "USER_AGENT" "TYPE_LOG" CONTEXT:{'request' => [], 'response' => []}
     */
    private static $format = [
        'STATUS' => '%s',
        'STATUS_CODE' => '%s',
        'IP' => '%s',
        'METHOD' => '%s',
        'PATH_INFO' => '%s',
        'MESSAGE' => '"%s"',
        'CODE' => '%s',
        'REQUEST_TIME' => '%s',
        'ID_USER' => '%s',
        'USER_NAME' => '%s',
        'USER_AGENT' => '"%s"',
        'TYPE_LOG' => '"%s"'
    ];

    /**
     * @var array $context содержит контекст логов
     */
    private static $context = [
        'request' => [],
        'response' => []
    ];

    /**
     * Обнуление переменных, если во время работы приложения планируется ещё раз писать логи
     *
     * @return bool
     */
    protected static function init(): bool
    {
        self::$context = [
            'request' => [],
            'response' => []
        ];

        return true;
    }

    /**
     * Подготовка строки для лога
     *
     * @param array $data Данные
     * @return string
     */
    protected static function prepareFormat(array $data): string
    {
        return vsprintf(implode(' ', self::$format), $data);
    }

    /**
     * Получение конфигов
     *
     * @param string $param
     * @return mixed
     */
    public static function getConfig(string $param)
    {
        return config('elastic')[$param];
    }

    /**
     * Установка контекста
     *
     * @param String $field
     * @param $data
     * @return bool
     */
    public static function setContext(String $field, $data): bool
    {
        if (array_key_exists($field, self::$context)) {
            self::$context[$field] = $data;
            return true;
        }

        return false;
    }

    /**
     * Если пользователь не был назначен, берем его с приложения
     *
     * @return User|null
     */
    public static function resolveUser()
    {
        if (!self::$user) {
            self::$user = App()->make('request')->user();
        }

        return self::$user;
    }

    /**
     * Устанавливаем пользователя, полезно, если вызван метод авторизации и пользователь ещё не авторизован
     *
     * @param User $user
     * @return User|null
     */
    public static function setUser(User $user)
    {
        self::$user = $user;

        return $user;
    }

    /**
     * Запись лога в файл
     *
     * @param array $data
     * @return bool
     */
    public static function Log(array $data): bool
    {
        if (self::getConfig('app_info_log_enable')) {
            Log::info(self::prepareFormat($data), self::$context);
            self::init();

            return true;
        }

        return false;
    }

}