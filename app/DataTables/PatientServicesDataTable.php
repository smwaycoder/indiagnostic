<?php

namespace App\DataTables;

use App\Models\PatientService;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Html\Editor\Editor;
use Yajra\DataTables\Html\Editor\Fields;
use Yajra\DataTables\Services\DataTable;
use Auth;

class PatientServicesDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {   
        return datatables()
            ->eloquent($query)
            ->addIndexColumn()
            ->addColumn('fullname', function($row){return 
                 '<a href="/patients-services/profile/'.$row->id.'">'.$row->patient->first_name.' '.$row->patient->last_name.'</a>'; })
            ->addColumn('doctor', function($row){
                return ($row->patientSender)  ? $row->patientSender->fullname : ''; })
            ->addColumn('telegram', function($row){
                    return ($row->patient->user && $row->patient->user->telegram_user_id)  ? 'Ulangan' : 'Ulanmagan'; })
            ->addColumn('added', function($row){return  $row->created_at; })
            ->addColumn('price', function($row){return  number_format($row->price, 2, ',', '  '); })
            ->editColumn('status.name', function($row){ return '<span class="badge badge-'.$row->statusClass.'">'.$row->status->name.'</span>'; })
            ->editColumn('service.type.name', function($row){ return '<span class="badge badge-'.$row->statusClass.'">'.$row->service->type->name.'</span>'; })
            ->addColumn('action', function($row){ return  
                (Auth::user()->isReception()) ? '<a href="/repetition/create/'.$row->patient->id.'" class="btn btn-primary btn-sm scroll-click"> Qo\'shish </a> <br><br>': 
                ((\Auth::user()->isReception() && $row->status->name == "Ro'yxatga olindi") || (\Auth::user()->isAdmin()) ? 
                '<form action="/patient-services/'.$row->id.'" id="deleteForm" method="post">
                <input type="hidden" name="_method" value="delete">
                <button type="submmcit" class="btn btn-danger btn-sm" onclick="return confirm(\'Ishonchingiz komilmi?\')" > O\'chirish </button> 
            </form>':"");})
            ->rawColumns(['fullname', 'action','status.name','service.type.name']);
            // ->filter(function ($query) {
            //     if (request()->has('name')) {
            //         $query->where('name', 'like', "%" . request('name') . "%");
            //     }

            //     if (request()->has('email')) {
            //         $query->where('email', 'like', "%" . request('email') . "%");
            //     }
            // }, true)
    }

   
    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\PatientService $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(PatientService $model)
    {
        $query = $model::with(['patient:id,first_name,last_name,phone,user_id', 'status','service:id,name,price,service_type_id','service.type:id,name', 'patientSender:id,fullname'])->orderBy('id','desc');
        return $query;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
                    ->setTableId('patientServices')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->buttons(
                        Button::make('excel'),
                        Button::make('print'),
                    )->parameters([
                        'order' => [
                            1, // here is the column number
                            'desc'
                        ],
                        'pageLength' => 40,
                    ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [

            Column::make('DT_RowIndex')->title('#')->searchable(false),
            Column::make('id')->title('#')->searchable(false)->visible(false)->exportable(false),
            Column::make('fullname')->title('Mijoz ismi'),
            Column::make('patient.first_name')->title('Mijoz ismi')->visible(false)->exportable(false),
            Column::make('patient.last_name')->title('Mijoz ismi')->visible(false)->exportable(false),
            Column::make('patient.phone')->title('Telefon raqam'),
            Column::make('service.type.name')->title('Xizmat turi'),           
            Column::make('service.name')->title('Xizmat nomi'),    
            Column::make('doctor')->title('Shifokor'),    
            Column::make('added')->title('Qo\'shilgan vaqt'),
            Column::make('created_at')->title('Qoshilgan vaqt')->visible(false)->exportable(false),
            Column::make('telegram')->title('Telegram'),           
            Column::make('price')->title('Xizmat narxi'),           
            Column::make('status.name')->title('Xizmat holati'),
            Column::make('source')->title('Mijoz manbasi'),
            Column::make('action')->title('Amal')->exportable(false),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'Xizmatlar_' . date('Ymd');
    }
}
