<?php 

namespace App\Services;

use App\Models\User;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Services\SmsService;

class PatientService{

    private $smsService;

    public function __construct(SmsService $smsService){
        $this->smsService = $smsService;
    }

    public function createUser($patient)
    {    
        DB::transaction(function () use ($patient){
            $password = rand(111111,999999);
            $phone = substr($patient->phone, -9);

            $userRole = Role::where('name','patient')->first();
            if(!str_contains($patient->phone, 'x')){
                $user = User::create([
                    'name'=> $patient->fullName,
                    'username'=> $phone,
                    'password'=> \bcrypt($password),
                    'email'=> 'patient'.$patient->id.'@gmail.com',
                    'remember_token' =>  Str::random(12)
                ]);    
     
                $user->assignRole($userRole);
                $patient->user_id =  $user->id;
                
                if($patient->update()){
    
                    $params['phones'] = [$phone];
                    $params['message'] = 'ProMed klinikasidan tekshiruv natijalarini bilish uchun telegram bot: https://t.me/ProMedClinicBot?start='.$user->remember_token.'. Bizni kuzatib boring: bit.ly/3lubdw7';
                   
                    $resultCode = $this->smsService->send($params);
                    
                    $user->sended_pass = ($resultCode == 200);
                    $user->update();

                    //TODO: send sms
                }
            }
        });
    
    }

    public function sendTelegramBotLink($patient)
    {
        $user = $patient->user;

        if($user && !$user->telegram_user_id){
                
            $params['phones'] = [$patient->phone];
    
            if(!$user->remember_token){
                $user->remember_token = Str::random(12);
                $user->save();
            }
            
            $params['message'] = 'ProMed klinikasidan tekshiruv natijalarini bilish uchun telegram bot / Telegram-bot dlja polučenija rezulʹtatov analizov iz kliniki ProMed: https://t.me/ProMedClinicBot?start='.$user->remember_token.'. Bizni kuzatib boring: bit.ly/3lubdw7';
            
            $this->smsService->send($params);
        
        }
    }

}