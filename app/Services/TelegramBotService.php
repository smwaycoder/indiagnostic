<?php 

namespace App\Services;

use App\Http\Controllers\PrintController;
use Telegram;
use App\Models\TelegramUser;
use App\Models\User;
use App\Models\PatientSender;
use App\Models\ServiceType;
use App\Models\Patient;
use App\Models\PatientService;

use Telegram\Bot\FileUpload\InputFile;

use File;
use Telegram\Bot\Keyboard\Keyboard;
use  Telegram\Bot\Exceptions\TelegramResponseException;

//services

use App\Services\TelegramUserService;
use App\Services\BotTextService;


use Illuminate\Support\Facades\DB;


class TelegramBotService
{
    public $telegramUserService;
    public $textService;

    public function __construct(TelegramUserService $telegramUserService,BotTextService $textService){
        $this->telegramUserService = $telegramUserService;
        $this->textService = $textService;
    }

    public function handle()
    {
        $update = Telegram::getWebhookUpdates();
       
        $updates = json_decode($update,true);
        // private chatdan yozilmasa ignor qilamiz
        if (isset($updates['message']['chat']['type'])){
            $type = $updates['message']['chat']['type'];

            if ($type != 'private'){
                return false;
            }
        }

        // chat ID va text olish 

        $isCallback = false;

        if(isset($updates['message']))
        {
            $telegram = $updates['message'];

        } elseif(isset($updates['callback_query'])) {

            $isCallback = true;
            $telegram = $updates['callback_query'];
       
        } else {
            return false;
        }

        $user_id = $telegram['from']['id'];

        $text = ($isCallback) ? $telegram['data'] : ((isset($telegram['text'])) ? $telegram['text']  : '');

        $telegramUser = $this->telegramUserService->getUser($user_id, $telegram['from']);


        $hiddenReplyMarkup = json_encode([
            'remove_keyboard' => true,
        ]);


        // patient senderlar uchun register qismi
        if(str_contains($text, '/start')){
            if(strlen($text) > 7){
             
                $token = substr($text, 7);
                $sender = PatientSender::where('uuid', $token)->first();
                
                if($sender){
                    $this->senderRegister($sender,$user_id);
                } else {
                    
                    $user = User::where('remember_token',$token)->first();
                    
                    if ($user){
                        $user->telegram_user_id = $user_id;
                        $user->remember_token = null;
                        $user->save();

                        $this->telegramUserService->setLanguage($user_id, 'ru');
                        $this->showMain($user_id); 
                    }

                }


                return false; 
            }


        }
        
       

        if($text == '/start'){
            $this->telegramUserService->setLanguage($user_id, 'ru');
            $this->showMain($user_id); 
            return false;       
        }
        else{
            switch($this->telegramUserService->getStep($user_id)){
                case 'main':
                    if($text == "🇺🇿🔄🇷🇺 " . $this->textService->getText('change_lang', $user_id)){
                        if($text == "🇺🇿🔄🇷🇺 Tilni o'zgartirish"){
                            $this->telegramUserService->setLanguage($user_id, 'ru');
                        }elseif($text == "🇺🇿🔄🇷🇺 Сменить язык")
                        {
                            $this->telegramUserService->setLanguage($user_id, 'uz');
                        }
                        $this->showMain($user_id);

                        return false;
                    }
                    
                    switch($text){
                        case $this->textService->getText('get_results', $user_id):
                           
                            $this->chooseResultDates($user_id);
                            return false;
                            break;
                        case $this->textService->getText('clinic_location', $user_id):
                            $this->sendLocation($user_id);
                            return false;
                            break;
                        case $this->textService->getText('prices', $user_id):
                            $this->showPrices($user_id);
                            return false;
                            break;
                        case $this->textService->getText('contact_reception', $user_id):
                            $reply_markup = $this->getMainReplyKeyboard($user_id);
                            Telegram::sendMessage(['chat_id' => $user_id, 'parse_mode'=>'html','reply_markup' => $reply_markup, 'text'=> $this->textService->getText('contact_reception_label',$user_id)]);
                            return false;
                            break;
                        case "📞 ".$this->textService->getText('contact_director', $user_id):
                            
                            $reply_markup = $this->getMainReplyKeyboard($user_id);
                            Telegram::sendMessage(['chat_id' => $user_id, 'parse_mode'=>'html','reply_markup'=> $reply_markup, 'text'=> $this->textService->getText('contact_director_label',$user_id)]);
                            
                            return false;
                            break;

                        case "💬 ".$this->textService->getText('feedback', $user_id):

                            $keyboard = [
                                [$this->textService->getText('main_menu', $user_id)],
                            ];
                            
                            $reply_markup = Keyboard::make(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => true]);
                    
                            Telegram::sendMessage(['chat_id' => $user_id, 'parse_mode'=>'html', 'reply_markup'=> $reply_markup, 'text'=> $this->textService->getText('feedback_label',$user_id)]);
                            $this->telegramUserService->setStep($user_id, 'feedback');
                            return false;
                            break;
                        
                        case $this->textService->getText('feedback', $user_id):
                            Telegram::sendMessage(['chat_id' => $user_id, 'parse_mode'=>'html','text'=> $this->textService->getText('feedback_label',$user_id)]);
                            return false;
                            break;
                        default:
                            Telegram::sendMessage(['chat_id' => $user_id, 'parse_mode'=>'html','text'=> $this->textService->getText('choose_menu',$user_id)]);
                        
                            return false;
                            break;
                    }

                    return false;
                    break;
                    
                case 'feedback':
                    if($text == $this->textService->getText('main_menu',$user_id)){
                        $this->showMain($user_id);
                        return false;
                    }

                    $this->sendFeedback($text,$user_id);
                    Telegram::sendMessage(['chat_id' => $user_id, 'text'=> $this->textService->getText('thanks_for_feedback', $user_id)]);
                    $this->showMain($user_id);
            
                    break;
                case 'choose_service_type':
                    if($text == $this->textService->getText('main_menu',$user_id)){
                        $this->showMain($user_id);
                        return false;
                    }
                    $this->sendServicePrice($text,$user_id);
            
                    break;

                case 'choose_result_date':
                    if($text == $this->textService->getText('main_menu',$user_id)){
                        $this->showMain($user_id);
                        return false;
                    }
                    $this->sendResultForDate($text,$user_id);

                    return false;
            
                    break;
            }
        }
       
    }

    public function senderRegister($sender,$user_id)
    {
        $sender->telegram_user_id = $user_id;
        $sender->uuid = '';
                
        if($sender->update()){
            try {
                Telegram::sendMessage(['chat_id' => $user_id, 'parse_mode'=>'html','text' =>'Tabriklaymiz '.$sender->fullname.' ! Siz ProMed klinikasi telegram botiga muvafaqqiyatli ulandingiz ! Bot sizga kerakli ma\'lumotlarni yuborib turadi!']);
                return false;    
            } catch (TelegramResponseException $e) {
                \Log::error('Not sended for User - '. $user_id);
            }
        }
    }

    public function showMain($user_id)
    {
        $this->telegramUserService->setStep($user_id, 'main');
        
        $keyboard = [
            [$this->textService->getText('get_results', $user_id),$this->textService->getText('prices', $user_id)],
            [$this->textService->getText('clinic_location', $user_id), "💬 ".$this->textService->getText('feedback', $user_id)],
            [$this->textService->getText('contact_reception', $user_id)],
            ["📞 ".$this->textService->getText('contact_director', $user_id)],
            ["🇺🇿🔄🇷🇺 ". $this->textService->getText('change_lang', $user_id)]
        ];
        
        $reply_markup = Keyboard::make(['keyboard' => $keyboard, 'resize_keyboard' => true]);

        Telegram::sendMessage(['chat_id' => $user_id, 'reply_markup' => $reply_markup, 'parse_mode'=>'html','text' => $this->textService->getText('welcome', $user_id)]);
        
    }

    public function sendFeedback($text, $user_id)
    {
        $text = '<b>💬  Мижоздан келган отзыв </b>: '.$text;

        $user = User::where('telegram_user_id',$user_id)->first();

        if($user && $user->patient){
            $text = $text.PHP_EOL;
            $text = $text.'-----------------------------------'.PHP_EOL;
            $text = $text.'<b>Мижоз исми</b>: '.$user->patient->full_name.PHP_EOL;
            $text = $text.'<b>Мижоз телефон рақами</b>: '.$user->patient->phone.PHP_EOL;
        }

        Telegram::sendMessage(['chat_id' => 2185696, 'text'=> $text, 'parse_mode'=>'html']);
    }

    public function chooseResultDates($user_id)
    {
        
        
        $daysWithServices = DB::table('patients')
            ->join('patient_services', 'patients.id', '=', 'patient_services.patient_id')
            ->join('users', 'users.id', '=', 'patients.user_id')
            ->join('telegram_users', 'telegram_users.id', '=', 'users.telegram_user_id')
            ->selectRaw('DATE(patient_services.created_at) as service_date')
            ->groupBy('service_date')
            ->where('users.telegram_user_id', $user_id)
            ->where('patient_services.service_status_id', PatientService::STATUS_CONCLUSED) // Replace $userId with the actual user ID
            ->get()
            ->pluck('service_date')
            ->toArray();

        if(count($daysWithServices)){
            $this->telegramUserService->setStep($user_id, 'choose_result_date');
            
            $keyboard = [];
 
            foreach($daysWithServices as $date){
                $keyboard[] = [$date];
            }
    
            $keyboard[] = [$this->textService->getText('main_menu',$user_id)];
            
            $reply_markup = Keyboard::make(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => true]);
    
            Telegram::sendMessage(['chat_id' => $user_id, 'reply_markup' => $reply_markup, 'parse_mode'=>'html','text' => $this->textService->getText('choose_result_date', $user_id)]);
    
        } else {

            $reply_markup = $this->getMainReplyKeyboard($user_id);
            Telegram::sendMessage(['chat_id' => $user_id, 'reply_markup' => $reply_markup, 'parse_mode'=>'html','text' => $this->textService->getText('no_results', $user_id)]);

        }
       
    }

    public function showPrices($user_id)
    {
        $this->telegramUserService->setStep($user_id, 'choose_service_type');
        $types = ServiceType::where('visible_for_patient',true)->get();
       
        $keyboard = [];

        foreach($types as $type){
            $keyboard[] = [$type->name];
        }


        $keyboard[] = [$this->textService->getText('main_menu',$user_id)];
        
        $reply_markup = Keyboard::make(['keyboard' => $keyboard, 'resize_keyboard' => true]);

        Telegram::sendMessage(['chat_id' => $user_id, 'reply_markup' => $reply_markup, 'parse_mode'=>'html','text' => $this->textService->getText('choose_service_type', $user_id)]);

    }

    public function sendServicePrice($text,$user_id)
    {

        Telegram::sendMessage(['chat_id' => $user_id, 'parse_mode'=>'html','text' => $this->textService->getText('waiting_price', $user_id)]);


        $serviceType = ServiceType::where('name',$text)->first();
        
        // bu yerda agar shunaqa service type bo'lsa unda pdf generate qilinadi, vaqtincha saqlanadi va telegramdan yuboriladi

        if($serviceType){
            
            $path = PrintController::generatePrices($serviceType->id);
            
            try {

                // $reply_markup = $this->getMainReplyKeyboard($user_id);

                Telegram::sendDocument([
                    'chat_id' => $user_id, 
                    'document' => new InputFile($path),
                    'caption' => $serviceType->name,
                    // 'reply_markup' => $reply_markup
                ]);

             } catch (TelegramResponseException $e) {
             
                \Log::error('Not sended for User - '. $e->getMessage());
            }
        
        }
       
        return false;
    }

    public function sendResultForDate($text,$user_id)
    {
       
        $patient = $this->telegramUserService->getPatient($user_id);

        if($patient){
            $services = PatientService::where('patient_id',$patient->id)->whereDate('created_at', $text)->get();
      
            if($services && $services->count()){
     

                foreach($services as $patient_service){
                //    info($patient_service->conclusion_file);
                    try {

                        $filePath = public_path().'/storage/'.$patient_service->conclusion_file;
                        if($patient_service->conclusion_file && File::exists($filePath)){

                            $reply_markup = $this->getMainReplyKeyboard($user_id);

                            Telegram::sendDocument([
                                'chat_id' => $user_id, 
                                'document' => new InputFile($filePath),
                                'caption' => $patient_service->service->name,
                                'reply_markup' => $reply_markup
                            ]);
                        } 
                      
                    } catch (TelegramResponseException $e) {
                    
                        \Log::error('Not sended for User - '. $e->getMessage());
                    }
                }
            
            }
        }
       
    }

    public function sendLocation($user_id)
    {
        Telegram::sendMessage(['chat_id' => $user_id, 'parse_mode'=>'html','text'=> $this->textService->getText('clinic_location_text',$user_id)]);
        Telegram::sendLocation(['chat_id' => $user_id, 'parse_mode'=>'html','latitude' =>'41.345954', 'longitude' =>'69.391086']);
        
        
        $text = '<b>📍 '.$this->textService->getText('address_label',$user_id).'</b>: '.$this->textService->getText('full_address',$user_id).PHP_EOL.PHP_EOL;
        $text = $text.'<b>☎️ '.$this->textService->getText('phone_label',$user_id).'</b>: +998 71 200 67 77'.PHP_EOL;
        
        $reply_markup = $this->getMainReplyKeyboard($user_id);


        Telegram::sendMessage(['chat_id' => $user_id, 'parse_mode'=>'html', 'reply_markup'=> $reply_markup, 'text'=> $text]);
    
    }

    public function getMainReplyKeyboard($user_id)
    {

        $keyboard = [
            [$this->textService->getText('get_results', $user_id),$this->textService->getText('prices', $user_id)],
            [$this->textService->getText('clinic_location', $user_id), "💬 ".$this->textService->getText('feedback', $user_id)],
            [$this->textService->getText('contact_reception', $user_id)],
            ["📞 ".$this->textService->getText('contact_director', $user_id)],
            ["🇺🇿🔄🇷🇺 ". $this->textService->getText('change_lang', $user_id)]
        ];
        
        return Keyboard::make(['keyboard' => $keyboard, 'resize_keyboard' => true]);
    }
}