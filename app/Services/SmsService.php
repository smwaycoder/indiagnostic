<?php 

namespace App\Services;
use Illuminate\Support\Facades\Http;
use Throwable;
class SmsService
{

 
  public function send($params)
  {
    $username = config('sms.username');
    $password = config('sms.password');
    $baseUrl = config('sms.baseUrl');

    foreach($params['phones'] as $phone){
      
      $numericOnly = preg_replace('/[^0-9]/', '', $phone);
      $phoneFormat = substr($numericOnly, -9);

      $messages[] = [
          "recipient" => $phoneFormat,
          "message-id" => "abc000000001".$phoneFormat,
          "sms"=> [
              "originator"=> "3700",
              "content"=>  [
              "text" => $params['message']
              ]
          ]
      ];
    
    }

    $response = Http::withBasicAuth($username,$password)->post($baseUrl, [
        'messages' => $messages
    ]);

 
    return $response->status();
  }

}