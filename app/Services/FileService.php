<?php

namespace App\Services;
use Illuminate\Support\Facades\Storage;

use App\Models\PatientService;
use App\Models\Patient;

use Barryvdh\DomPDF\Facade as PDF;
class FileService {

    public static function saveConclusion($patientService)
    {
        $pdfConclusion = PDF::loadView('pdf.conclusion', [
            'patientService' => $patientService,
            'content' => $patientService->conclusion
        ]);

        $fullPathRecommendation = '';
        
        if($patientService->recommendation){
            $pdfRecommendation = PDF::loadView('pdf.conclusion', [
                'patientService' => $patientService,
                'content' => $patientService->recommendation
            ]);

            // added comment
            $fullPathRecommendation = '/recommendations/'.$patientService->id.'recommendation.pdf';
            Storage::put('public'.$fullPathRecommendation, $pdfRecommendation->output());
        }
     
        
        $fullPathConclusion = '/conclusions/'.$patientService->id.'conclusion.pdf';
        Storage::put('public'.$fullPathConclusion, $pdfConclusion->output());

        $updated = $patientService->update([
            'conclusion_file' =>  $fullPathConclusion,
            'recommendation_file' => ($fullPathRecommendation != '') ? $fullPathRecommendation : null 
        ]);

        if($updated){
            return true;
        }

        return false;
            
    }
   
}