<?php 

namespace App\Services;

use Telegram;
use App\Models\TelegramUser;
use App\Models\User;
use App\Models\PatientSender;

use Telegram\Bot\Keyboard\Keyboard;
use  Telegram\Bot\Exceptions\TelegramResponseException;

//services

use App\Services\TelegramUserService;
use App\Services\BotTextService;

class TelegramBotService
{

    public function __construct(
        public TelegramUserService $telegramUserService,
        public BotTextService $textService,
    ){}

    public function handle()
    {
        $update = Telegram::getWebhookUpdates();
       
        $updates = json_decode($update,true);
        // private chatdan yozilmasa ignor qilamiz
        if (isset($updates['message']['chat']['type'])){
            $type = $updates['message']['chat']['type'];

            if ($type != 'private'){
                return false;
            }
        }

        // chat ID va text olish 

        $isCallback = false;

        if(isset($updates['message']))
        {
            $telegram = $updates['message'];

        } elseif(isset($updates['callback_query'])) {

            $isCallback = true;
            $telegram = $updates['callback_query'];
       
        } else {
            return false;
        }

        $user_id = $telegram['from']['id'];

        $text = ($isCallback) ? $telegram['data'] : ((isset($telegram['text'])) ? $telegram['text']  : '');

        $telegramUser = $this->telegramUserService->getUser($user_id, $telegram['from']);


        $hiddenReplyMarkup = json_encode([
            'remove_keyboard' => true,
        ]);


        // patient senderlar uchun register qismi
        if(str_contains($text, '/start')){
            if(strlen($text) > 7){
             
                $token = substr($text, 7);
                $sender = PatientSender::where('uuid', $token)->first();
                
                if($sender){
                    $this->senderRegister($sender,$user_id);
                }
                return false; 
            }
        }
        
       

        if($text == '/start'){
            $this->telegramUserService->setLanguage($user_id, 'ru');
            $this->showMain($user_id);        
        }
        elseif (isset($telegram['contact'])){
            if (isset($telegram['contact']['user_id']) && $telegram['contact']['user_id'] == $user_id){

                $phone = substr($telegram['contact']['phone_number'], -9);
               
                if(!User::where('username',$phone)->exists()){
                 
                    Telegram::sendMessage(['chat_id' => $user_id, 'parse_mode'=>'html','text' => 'Bu raqam ProMed klinikasida ro\'yxatga olinmagan!', 'reply_markup' => $hiddenReplyMarkup]);
                    return false;
                }else{
                    $telegramUser->update([
                        'step' => 1,
                        'phone' => $phone
                    ]);
                    Telegram::sendMessage(['chat_id' => $user_id, 'parse_mode'=>'html','text' => 'ProMed klinikasi tomonidan berilgan parolni kiriting', 'reply_markup' => $hiddenReplyMarkup]);
                }
            }
        }

        if($telegramUser->step == 1 && $text != '/start' && !isset($telegram['contact'])){
            //demak parol kirityapti

            if(auth()->attempt(array('username' => $telegramUser->phone, 'password' => $text))){
                $telegramUser->update(['step' => 2]);
             
                $user = User::find(auth()->id());
                $user->telegram_user_id= $telegramUser->id;
                $user->save();

                $welcomeText = "Tabriklaymiz ".$user->name.", siz ProMed botidan muvafaqqiyatli ro'yxatdan o'tdingiz. Bot sizga ProMed klinikasidan olingan tekshiruv natijalarini yuborib turadi";
                Telegram::sendMessage(['chat_id' => $user_id, 'parse_mode'=>'html','text' => $welcomeText, 'reply_markup' => $hiddenReplyMarkup]);  
            }else{
                Telegram::sendMessage(['chat_id' => $user_id, 'parse_mode'=>'html','text' => 'Parolni xato kiritdingiz. Tekshirib qaytadan kiriting', 'reply_markup' => $hiddenReplyMarkup]);
                return false;
            }
        }

        
        if($text == 'ping'){
            try {
                Telegram::sendMessage(['chat_id' => $user_id, 'parse_mode'=>'html','text' =>'pong']);
            } catch (TelegramResponseException $e) {
                \Log::error('Not sended for User - '. $user_id);
            }
        }

        switch($text){
            case 'Klinika manzili':
                Telegram::sendMessage(['chat_id' => $user_id, 'parse_mode'=>'html','text'=> 'Klinikamiz quyidagi manzilda joylashgan:']);
                Telegram::sendLocation(['chat_id' => $user_id, 'parse_mode'=>'html','latitude' =>'41.345954', 'longitude' =>'69.391086']);
                break;
            case 'Otziv qoldirish':
                Telegram::sendMessage(['chat_id' => $user_id, 'parse_mode'=>'html','text'=> 'Klinika rahbariyati uchun fikr va takliflaringizni yozib qoldiring:']);
                break;
            case "Registratura bilan bog'lanish":

                $txt = "Registratura bilan quyidagi manzil orqali bog'lanishingiz mumkin: @ProMed_clinica";

                Telegram::sendMessage(['chat_id' => $user_id, 'parse_mode'=>'html','text'=> $txt]);
                break;
            case "Klinika rahbariyati bilan bog'lanish":
                $txt = "Klinika rahbariyati bilan quyidagi manzil orqali bog'lanishingiz mumkin: @f_juraev";

                Telegram::sendMessage(['chat_id' => $user_id, 'parse_mode'=>'html','text'=> $txt]);
                break;
            
        }

       
    }

    public function senderRegister($sender,$user_id)
    {
        $sender->telegram_user_id = $user_id;
        $sender->uuid = '';
                
        if($sender->update()){
            try {
                Telegram::sendMessage(['chat_id' => $user_id, 'parse_mode'=>'html','text' =>'Tabriklaymiz '.$sender->fullname.' ! Siz ProMed klinikasi telegram botiga muvafaqqiyatli ulandingiz ! Bot sizga kerakli ma\'lumotlarni yuborib turadi!']);
                return false;    
            } catch (TelegramResponseException $e) {
                \Log::error('Not sended for User - '. $user_id);
            }
        }
    }

    public function showMain($user_id)
    {
        $this->telegramUserService->setStep($user_id, 'main');
        
        $keyboard = [
            [$this->textService->getText('get_results', $user_id),$this->textService->getText('prices', $user_id)],
            [$this->textService->getText('clinic_location', $user_id), "💬 ".$this->textService->getText('feedback', $user_id)],
            [$this->textService->getText('contact_reception', $user_id)],
            ["📞 ".$this->textService->getText('contact_doctor', $user_id)],
            ["📞 ".$this->textService->getText('contact_director', $user_id)],
            ["🇺🇿🔄🇷🇺 ". $this->textService->getText('change_lang', $user_id)]
        ];
        
        $reply_markup = Keyboard::make(['keyboard' => $keyboard, 'resize_keyboard' => true, 'one_time_keyboard' => true]);

        Telegram::sendMessage(['chat_id' => $user_id, 'reply_markup' => $reply_markup, 'parse_mode'=>'html','text' => $this->textService->getText('welcome', $user_id)]);
        
    }
}