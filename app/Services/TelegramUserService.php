<?php 

namespace App\Services;
use App\Models\TelegramUser;
use App\Models\Patient;

class TelegramUserService
{

    public function getUser($chatID, $telegramData)
    {
        return TelegramUser::firstOrCreate(['id' => $chatID], $telegramData);
    }

    public function getPatient($chatID)
    {
        $patient = Patient::whereHas('user', function($q) use($chatID) {
            $q->where('telegram_user_id', $chatID);
        })->first(); 

        return $patient;
    }

    public function setStep($chatID, $step)
    {
        $user =  TelegramUser::find($chatID);

        $user->step = $step;
        $user->save();
    }

    public function getStep($chatID)
    {
        $user =  TelegramUser::find($chatID);

        return $user->step;
    }


    public function getLanguage($chatID){
        $user = TelegramUser::find($chatID);

        if($user){
            return $user->lang;
        }

        // default lang sifatida uz qaytaramiz
        return 'uz';
    }


    public function setLanguage($chatID, $lang)
    {
     
        $user =  TelegramUser::find($chatID);
    
        if($user){
            $user->lang = $lang;
            $user->save();
        }

    }





}