<?php 

namespace App\Services;
use App\Models\BotText;

class BotTextService
{

    public $service;


    public function __construct(TelegramUserService $service ){
        $this->service = $service;
    }

    public function getText($keyword, $chatID)
    {
        $lang = $this->service->getLanguage($chatID);

        $text = BotText::where('keyword', $keyword)->first();
     
        if($text){
            return $text->$lang;
        }
        return '';
    }






}