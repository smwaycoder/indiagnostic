<?php

namespace App\Observers;

use App\Models\PatientService;


class PatientServiceObserver
{

    /**
     * Handle the PatientService "created" event.
     *
     * @param  \App\Models\PatientService  $patientService
     * @return void
     */
    public function created(PatientService $patientService)
    {
        // tekshirish kerak agar patientga telegram ulanmagan  bo'lsa unga xabar yuborish 
      
    }

    /**
     * Handle the PatientService "updated" event.
     *
     * @param  \App\Models\PatientService  $patientService
     * @return void
     */
    public function updated(PatientService $patientService)
    {
        //
    }

    /**
     * Handle the PatientService "deleted" event.
     *
     * @param  \App\Models\PatientService  $patientService
     * @return void
     */
    public function deleted(PatientService $patientService)
    {
        //
    }

    /**
     * Handle the PatientService "restored" event.
     *
     * @param  \App\Models\PatientService  $patientService
     * @return void
     */
    public function restored(PatientService $patientService)
    {
        //
    }

    /**
     * Handle the PatientService "force deleted" event.
     *
     * @param  \App\Models\PatientService  $patientService
     * @return void
     */
    public function forceDeleted(PatientService $patientService)
    {
        //
    }
}
