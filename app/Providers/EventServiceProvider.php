<?php

namespace App\Providers;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;
use App\Events\GivedConclusion;
use App\Events\PatientServicePayed;
use App\Listeners\SendConclusionNotifications;
use App\Listeners\SendDoctorNotification;
use App\Models\PatientService;
use App\Observers\PatientServiceObserver;


class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        GivedConclusion::class => [
            SendConclusionNotifications::class,
        ],
        PatientServicePayed::class => [
            SendDoctorNotification::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        PatientService::observe(PatientServiceObserver::class);
    }
}
