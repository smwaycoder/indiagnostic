<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use App\Models\ServiceType;
use Auth;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*', function ($view) 
        {
          if(Auth::check()){
            $user  = Auth::user();
            $view->with('user', $user);
          }  
        });  

        
      $serviceTypes = ServiceType::get();
      View::share('serviceTypes', $serviceTypes); 
      $birthYears = range(1920, date('Y'));
      View::share('birthYears', $birthYears); 
      

    }
}
